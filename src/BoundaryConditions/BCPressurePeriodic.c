#include <stdio.h>
#include <arrayfunctions.h>
#include <mpivars.h>
#include <boundaryconditions.h>

int BCPressurePeriodic_imin(int *dim, double *phi, void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  double *pbuf = boundary->buffer;

  int i, j, k, is, ie, js, je, ks, ke;
  if ((mpi->ip == 0) || (mpi->ip == mpi->iproc-1)) {
    int source[3],dest[3],limits[6];
    source[0] = mpi->iproc-1; source[1] = mpi->jp; source[2] = mpi->kp;
    dest[0]   = 0           ; dest[1]   = mpi->jp; dest[2]   = mpi->kp;
    limits[0] = imax-ghosts;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = kmax;
    ierr = MPIGetArrayData3D(pbuf,phi,&source[0],&dest[0],&limits[0],dim,mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = -ghosts;
    ie = 0;
    js = boundary->js;
    je = boundary->je;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imax ,jmax,kmax,ghosts);
          int pi = Index1D(i-is,j,k,ghosts,jmax,kmax,0     );
          phi[pb] = pbuf[pi];
        }
      }
    }
  }

  return(0);
}

int BCPressurePeriodic_imax(int *dim, double *phi, void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  double *pbuf = boundary->buffer;

  int i, j, k, is, ie, js, je, ks, ke;
  if ((mpi->ip == 0) || (mpi->ip == mpi->iproc-1)) {
    int source[3],dest[3],limits[6];
    source[0] = 0           ; source[1] = mpi->jp; source[2] = mpi->kp;
    dest[0]   = mpi->iproc-1; dest[1]   = mpi->jp; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = ghosts;
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = kmax;
    ierr = MPIGetArrayData3D(pbuf,phi,&source[0],&dest[0],&limits[0],dim,mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = imax;
    ie = imax+ghosts;
    js = boundary->js;
    je = boundary->je;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imax ,jmax,kmax,ghosts);
          int pi = Index1D(i-is,j,k,ghosts,jmax,kmax,0     );
          phi[pb] = pbuf[pi];
        }
      }
    }
  }

  return(0);
}

int BCPressurePeriodic_jmin(int *dim, double *phi, void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  double *pbuf = boundary->buffer;;

  int i, j, k, is, ie, js, je, ks, ke;
  if ((mpi->jp == 0) || (mpi->jp == mpi->jproc-1)) {
    int source[3],dest[3],limits[6];
    source[0] = mpi->ip; source[1] = mpi->jproc-1; source[2] = mpi->kp;
    dest[0]   = mpi->ip; dest[1]   = 0           ; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = jmax-ghosts;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = kmax;
    ierr = MPIGetArrayData3D(pbuf,phi,&source[0],&dest[0],&limits[0],dim,mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = -ghosts;
    je = 0;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imax,jmax  ,kmax,ghosts);
          int pi = Index1D(i,j-js,k,imax,ghosts,kmax,0     );
          phi[pb] = pbuf[pi];
        }
      }
    }
  }

  return(0);
}

int BCPressurePeriodic_jmax(int *dim, double *phi, void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  double *pbuf = boundary->buffer;;

  int i, j, k, is, ie, js, je, ks, ke;
  if ((mpi->jp == 0) || (mpi->jp == mpi->jproc-1)) {
    int source[3],dest[3],limits[6];
    source[0] = mpi->ip; source[1] = 0           ; source[2] = mpi->kp;
    dest[0]   = mpi->ip; dest[1]   = mpi->jproc-1; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = ghosts;
    limits[4] = 0;
    limits[5] = kmax;
    ierr = MPIGetArrayData3D(pbuf,phi,&source[0],&dest[0],&limits[0],dim,mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = jmax;
    je = jmax+ghosts;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imax,jmax  ,kmax,ghosts);
          int pi = Index1D(i,j-js,k,imax,ghosts,kmax,0     );
          phi[pb] = pbuf[pi];
        }
      }
    }
  }

  return(0);
}

int BCPressurePeriodic_kmin(int *dim, double *phi, void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  double *pbuf = boundary->buffer;

  int i, j, k, is, ie, js, je, ks, ke;
  if ((mpi->kp == 0) || (mpi->kp == mpi->kproc-1)) {
    int source[3],dest[3],limits[6];
    source[0] = mpi->ip; source[1] = mpi->jp; source[2] = mpi->kproc-1;
    dest[0]   = mpi->ip; dest[1]   = mpi->jp; dest[2]   = 0           ;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = kmax-ghosts;
    limits[5] = kmax;
    ierr = MPIGetArrayData3D(pbuf,phi,&source[0],&dest[0],&limits[0],dim,mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = boundary->js;
    je = boundary->je;
    ks = -ghosts;
    ke = 0;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k   ,imax,jmax,kmax  ,ghosts);
          int pi = Index1D(i,j,k-ks,imax,jmax,ghosts,0     );
          phi[pb] = pbuf[pi];
        }
      }
    }
  }

  return(0);
}

int BCPressurePeriodic_kmax(int *dim, double *phi, void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];

  double *pbuf = boundary->buffer;

  int i, j, k, is, ie, js, je, ks, ke;
  if ((mpi->kp == 0) || (mpi->kp == mpi->kproc-1)) {
    int source[3],dest[3],limits[6];
    source[0] = mpi->ip; source[1] = mpi->jp; source[2] = 0           ;
    dest[0]   = mpi->ip; dest[1]   = mpi->jp; dest[2]   = mpi->kproc-1;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = ghosts;
    ierr = MPIGetArrayData3D(pbuf,phi,&source[0],&dest[0],&limits[0],dim,mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = boundary->js;
    je = boundary->je;
    ks = kmax;
    ke = kmax+ghosts;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k   ,imax,jmax,kmax  ,ghosts);
          int pi = Index1D(i,j,k-ks,imax,jmax,ghosts,0     );
          phi[pb] = pbuf[pi];
        }
      }
    }
  }

  return(0);
}

