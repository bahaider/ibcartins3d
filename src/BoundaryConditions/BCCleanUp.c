#include <stdlib.h>
#include <boundaryconditions.h>

int BCCleanUp(void *b)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  if (boundary->buffer) free(boundary->buffer);
  return(0);
}
