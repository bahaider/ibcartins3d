#include <stdio.h>
#include <string.h>
#include <arrayfunctions.h>
#include <boundaryconditions.h>

int BCVelocitySolid_imin(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((imaxu-imax) == 1)  staggered = 1;
  else if (imaxu == imax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocitySolid_imin(): imaxu (%d) and imax (%d) values are not compatible.\n",
            imaxu,imax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = -ghosts;
    ie = 0;
    js = boundary->js;
    je = boundary->je;
    ks = boundary->ks;
    ke = boundary->ke;
    if (staggered) {
      for (i = ie; i < ie+1; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i  ,j,k,imaxu,jmax,kmax,ghosts);
            u[pb] = (delta ? 0 : boundary->uWall);
          }
        }
      }
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i ,j,k,imaxu,jmax,kmax,ghosts);
            int pi = Index1D(-i,j,k,imaxu,jmax,kmax,ghosts);
            u[pb] = (delta ? -u[pi] : 2*boundary->uWall - u[pi]);
          }
        }
      }
    } else {
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i     ,j,k,imaxu,jmax,kmax,ghosts);
            int pi = Index1D(-(i+1),j,k,imaxu,jmax,kmax,ghosts);
            u[pb] = (delta ? -u[pi] : 2*boundary->uWall - u[pi]);
          }
        }
      }
    }
    /* v */
    is = -ghosts;
    ie = 0;
    js = (staggered ? boundary->jsv : boundary->js);
    je = (staggered ? boundary->jev : boundary->je);
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i     ,j,k,imax,jmaxv,kmax,ghosts);
          int pi = Index1D(-(i+1),j,k,imax,jmaxv,kmax,ghosts);
          v[pb] = (delta ? -v[pi] : 2*boundary->vWall - v[pi]);
        }
      }
    }
    /* w */
    is = -ghosts;
    ie = 0;
    js = boundary->js;
    je = boundary->je;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i     ,j,k,imax,jmax,kmaxw,ghosts);
          int pi = Index1D(-(i+1),j,k,imax,jmax,kmaxw,ghosts);
          w[pb] = (delta ? -w[pi] : 2*boundary->wWall - w[pi]);
        }
      }
    }
  }

  return(0);
}

int BCVelocitySolid_imax(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((imaxu-imax) == 1)  staggered = 1;
  else if (imaxu == imax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocitySolid_imax(): imaxu (%d) and imax (%d) values are not compatible.\n",
            imaxu,imax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = imaxu;
    ie = imaxu+ghosts;
    js = boundary->js;
    je = boundary->je;
    ks = boundary->ks;
    ke = boundary->ke;
    if (staggered) {
      for (i = is-1; i < is; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i  ,j,k,imaxu,jmax,kmax,ghosts);
            u[pb] = (delta ? 0 : boundary->uWall);
          }
        }
      }
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i          ,j,k,imaxu,jmax,kmax,ghosts);
            int pi = Index1D(imax+is-i-1,j,k,imaxu,jmax,kmax,ghosts);
            u[pb] = (delta ? -u[pi] : 2*boundary->uWall - u[pi]);
          }
        }
      }
    } else {
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i          ,j,k,imaxu,jmax,kmax,ghosts);
            int pi = Index1D(imax+is-i-1,j,k,imaxu,jmax,kmax,ghosts);
            u[pb] = (delta ? -u[pi] : 2*boundary->uWall - u[pi]);
          }
        }
      }
    }
    /* v */
    is = imax;
    ie = imax+ghosts;
    js = (staggered ? boundary->jsv : boundary->js);
    je = (staggered ? boundary->jev : boundary->je);
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i          ,j,k,imax,jmaxv,kmax,ghosts);
          int pi = Index1D(imax+is-i-1,j,k,imax,jmaxv,kmax,ghosts);
          v[pb] = (delta ? -v[pi] : 2*boundary->vWall - v[pi]);
        }
      }
    }
    /* w */
    is = imax;
    ie = imax+ghosts;
    js = boundary->js;
    je = boundary->je;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i          ,j,k,imax,jmax,kmaxw,ghosts);
          int pi = Index1D(imax+is-i-1,j,k,imax,jmax,kmaxw,ghosts);
          w[pb] = (delta ? -w[pi] : 2*boundary->wWall - w[pi]);
        }
      }
    }
  }

  return(0);
}

int BCVelocitySolid_jmin(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((jmaxv-jmax) == 1)  staggered = 1;
  else if (jmaxv == jmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocitySolid_jmin(): jmaxv (%d) and jmax (%d) values are not compatible.\n",
            jmaxv,jmax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = (staggered ? boundary->isu : boundary->is) - ghosts;
    ie = (staggered ? boundary->ieu : boundary->ie) + ghosts;
    js = -ghosts;
    je = 0;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j     ,k,imaxu,jmax,kmax,ghosts);
          int pi = Index1D(i,-(j+1),k,imaxu,jmax,kmax,ghosts);
          u[pb] = (delta ? -u[pi] : 2*boundary->uWall - u[pi]);
        }
      }
    }
    /* v */
    is = boundary->is - ghosts;
    ie = boundary->ie + ghosts;
    js = -ghosts;
    je = 0;
    ks = boundary->ks;
    ke = boundary->ke;
    if (staggered) {
      for (i = is; i < ie; i++) {
        for (j = je; j < je+1; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i,j  ,k,imax,jmaxv,kmax,ghosts);
            v[pb] = (delta ? 0 : boundary->vWall);
          }
        }
      }
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i,j ,k,imax,jmaxv,kmax,ghosts);
            int pi = Index1D(i,-j,k,imax,jmaxv,kmax,ghosts);
            v[pb] = (delta ? -v[pi] : 2*boundary->vWall - v[pi]);
          }
        }
      }
    } else {
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i,j     ,k,imax,jmaxv,kmax,ghosts);
            int pi = Index1D(i,-(j+1),k,imax,jmaxv,kmax,ghosts);
            v[pb] = (delta ? -v[pi] : 2*boundary->vWall - v[pi]);
          }
        }
      }
    }
    /* w */
    is = boundary->is - ghosts;
    ie = boundary->ie + ghosts;
    js = -ghosts;
    je = 0;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j     ,k,imax,jmax,kmaxw,ghosts);
          int pi = Index1D(i,-(j+1),k,imax,jmax,kmaxw,ghosts);
          w[pb] = (delta ? -w[pi] : 2*boundary->wWall - w[pi]);
        }
      }
    }
  }

  return(0);
}

int BCVelocitySolid_jmax(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((jmaxv-jmax) == 1)  staggered = 1;
  else if (jmaxv == jmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocitySolid_jmax(): jmaxv (%d) and jmax (%d) values are not compatible.\n",
            jmaxv,jmax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = (staggered ? boundary->isu : boundary->is) - ghosts;
    ie = (staggered ? boundary->ieu : boundary->ie) + ghosts;
    js = jmax;
    je = jmax+ghosts;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j          ,k,imaxu,jmax,kmax,ghosts);
          int pi = Index1D(i,jmax+js-j-1,k,imaxu,jmax,kmax,ghosts);
          u[pb] = (delta ? -u[pi] : 2*boundary->uWall - u[pi]);
        }
      }
    }
    /* v */
    is = boundary->is - ghosts;
    ie = boundary->ie + ghosts;
    js = jmaxv;
    je = jmaxv+ghosts;
    ks = boundary->ks;
    ke = boundary->ke;
    if (staggered) {
      for (i = is; i < ie; i++) {
        for (j = js-1; j < js; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i,j  ,k,imax,jmaxv,kmax,ghosts);
            v[pb] = (delta ? 0 : boundary->vWall);
          }
        }
      }
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i,j          ,k,imax,jmaxv,kmax,ghosts);
            int pi = Index1D(i,jmax+js-j-1,k,imax,jmaxv,kmax,ghosts);
            v[pb] = (delta ? -v[pi] : 2*boundary->vWall - v[pi]);
          }
        }
      }
    } else {
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i,j          ,k,imax,jmaxv,kmax,ghosts);
            int pi = Index1D(i,jmax+js-j-1,k,imax,jmaxv,kmax,ghosts);
            v[pb] = (delta ? -v[pi] : 2*boundary->vWall - v[pi]);
          }
        }
      }
    }
    /* w */
    is = boundary->is - ghosts;
    ie = boundary->ie + ghosts;
    js = jmax;
    je = jmax+ghosts;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j          ,k,imax,jmax,kmaxw,ghosts);
          int pi = Index1D(i,jmax+js-j-1,k,imax,jmax,kmaxw,ghosts);
          w[pb] = (delta ? -w[pi] : 2*boundary->wWall - w[pi]);
        }
      }
    }
  }

  return(0);
}

int BCVelocitySolid_kmin(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((kmaxw-kmax) == 1)  staggered = 1;
  else if (kmaxw == kmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocitySolid_kmin(): kmaxw (%d) and kmax (%d) values are not compatible.\n",
            kmaxw,kmax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = (staggered ? boundary->isu : boundary->is) - ghosts;
    ie = (staggered ? boundary->ieu : boundary->ie) + ghosts;
    js = boundary->js - ghosts;
    je = boundary->je + ghosts;
    ks = -ghosts;
    ke = 0;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k     ,imaxu,jmax,kmax,ghosts);
          int pi = Index1D(i,j,-(k+1),imaxu,jmax,kmax,ghosts);
          u[pb] = (delta ? -u[pi] : 2*boundary->uWall - u[pi]);
        }
      }
    }
    /* v */
    is = boundary->is - ghosts;
    ie = boundary->ie + ghosts;
    js = (staggered ? boundary->jsv : boundary->js) - ghosts;
    je = (staggered ? boundary->jev : boundary->je) + ghosts;
    ks = -ghosts;
    ke = 0;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k     ,imax,jmaxv,kmax,ghosts);
          int pi = Index1D(i,j,-(k+1),imax,jmaxv,kmax,ghosts);
          v[pb] = (delta ? -v[pi] : 2*boundary->vWall - v[pi]);
        }
      }
    }
    /* w */
    is = boundary->is - ghosts;
    ie = boundary->ie + ghosts;
    js = boundary->js - ghosts;
    je = boundary->je + ghosts;
    ks = -ghosts;
    ke = 0;
    if (staggered) {
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ke; k < ke+1; k++) {
            int pb = Index1D(i,j,k  ,imax,jmax,kmaxw,ghosts);
            w[pb] = (delta ? 0 : boundary->wWall);
          }
        }
      }
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i,j,k ,imax,jmax,kmaxw,ghosts);
            int pi = Index1D(i,j,-k,imax,jmax,kmaxw,ghosts);
            w[pb] = (delta ? -w[pi] : 2*boundary->wWall - w[pi]);
          }
        }
      }
    } else {
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i,j,k     ,imax,jmax,kmaxw,ghosts);
            int pi = Index1D(i,j,-(k+1),imax,jmax,kmaxw,ghosts);
            w[pb] = (delta ? -w[pi] : 2*boundary->wWall - w[pi]);
          }
        }
      }
    }
  }

  return(0);
}

int BCVelocitySolid_kmax(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((kmaxw-kmax) == 1)  staggered = 1;
  else if (kmaxw == kmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocitySolid_kmax(): kmaxw (%d) and kmax (%d) values are not compatible.\n",
            kmaxw,kmax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = (staggered ? boundary->isu : boundary->is) - ghosts;
    ie = (staggered ? boundary->ieu : boundary->ie) + ghosts;
    js = boundary->js - ghosts;
    je = boundary->je + ghosts;
    ks = kmax;
    ke = kmax+ghosts;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k          ,imaxu,jmax,kmax,ghosts);
          int pi = Index1D(i,j,kmax+ks-k-1,imaxu,jmax,kmax,ghosts);
          u[pb] = (delta ? -u[pi] : 2*boundary->uWall - u[pi]);
        }
      }
    }
    /* v */
    is = boundary->is - ghosts;
    ie = boundary->ie + ghosts;
    js = (staggered ? boundary->jsv : boundary->js) - ghosts;
    je = (staggered ? boundary->jev : boundary->je) + ghosts;
    ks = kmax;
    ke = kmax+ghosts;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k          ,imax,jmaxv,kmax,ghosts);
          int pi = Index1D(i,j,kmax+ks-k-1,imax,jmaxv,kmax,ghosts);
          v[pb] = (delta ? -v[pi] : 2*boundary->vWall - v[pi]);
        }
      }
    }
    /* w */
    is = boundary->is - ghosts;
    ie = boundary->ie + ghosts;
    js = boundary->js - ghosts;
    je = boundary->je + ghosts;
    ks = kmaxw;
    ke = kmaxw+ghosts;
    if (staggered) {
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks-1; k < ks; k++) {
            int pb = Index1D(i,j,k  ,imax,jmax,kmaxw,ghosts);
            w[pb] = (delta ? 0 : boundary->wWall);
          }
        }
      }
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i,j,k          ,imax,jmax,kmaxw,ghosts);
            int pi = Index1D(i,j,kmax+ks-k-1,imax,jmax,kmaxw,ghosts);
            w[pb] = (delta ? -w[pi] : 2*boundary->wWall - w[pi]);
          }
        }
      }
    } else {
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int pb = Index1D(i,j,k          ,imax,jmax,kmaxw,ghosts);
            int pi = Index1D(i,j,kmax+ks-k-1,imax,jmax,kmaxw,ghosts);
            w[pb] = (delta ? -w[pi] : 2*boundary->wWall - w[pi]);
          }
        }
      }
    }
  }

  return(0);
}

