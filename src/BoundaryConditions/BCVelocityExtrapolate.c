#include <stdio.h>
#include <string.h>
#include <arrayfunctions.h>
#include <boundaryconditions.h>

int BCVelocityExtrapolate_imin(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((imaxu-imax) == 1)  staggered = 1;
  else if (imaxu == imax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityExtrapolate_imin(): imaxu (%d) and imax (%d) values are not compatible.\n",
            imaxu,imax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = -ghosts;
    ie = 0;
    js = boundary->js;
    je = boundary->je;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i ,j,k,imaxu,jmax,kmax,ghosts);
          int pi = Index1D(ie,j,k,imaxu,jmax,kmax,ghosts);
          u[pb] = u[pi];
        }
      }
    }
    /* v */
    is = -ghosts;
    ie = 0;
    js = (staggered ? boundary->jsv : boundary->js);
    je = (staggered ? boundary->jev : boundary->je);
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i ,j,k,imax,jmaxv,kmax,ghosts);
          int pi = Index1D(ie,j,k,imax,jmaxv,kmax,ghosts);
          v[pb] = v[pi];
        }
      }
    }
    /* w */
    is = -ghosts;
    ie = 0;
    js = boundary->js;
    je = boundary->je;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i ,j,k,imax,jmax,kmaxw,ghosts);
          int pi = Index1D(ie,j,k,imax,jmax,kmaxw,ghosts);
          w[pb] = w[pi];
        }
      }
    }
  }

  return(0);
}

int BCVelocityExtrapolate_imax(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((imaxu-imax) == 1)  staggered = 1;
  else if (imaxu == imax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityExtrapolate_imax(): imaxu (%d) and imax (%d) values are not compatible.\n",
            imaxu,imax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = imaxu;
    ie = imaxu+ghosts;
    js = boundary->js;
    je = boundary->je;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imaxu,jmax,kmax,ghosts);
          int pi = Index1D(is-1,j,k,imaxu,jmax,kmax,ghosts);
          u[pb] = u[pi];
        }
      }
    }
    /* v */
    is = imax;
    ie = imax+ghosts;
    js = (staggered ? boundary->jsv : boundary->js);
    je = (staggered ? boundary->jev : boundary->je);
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imax,jmaxv,kmax,ghosts);
          int pi = Index1D(is-1,j,k,imax,jmaxv,kmax,ghosts);
          v[pb] = v[pi];
        }
      }
    }
    /* w */
    is = imax;
    ie = imax+ghosts;
    js = boundary->js;
    je = boundary->je;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imax,jmax,kmaxw,ghosts);
          int pi = Index1D(is-1,j,k,imax,jmax,kmaxw,ghosts);
          w[pb] = w[pi];
        }
      }
    }
  }

  return(0);
}

int BCVelocityExtrapolate_jmin(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((jmaxv-jmax) == 1)  staggered = 1;
  else if (jmaxv == jmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityExtrapolate_jmin(): jmaxv (%d) and jmax (%d) values are not compatible.\n",
            jmaxv,jmax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = (staggered ? boundary->isu : boundary->is);
    ie = (staggered ? boundary->ieu : boundary->ie);
    js = -ghosts;
    je = 0;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j ,k,imaxu,jmax,kmax,ghosts);
          int pi = Index1D(i,je,k,imaxu,jmax,kmax,ghosts);
          u[pb] = u[pi];
        }
      }
    }
    /* v */
    is = boundary->is;
    ie = boundary->ie;
    js = -ghosts;
    je = 0;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j ,k,imax,jmaxv,kmax,ghosts);
          int pi = Index1D(i,je,k,imax,jmaxv,kmax,ghosts);
          v[pb] = v[pi];
        }
      }
    }
    /* w */
    is = boundary->is;
    ie = boundary->ie;
    js = -ghosts;
    je = 0;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j     ,k,imax,jmax,kmaxw,ghosts);
          int pi = Index1D(i,je,k,imax,jmax,kmaxw,ghosts);
          w[pb] = w[pi];
        }
      }
    }
  }

  return(0);
}

int BCVelocityExtrapolate_jmax(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((jmaxv-jmax) == 1)  staggered = 1;
  else if (jmaxv == jmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityExtrapolate_jmax(): jmaxv (%d) and jmax (%d) values are not compatible.\n",
            jmaxv,jmax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = (staggered ? boundary->isu : boundary->is);
    ie = (staggered ? boundary->ieu : boundary->ie);
    js = jmax;
    je = jmax+ghosts;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imaxu,jmax,kmax,ghosts);
          int pi = Index1D(i,js-1,k,imaxu,jmax,kmax,ghosts);
          u[pb] = u[pi];
        }
      }
    }
    /* v */
    is = boundary->is;
    ie = boundary->ie;
    js = jmaxv;
    je = jmaxv+ghosts;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imax,jmaxv,kmax,ghosts);
          int pi = Index1D(i,js-1,k,imax,jmaxv,kmax,ghosts);
          v[pb] = v[pi];
        }
      }
    }
    /* w */
    is = boundary->is;
    ie = boundary->ie;
    js = jmax;
    je = jmax+ghosts;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imax,jmax,kmaxw,ghosts);
          int pi = Index1D(i,js-1,k,imax,jmax,kmaxw,ghosts);
          w[pb] = w[pi];
        }
      }
    }
  }

  return(0);
}

int BCVelocityExtrapolate_kmin(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((kmaxw-kmax) == 1)  staggered = 1;
  else if (kmaxw == kmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityExtrapolate_kmin(): kmaxw (%d) and kmax (%d) values are not compatible.\n",
            kmaxw,kmax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = (staggered ? boundary->isu : boundary->is);
    ie = (staggered ? boundary->ieu : boundary->ie);
    js = boundary->js;
    je = boundary->je;
    ks = -ghosts;
    ke = 0;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k ,imaxu,jmax,kmax,ghosts);
          int pi = Index1D(i,j,ke,imaxu,jmax,kmax,ghosts);
          u[pb] = u[pi];
        }
      }
    }
    /* v */
    is = boundary->is;
    ie = boundary->ie;
    js = (staggered ? boundary->jsv : boundary->js);
    je = (staggered ? boundary->jev : boundary->je);
    ks = -ghosts;
    ke = 0;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k ,imax,jmaxv,kmax,ghosts);
          int pi = Index1D(i,j,ke,imax,jmaxv,kmax,ghosts);
          v[pb] = v[pi];
        }
      }
    }
    /* w */
    is = boundary->is;
    ie = boundary->ie;
    js = boundary->js;
    je = boundary->je;
    ks = -ghosts;
    ke = 0;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k ,imax,jmax,kmaxw,ghosts);
          int pi = Index1D(i,j,ke,imax,jmax,kmaxw,ghosts);
          w[pb] = w[pi];
        }
      }
    }
  }

  return(0);
}

int BCVelocityExtrapolate_kmax(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((kmaxw-kmax) == 1)  staggered = 1;
  else if (kmaxw == kmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityExtrapolate_kmax(): kmaxw (%d) and kmax (%d) values are not compatible.\n",
            kmaxw,kmax);
    return(1);
  }

  if (boundary->on_this_proc) {
    int i, j, k, is, ie, js, je, ks, ke;
    /* u */
    is = (staggered ? boundary->isu : boundary->is);
    ie = (staggered ? boundary->ieu : boundary->ie);
    js = boundary->js;
    je = boundary->je;
    ks = kmax;
    ke = kmax+ghosts;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k   ,imaxu,jmax,kmax,ghosts);
          int pi = Index1D(i,j,ks-1,imaxu,jmax,kmax,ghosts);
          u[pb] = u[pi];
        }
      }
    }
    /* v */
    is = boundary->is;
    ie = boundary->ie;
    js = (staggered ? boundary->jsv : boundary->js);
    je = (staggered ? boundary->jev : boundary->je);
    ks = kmax;
    ke = kmax+ghosts;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k        ,imax,jmaxv,kmax,ghosts);
          int pi = Index1D(i,j,ks-1,imax,jmaxv,kmax,ghosts);
          v[pb] = v[pi];
        }
      }
    }
    /* w */
    is = boundary->is;
    ie = boundary->ie;
    js = boundary->js;
    je = boundary->je;
    ks = kmaxw;
    ke = kmaxw+ghosts;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k   ,imax,jmax,kmaxw,ghosts);
          int pi = Index1D(i,j,ks-1,imax,jmax,kmaxw,ghosts);
          w[pb] = w[pi];
        }
      }
    }
  }

  return(0);
}

