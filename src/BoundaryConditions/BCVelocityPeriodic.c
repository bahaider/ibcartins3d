#include <stdio.h>
#include <arrayfunctions.h>
#include <mpivars.h>
#include <boundaryconditions.h>

int BCVelocityPeriodic_imin(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  double         *buf      = boundary->buffer;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((imaxu-imax) == 1)  staggered = 1;
  else if (imaxu == imax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityPeriodic_imin(): imaxu (%d) and imax (%d) values are not compatible.\n",
            imaxu,imax);
    return(1);
  }

  int i, j, k, is, ie, js, je, ks, ke;
  /* u */
  if ((mpi->ip == 0) || (mpi->ip == mpi->iproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->iproc-1; source[1] = mpi->jp; source[2] = mpi->kp;
    dest[0]   = 0           ; dest[1]   = mpi->jp; dest[2]   = mpi->kp;
    limits[0] = (staggered ? (imaxu-1-ghosts) : (imaxu-ghosts));
    limits[1] = (staggered ? imaxu-1          : imaxu         );
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = kmax;
    dim[0]    = imaxu;
    dim[1]    = jmax;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,u,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = -ghosts;
    ie = 0;
    js = boundary->js;
    je = boundary->je;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imaxu ,jmax,kmax,ghosts);
          int pi = Index1D(i-is,j,k,ghosts,jmax,kmax,0     );
          u[pb] = buf[pi];
        }
      }
    }
  }
  /* v */
  if ((mpi->ip == 0) || (mpi->ip == mpi->iproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->iproc-1; source[1] = mpi->jp; source[2] = mpi->kp;
    dest[0]   = 0           ; dest[1]   = mpi->jp; dest[2]   = mpi->kp;
    limits[0] = imax-ghosts;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = jmaxv;
    limits[4] = 0;
    limits[5] = kmax;
    dim[0]    = imax;
    dim[1]    = jmaxv;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,v,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = -ghosts;
    ie = 0;
    js = (staggered ? boundary->jsv : boundary->js);
    je = (staggered ? boundary->jev : boundary->je);
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imax  ,jmaxv,kmax,ghosts);
          int pi = Index1D(i-is,j,k,ghosts,jmaxv,kmax,0     );
          v[pb] = buf[pi];
        }
      }
    }
  }
  /* w */
  if ((mpi->ip == 0) || (mpi->ip == mpi->iproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->iproc-1; source[1] = mpi->jp; source[2] = mpi->kp;
    dest[0]   = 0           ; dest[1]   = mpi->jp; dest[2]   = mpi->kp;
    limits[0] = imax-ghosts;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = kmaxw;
    dim[0]    = imax;
    dim[1]    = jmax;
    dim[2]    = kmaxw;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,w,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = -ghosts;
    ie = 0;
    js = boundary->js;
    je = boundary->je;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imax  ,jmax,kmaxw,ghosts);
          int pi = Index1D(i-is,j,k,ghosts,jmax,kmaxw,0     );
          w[pb] = buf[pi];
        }
      }
    }
  }

  return(0);
}

int BCVelocityPeriodic_imax(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  double         *buf      = boundary->buffer;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((imaxu-imax) == 1)  staggered = 1;
  else if (imaxu == imax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityPeriodic_imax(): imaxu (%d) and imax (%d) values are not compatible.\n",
            imaxu,imax);
    return(1);
  }

  int i, j, k, is, ie, js, je, ks, ke;
  /* u */
  if ((mpi->ip == 0) || (mpi->ip == mpi->iproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = 0           ; source[1] = mpi->jp; source[2] = mpi->kp;
    dest[0]   = mpi->iproc-1; dest[1]   = mpi->jp; dest[2]   = mpi->kp;
    limits[0] = (staggered ? 1        : 0     );
    limits[1] = (staggered ? 1+ghosts : ghosts);
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = kmax;
    dim[0]    = imaxu;
    dim[1]    = jmax;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,u,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = imaxu;
    ie = imaxu+ghosts;
    js = boundary->js;
    je = boundary->je;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imaxu ,jmax,kmax,ghosts);
          int pi = Index1D(i-is,j,k,ghosts,jmax,kmax,0     );
          u[pb] = buf[pi];
        }
      }
    }
  }
  /* v */
  if ((mpi->ip == 0) || (mpi->ip == mpi->iproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = 0           ; source[1] = mpi->jp; source[2] = mpi->kp;
    dest[0]   = mpi->iproc-1; dest[1]   = mpi->jp; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = ghosts;
    limits[2] = 0;
    limits[3] = jmaxv;
    limits[4] = 0;
    limits[5] = kmax;
    dim[0]    = imax;
    dim[1]    = jmaxv;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,v,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = imax;
    ie = imax+ghosts;
    js = (staggered ? boundary->jsv : boundary->js);
    je = (staggered ? boundary->jev : boundary->je);
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imax  ,jmaxv,kmax,ghosts);
          int pi = Index1D(i-is,j,k,ghosts,jmaxv,kmax,0     );
          v[pb] = buf[pi];
        }
      }
    }
  }
  /* w */
  if ((mpi->ip == 0) || (mpi->ip == mpi->iproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = 0           ; source[1] = mpi->jp; source[2] = mpi->kp;
    dest[0]   = mpi->iproc-1; dest[1]   = mpi->jp; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = ghosts;
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = kmaxw;
    dim[0]    = imax;
    dim[1]    = jmax;
    dim[2]    = kmaxw;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,w,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = imax;
    ie = imax+ghosts;
    js = boundary->js;
    je = boundary->je;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i   ,j,k,imax  ,jmax,kmaxw,ghosts);
          int pi = Index1D(i-is,j,k,ghosts,jmax,kmaxw,0     );
          w[pb] = buf[pi];
        }
      }
    }
  }

  return(0);
}

int BCVelocityPeriodic_jmin(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  double         *buf      = boundary->buffer;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((jmaxv-jmax) == 1)  staggered = 1;
  else if (jmaxv == jmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityPeriodic_jmin(): jmaxv (%d) and jmax (%d) values are not compatible.\n",
            jmaxv,jmax);
    return(1);
  }

  int i, j, k, is, ie, js, je, ks, ke;
  /* u */
  if ((mpi->jp == 0) || (mpi->jp == mpi->jproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = mpi->jproc-1; source[2] = mpi->kp;
    dest[0]   = mpi->ip; dest[1]   = 0           ; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = imaxu;
    limits[2] = jmax-ghosts;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = kmax;
    dim[0]    = imaxu;
    dim[1]    = jmax;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,u,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = (staggered ? boundary->isu : boundary->is);
    ie = (staggered ? boundary->ieu : boundary->ie);
    js = -ghosts;
    je = 0;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imaxu,jmax  ,kmax,ghosts);
          int pi = Index1D(i,j-js,k,imaxu,ghosts,kmax,0     );
          u[pb] = buf[pi];
        }
      }
    }
  }
  /* v */
  if ((mpi->jp == 0) || (mpi->jp == mpi->jproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = mpi->jproc-1; source[2] = mpi->kp;
    dest[0]   = mpi->ip; dest[1]   = 0           ; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = (staggered ? jmaxv-1-ghosts : jmax-ghosts);
    limits[3] = (staggered ? jmaxv-1        : jmax       );
    limits[4] = 0;
    limits[5] = kmax;
    dim[0]    = imax;
    dim[1]    = jmaxv;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,v,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = -ghosts;
    je = 0;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imax,jmaxv ,kmax,ghosts);
          int pi = Index1D(i,j-js,k,imax,ghosts,kmax,0     );
          v[pb] = buf[pi];
        }
      }
    }
  }
  /* w */
  if ((mpi->jp == 0) || (mpi->jp == mpi->jproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = mpi->jproc-1; source[2] = mpi->kp;
    dest[0]   = mpi->ip; dest[1]   = 0           ; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = jmax-ghosts;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = kmaxw;
    dim[0]    = imax;
    dim[1]    = jmax;
    dim[2]    = kmaxw;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,w,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = -ghosts;
    je = 0;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imax,jmax  ,kmaxw,ghosts);
          int pi = Index1D(i,j-js,k,imax,ghosts,kmaxw,0     );
          w[pb] = buf[pi];
        }
      }
    }
  }

  return(0);
}

int BCVelocityPeriodic_jmax(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  double         *buf      = boundary->buffer;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((jmaxv-jmax) == 1)  staggered = 1;
  else if (jmaxv == jmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityPeriodic_jmax(): jmaxv (%d) and jmax (%d) values are not compatible.\n",
            jmaxv,jmax);
    return(1);
  }

  int i, j, k, is, ie, js, je, ks, ke;
  /* u */
  if ((mpi->jp == 0) || (mpi->jp == mpi->jproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = 0           ; source[2] = mpi->kp;
    dest[0]   = mpi->ip; dest[1]   = mpi->jproc-1; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = imaxu;
    limits[2] = 0;
    limits[3] = ghosts;
    limits[4] = 0;
    limits[5] = kmax;
    dim[0]    = imaxu;
    dim[1]    = jmax;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,u,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = (staggered ? boundary->isu : boundary->is);
    ie = (staggered ? boundary->ieu : boundary->ie);
    js = jmax;
    je = jmax+ghosts;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imaxu,jmax  ,kmax,ghosts);
          int pi = Index1D(i,j-js,k,imaxu,ghosts,kmax,0     );
          u[pb] = buf[pi];
        }
      }
    }
  }
  /* v */
  if ((mpi->jp == 0) || (mpi->jp == mpi->jproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = 0           ; source[2] = mpi->kp;
    dest[0]   = mpi->ip; dest[1]   = mpi->jproc-1; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = (staggered ? 1        : 0     );
    limits[3] = (staggered ? 1+ghosts : ghosts);
    limits[4] = 0;
    limits[5] = kmax;
    dim[0]    = imax;
    dim[1]    = jmaxv;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,v,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = jmaxv;
    je = jmaxv+ghosts;
    ks = boundary->ks;
    ke = boundary->ke;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imax,jmaxv ,kmax,ghosts);
          int pi = Index1D(i,j-js,k,imax,ghosts,kmax,0     );
          v[pb] = buf[pi];
        }
      }
    }
  }
  /* w */
  if ((mpi->jp == 0) || (mpi->jp == mpi->jproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = 0           ; source[2] = mpi->kp;
    dest[0]   = mpi->ip; dest[1]   = mpi->jproc-1; dest[2]   = mpi->kp;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = ghosts;
    limits[4] = 0;
    limits[5] = kmaxw;
    dim[0]    = imax;
    dim[1]    = jmax;
    dim[2]    = kmaxw;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,w,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = jmax;
    je = jmax+ghosts;
    ks = (staggered ? boundary->ksw : boundary->ks);
    ke = (staggered ? boundary->kew : boundary->ke);
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j   ,k,imax,jmax  ,kmaxw,ghosts);
          int pi = Index1D(i,j-js,k,imax,ghosts,kmaxw,0     );
          w[pb] = buf[pi];
        }
      }
    }
  }

  return(0);
}

int BCVelocityPeriodic_kmin(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  double         *buf      = boundary->buffer;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((kmaxw-kmax) == 1)  staggered = 1;
  else if (kmaxw == kmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityPeriodic_kmin(): kmaxw (%d) and kmax (%d) values are not compatible.\n",
            kmaxw,kmax);
    return(1);
  }

  int i, j, k, is, ie, js, je, ks, ke;
  /* u */
  if ((mpi->kp == 0) || (mpi->kp == mpi->kproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = mpi->jp; source[2] = mpi->kproc-1;
    dest[0]   = mpi->ip; dest[1]   = mpi->jp; dest[2]   = 0           ;
    limits[0] = 0;
    limits[1] = imaxu;
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = kmax-ghosts;
    limits[5] = kmax;
    dim[0]    = imaxu;
    dim[1]    = jmax;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,u,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = (staggered ? boundary->isu : boundary->is);
    ie = (staggered ? boundary->ieu : boundary->ie);
    js = boundary->js;
    je = boundary->je;
    ks = -ghosts;
    ke = 0;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k   ,imaxu,jmax,kmax  ,ghosts);
          int pi = Index1D(i,j,k-ks,imaxu,jmax,ghosts,0     );
          u[pb] = buf[pi];
        }
      }
    }
  }
  /* v */
  if ((mpi->kp == 0) || (mpi->kp == mpi->kproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = mpi->jp; source[2] = mpi->kproc-1;
    dest[0]   = mpi->ip; dest[1]   = mpi->jp; dest[2]   = 0           ;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = jmaxv;
    limits[4] = kmax-ghosts;
    limits[5] = kmax;
    dim[0]    = imax;
    dim[1]    = jmaxv;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,v,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = (staggered ? boundary->jsv : boundary->js);
    je = (staggered ? boundary->jev : boundary->je);
    ks = -ghosts;
    ke = 0;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k   ,imax,jmaxv,kmax  ,ghosts);
          int pi = Index1D(i,j,k-ks,imax,jmaxv,ghosts,0     );
          v[pb] = buf[pi];
        }
      }
    }
  }
  /* w */
  if ((mpi->kp == 0) || (mpi->kp == mpi->kproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = mpi->jp; source[2] = mpi->kproc-1;
    dest[0]   = mpi->ip; dest[1]   = mpi->jp; dest[2]   = 0           ;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = (staggered ? kmaxw-1-ghosts : kmax-ghosts);
    limits[5] = (staggered ? kmaxw-1        : kmax);
    dim[0]    = imax;
    dim[1]    = jmax;
    dim[2]    = kmaxw;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,w,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = boundary->js;
    je = boundary->je;
    ks = -ghosts;
    ke = 0;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k   ,imax,jmax,kmaxw ,ghosts);
          int pi = Index1D(i,j,k-ks,imax,jmax,ghosts,0     );
          w[pb] = buf[pi];
        }
      }
    }
  }

  return(0);
}

int BCVelocityPeriodic_kmax(int *dim, double *u,double *v,double *w,void *b,void *m,int delta)
{
  DomainBoundary *boundary = (DomainBoundary*) b;
  MPIVariables   *mpi      = (MPIVariables*) m;
  double         *buf      = boundary->buffer;
  int             ierr     = 0;

  int imax    = dim[0];
  int jmax    = dim[1];
  int kmax    = dim[2];
  int ghosts  = dim[3];
  int imaxu   = dim[4];
  int jmaxv   = dim[5];
  int kmaxw   = dim[6];

  int staggered;
  if ((kmaxw-kmax) == 1)  staggered = 1;
  else if (kmaxw == kmax) staggered = 0;
  else {
    fprintf(stderr,"Error in BCVelocityPeriodic_kmax(): kmaxw (%d) and kmax (%d) values are not compatible.\n",
            kmaxw,kmax);
    return(1);
  }

  int i, j, k, is, ie, js, je, ks, ke;
  /* u */
  if ((mpi->kp == 0) || (mpi->kp == mpi->kproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = mpi->jp; source[2] = 0           ;
    dest[0]   = mpi->ip; dest[1]   = mpi->jp; dest[2]   = mpi->kproc-1;
    limits[0] = 0;
    limits[1] = imaxu;
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = 0;
    limits[5] = ghosts;
    dim[0]    = imaxu;
    dim[1]    = jmax;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,u,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = (staggered ? boundary->isu : boundary->is);
    ie = (staggered ? boundary->ieu : boundary->ie);
    js = boundary->js;
    je = boundary->je;
    ks = kmax;
    ke = kmax+ghosts;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k   ,imaxu,jmax,kmax  ,ghosts);
          int pi = Index1D(i,j,k-ks,imaxu,jmax,ghosts,0     );
          u[pb] = buf[pi];
        }
      }
    }
  }
  /* v */
  if ((mpi->kp == 0) || (mpi->kp == mpi->kproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = mpi->jp; source[2] = 0           ;
    dest[0]   = mpi->ip; dest[1]   = mpi->jp; dest[2]   = mpi->kproc-1;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = jmaxv;
    limits[4] = 0;
    limits[5] = ghosts;
    dim[0]    = imax;
    dim[1]    = jmaxv;
    dim[2]    = kmax;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,v,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = (staggered ? boundary->jsv : boundary->js);
    je = (staggered ? boundary->jev : boundary->je);
    ks = kmax;
    ke = kmax+ghosts;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k   ,imax,jmaxv,kmax  ,ghosts);
          int pi = Index1D(i,j,k-ks,imax,jmaxv,ghosts,0     );
          v[pb] = buf[pi];
        }
      }
    }
  }
  /* w */
  if ((mpi->kp == 0) || (mpi->kp == mpi->kproc-1)) {
    int source[3],dest[3],limits[6],dim[4];
    source[0] = mpi->ip; source[1] = mpi->jp; source[2] = 0           ;
    dest[0]   = mpi->ip; dest[1]   = mpi->jp; dest[2]   = mpi->kproc-1;
    limits[0] = 0;
    limits[1] = imax;
    limits[2] = 0;
    limits[3] = jmax;
    limits[4] = (staggered ? 1        : 0     );
    limits[5] = (staggered ? 1+ghosts : ghosts);
    dim[0]    = imax;
    dim[1]    = jmax;
    dim[2]    = kmaxw;
    dim[3]    = ghosts;
    ierr = MPIGetArrayData3D(buf,w,&source[0],&dest[0],&limits[0],&dim[0],mpi);
    if (ierr) return(ierr);
  }
  if (boundary->on_this_proc) {
    is = boundary->is;
    ie = boundary->ie;
    js = boundary->js;
    je = boundary->je;
    ks = kmaxw;
    ke = kmaxw+ghosts;
    for (i = is; i < ie; i++) {
      for (j = js; j < je; j++) {
        for (k = ks; k < ke; k++) {
          int pb = Index1D(i,j,k   ,imax,jmax,kmaxw ,ghosts);
          int pi = Index1D(i,j,k-ks,imax,jmax,ghosts,0     );
          w[pb] = buf[pi];
        }
      }
    }
  }

  return(0);
}

