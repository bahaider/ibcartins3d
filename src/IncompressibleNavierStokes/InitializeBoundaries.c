#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpivars.h>
#include <mathroutines.h>
#include <boundaryconditions.h>
#include <ibcartins3d.h>

static int CalculateLocalExtent(void*,void*);

int InitializeBoundaries(void *s,void *m)
{
  IBCartINS3D     *solver   = (IBCartINS3D*) s;
  MPIVariables    *mpi      = (MPIVariables*) m;
  DomainBoundary  *boundary = NULL;
  int             n,ierr    = 0;

  /* root process reads boundary condition file */
  if (!mpi->rank) {
    printf("Reading boundary conditions from %s.\n",solver->bcinp_filename);
    FILE *in;
    in = fopen(solver->bcinp_filename,"r");
    if (!in) {
      fprintf(stderr,"Error: boundary condition file %s not found.\n",solver->bcinp_filename);
      return(1);
    }

    /* read number of boundary conditions and allocate */
    ierr = fscanf(in,"%d",&solver->nBoundaryZones);
    boundary = (DomainBoundary*) calloc (solver->nBoundaryZones,sizeof(DomainBoundary));

    /* read each boundary condition */
    int n;
    for (n = 0; n < solver->nBoundaryZones; n++) {
      ierr = fscanf(in,"%s",boundary[n].bctype);
      ierr = fscanf(in,"%s",boundary[n].face);
      ierr = fscanf(in,"%lf %lf %lf %lf %lf %lf",
                 &boundary[n].xmin, &boundary[n].xmax,
                 &boundary[n].ymin, &boundary[n].ymax,
                 &boundary[n].zmin, &boundary[n].zmax);
      printf("  Boundary %20s:  %4s",boundary[n].bctype,boundary[n].face);
      if (!strcmp(boundary[n].bctype,_NOSLIP_)) {
        /* read in wall velocity for no-slip BC */
        ierr = fscanf(in,"%lf %lf %lf"  ,&boundary[n].uWall,
                      &boundary[n].vWall,&boundary[n].wWall);
        printf("  Wall velocity: %lf,%lf,%lf",boundary[n].uWall,
               boundary[n].vWall,boundary[n].wWall);
      } else if (!strcmp(boundary[n].bctype,_FREESTREAM_)) {
        /* read in velocity for freestream BC */
        ierr = fscanf(in,"%lf %lf %lf"  ,&boundary[n].uWall,
                      &boundary[n].vWall,&boundary[n].wWall);
        printf("  Freestream velocity: %lf,%lf,%lf",boundary[n].uWall,
               boundary[n].vWall,boundary[n].wWall);
      } else {
        boundary[n].uWall = boundary[n].vWall = boundary[n].wWall = 0.0;
      }
      printf("\n");
    }
    fclose(in);
    printf("%d boundary condition(s) read.\n",solver->nBoundaryZones);
  }

  /* tell other processes how many BCs are there and let them allocate */
  MPIBroadcast_integer(&solver->nBoundaryZones,1,0);
  if (mpi->rank) boundary = (DomainBoundary*) calloc (solver->nBoundaryZones,sizeof(DomainBoundary));
  /* communicate BC data to other processes */
  for (n = 0; n < solver->nBoundaryZones; n++) {
    MPIBroadcast_character(boundary[n].bctype,_MAX_STRING_SIZE_,0);
    MPIBroadcast_character(boundary[n].face  ,_MAX_STRING_SIZE_,0);
    MPIBroadcast_double   (&boundary[n].xmin ,1,0);
    MPIBroadcast_double   (&boundary[n].xmax ,1,0);
    MPIBroadcast_double   (&boundary[n].ymin ,1,0);
    MPIBroadcast_double   (&boundary[n].ymax ,1,0);
    MPIBroadcast_double   (&boundary[n].zmin ,1,0);
    MPIBroadcast_double   (&boundary[n].zmax ,1,0);
    MPIBroadcast_double   (&boundary[n].uWall,1,0);
    MPIBroadcast_double   (&boundary[n].vWall,1,0);
    MPIBroadcast_double   (&boundary[n].wWall,1,0);
  }

  solver->BoundaryZones    = boundary;

  /* each process calculates its own part of these boundaries */
  ierr = CalculateLocalExtent(solver,mpi);
  if (ierr) return(ierr);

  /* initialize function pointers for each boundary condition */
  int dim[4];
  dim[0] = solver->imax_local;
  dim[1] = solver->jmax_local;
  dim[2] = solver->kmax_local;
  dim[3] = solver->ghosts;
  for (n = 0; n < solver->nBoundaryZones; n++) {
    ierr = BCInitialize(&boundary[n],&dim[0]);
    if (ierr) return(ierr);
  }

  return(0);
}


int CalculateLocalExtent(void *s,void *m)
{
  IBCartINS3D     *solver   = (IBCartINS3D*) s;
  MPIVariables    *mpi      = (MPIVariables*) m;
  DomainBoundary  *boundary = solver->BoundaryZones;

  double *x,*y,*z;
  x  = solver->x;
  y  = solver->y;
  z  = solver->z;

  int imax, jmax, kmax;
  imax = solver->imax_local;
  jmax = solver->jmax_local;
  kmax = solver->kmax_local;

  int n;
  for (n = 0; n < solver->nBoundaryZones; n++) {
    if (!strcmp(boundary[n].face,"imin")) {

      if (mpi->ip == 0) {
        int js,je,ks,ke,jsv,jev,ksw,kew;
        FindInterval(boundary[n].ymin,boundary[n].ymax,y ,jmax  ,&js ,&je ); jsv = js; jev = je+1;
        FindInterval(boundary[n].zmin,boundary[n].zmax,z ,kmax  ,&ks ,&ke ); ksw = ks; kew = ke+1;
        boundary[n].js  = js ; boundary[n].je  = je ;
        boundary[n].jsv = jsv; boundary[n].jev = jev;
        boundary[n].ks  = ks ; boundary[n].ke  = ke ;
        boundary[n].ksw = ksw; boundary[n].kew = kew;
        if ((je-js <= 0) && (jev-jsv <= 0) && (ke-ks <= 0) && (kew-ksw <= 0))
              boundary[n].on_this_proc = 0;
        else  boundary[n].on_this_proc = 1;
      } else  boundary[n].on_this_proc = 0;

    } else if (!strcmp(boundary[n].face,"imax")) {

      if (mpi->ip == mpi->iproc-1) {
        int js,je,ks,ke,jsv,jev,ksw,kew;
        FindInterval(boundary[n].ymin,boundary[n].ymax,y ,jmax  ,&js ,&je ); jsv = js; jev = je+1;
        FindInterval(boundary[n].zmin,boundary[n].zmax,z ,kmax  ,&ks ,&ke ); ksw = ks; kew = ke+1;
        boundary[n].js  = js ; boundary[n].je  = je ;
        boundary[n].jsv = jsv; boundary[n].jev = jev;
        boundary[n].ks  = ks ; boundary[n].ke  = ke ;
        boundary[n].ksw = ksw; boundary[n].kew = kew;
        if ((je-js <= 0) && (jev-jsv <= 0) && (ke-ks <= 0) && (kew-ksw <= 0))
              boundary[n].on_this_proc = 0;
        else  boundary[n].on_this_proc = 1;
      } else  boundary[n].on_this_proc = 0;

    } else if (!strcmp(boundary[n].face,"jmin")) {

      if (mpi->jp == 0) {
        int is,ie,ks,ke,isu,ieu,ksw,kew;
        FindInterval(boundary[n].xmin,boundary[n].xmax,x ,imax  ,&is ,&ie ); isu = is; ieu = ie+1;
        FindInterval(boundary[n].zmin,boundary[n].zmax,z ,kmax  ,&ks ,&ke ); ksw = ks; kew = ke+1;
        boundary[n].is  = is ; boundary[n].ie  = ie ;
        boundary[n].isu = isu; boundary[n].ieu = ieu;
        boundary[n].ks  = ks ; boundary[n].ke  = ke ;
        boundary[n].ksw = ksw; boundary[n].kew = kew;
        if ((ie-is <= 0) && (ieu-isu <= 0) && (ke-ks <= 0) && (kew-ksw <= 0))
              boundary[n].on_this_proc = 0;
        else  boundary[n].on_this_proc = 1;
      } else  boundary[n].on_this_proc = 0;

    } else if (!strcmp(boundary[n].face,"jmax")) {

      if (mpi->jp == mpi->jproc-1) {
        int is,ie,ks,ke,isu,ieu,ksw,kew;
        FindInterval(boundary[n].xmin,boundary[n].xmax,x ,imax  ,&is ,&ie ); isu = is; ieu = ie+1;
        FindInterval(boundary[n].zmin,boundary[n].zmax,z ,kmax  ,&ks ,&ke ); ksw = ks; kew = ke+1;
        boundary[n].is  = is ; boundary[n].ie  = ie ;
        boundary[n].isu = isu; boundary[n].ieu = ieu;
        boundary[n].ks  = ks ; boundary[n].ke  = ke ;
        boundary[n].ksw = ksw; boundary[n].kew = kew;
        if ((ie-is <= 0) && (ieu-isu <= 0) && (ke-ks <= 0) && (kew-ksw <= 0))
              boundary[n].on_this_proc = 0;
        else  boundary[n].on_this_proc = 1;
      } else  boundary[n].on_this_proc = 0;

    } else if (!strcmp(boundary[n].face,"kmin")) {

      if (mpi->kp == 0) {
        int is,ie,js,je,isu,ieu,jsv,jev;
        FindInterval(boundary[n].xmin,boundary[n].xmax,x ,imax  ,&is ,&ie ); isu = is; ieu = ie+1;
        FindInterval(boundary[n].ymin,boundary[n].ymax,y ,jmax  ,&js ,&je ); jsv = js; jev = je+1;
        boundary[n].is  = is ; boundary[n].ie  = ie ;
        boundary[n].isu = isu; boundary[n].ieu = ieu;
        boundary[n].js  = js ; boundary[n].je  = je ;
        boundary[n].jsv = jsv; boundary[n].jev = jev;
        if ((ie-is <= 0) && (ieu-isu <= 0) && (je-js <= 0) && (jev-jsv <= 0))
              boundary[n].on_this_proc = 0;
        else  boundary[n].on_this_proc = 1;
      } else  boundary[n].on_this_proc = 0;

    } else if (!strcmp(boundary[n].face,"kmax")) {

      if (mpi->kp == mpi->kproc-1) {
        int is,ie,js,je,isu,ieu,jsv,jev;
        FindInterval(boundary[n].xmin,boundary[n].xmax,x ,imax  ,&is ,&ie ); isu = is; ieu = ie+1;
        FindInterval(boundary[n].ymin,boundary[n].ymax,y ,jmax  ,&js ,&je ); jsv = js; jev = je+1;
        boundary[n].is  = is ; boundary[n].ie  = ie ;
        boundary[n].isu = isu; boundary[n].ieu = ieu;
        boundary[n].js  = js ; boundary[n].je  = je ;
        boundary[n].jsv = jsv; boundary[n].jev = jev;
        if ((ie-is <= 0) && (ieu-isu <= 0) && (je-js <= 0) && (jev-jsv <= 0))
              boundary[n].on_this_proc = 0;
        else  boundary[n].on_this_proc = 1;
      } else  boundary[n].on_this_proc = 0;

    } else    boundary[n].on_this_proc = 0;
  }

  return(0);
}
