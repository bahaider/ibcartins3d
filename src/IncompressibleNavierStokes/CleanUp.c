#include <stdlib.h>
#include <ibcartins3d.h>
#include <immersedbody.h>
#include <boundaryconditions.h>
#include <linearsolver.h>
#include <interpolation.h>

int CleanUp(void* s)
{
  IBCartINS3D  *solver = (IBCartINS3D*) s;
  ImmersedBody *ib     = (ImmersedBody*) solver->body;
  int           ierr   = 0;

  if (ib->is_present) {
    if (solver->nboundary  > 0) free(solver->immersed_boundary ); /* Allocated in InitializeImmersedBody.c */
    if (solver->nboundaryu > 0) free(solver->immersed_boundaryu); /* Allocated in InitializeImmersedBody.c */
    if (solver->nboundaryv > 0) free(solver->immersed_boundaryv); /* Allocated in InitializeImmersedBody.c */
    if (solver->nboundaryw > 0) free(solver->immersed_boundaryw); /* Allocated in InitializeImmersedBody.c */
  }
  ierr = IBCleanUp(solver->body);
  if (ierr) return(ierr);

  if (solver->nBoundaryZones > 0) {
    int n;
    for (n = 0; n < solver->nBoundaryZones; n++) {
      ierr = BCCleanUp(&((DomainBoundary*)solver->BoundaryZones)[n]); 
      if (ierr) return(ierr);
    }
    free(solver->BoundaryZones);    /* Allocated in InitializeBoundaries() */
  }

  ierr = LSCleanUp(solver->predictorLSu); if(ierr) return(ierr);
  ierr = LSCleanUp(solver->predictorLSv); if(ierr) return(ierr);
  ierr = LSCleanUp(solver->predictorLSw); if(ierr) return(ierr);
  ierr = LSCleanUp(solver->correctorLS ); if(ierr) return(ierr);
  free(solver->predictorLSu);  /* Allocated in InitializeSolvers() */
  free(solver->predictorLSv);  /* Allocated in InitializeSolvers() */
  free(solver->predictorLSw);  /* Allocated in InitializeSolvers() */
  free(solver->correctorLS );  /* Allocated in InitializeSolvers() */

  ierr = InterpCleanUp(solver->ReconstructParams); if(ierr) return(ierr);
  free(solver->ReconstructParams);  /* Allocated in InitializeSolvers() */

  free(solver->iblanku); /* Allocated in Initialize() */
  free(solver->iblankv); /* Allocated in Initialize() */
  free(solver->iblankw); /* Allocated in Initialize() */
  free(solver->iblank ); /* Allocated in Initialize() */
  free(solver->iblankc); /* Allocated in Initialize() */

  free(solver->body);       /* Allocated in InitializeImmersedBody.c */
  free(solver->xg  );        /* Allocated in Initialize.c             */
  free(solver->xgu );        /* Allocated in Initialize.c             */
  free(solver->yg  );        /* Allocated in Initialize.c             */
  free(solver->ygv );        /* Allocated in Initialize.c             */
  free(solver->zg  );        /* Allocated in Initialize.c             */
  free(solver->zgw );        /* Allocated in Initialize.c             */
  free(solver->x   );        /* Allocated in Initialize.c             */
  free(solver->xu  );        /* Allocated in Initialize.c             */
  free(solver->y   );        /* Allocated in Initialize.c             */
  free(solver->yv  );        /* Allocated in Initialize.c             */
  free(solver->z   );        /* Allocated in Initialize.c             */
  free(solver->zw  );        /* Allocated in Initialize.c             */
  free(solver->u   );        /* Allocated in Initialize.c             */
  free(solver->v   );        /* Allocated in Initialize.c             */
  free(solver->w   );        /* Allocated in Initialize.c             */
  free(solver->uc  );        /* Allocated in Initialize.c             */
  free(solver->vc  );        /* Allocated in Initialize.c             */
  free(solver->wc  );        /* Allocated in Initialize.c             */
  free(solver->phi );        /* Allocated in Initialize.c             */
  free(solver->phic);        /* Allocated in Initialize.c             */
  free(solver->rhs_u);      /* Allocated in Initialize.c             */
  free(solver->rhs_v);      /* Allocated in Initialize.c             */
  free(solver->rhs_w);      /* Allocated in Initialize.c             */
  free(solver->divergence); /* Allocated in Initialize.c             */
  free(solver->du);         /* Allocated in Initialize.c             */
  free(solver->dv);         /* Allocated in Initialize.c             */
  free(solver->dw);         /* Allocated in Initialize.c             */
  free(solver->prev_du);    /* Allocated in Initialize.c             */
  free(solver->prev_dv);    /* Allocated in Initialize.c             */
  free(solver->prev_dw);    /* Allocated in Initialize.c             */
  free(solver->uI);         /* Allocated in Initialize.c             */
  free(solver->vI);         /* Allocated in Initialize.c             */
  free(solver->wI);         /* Allocated in Initialize.c             */
  free(solver->qC);         /* Allocated in Initialize.c             */
  free(solver->qI);         /* Allocated in Initialize.c             */
  free(solver->up);         /* Allocated in Initialize.c             */

  return(0);
}
