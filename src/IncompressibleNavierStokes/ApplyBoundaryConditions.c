#include <arrayfunctions.h>
#include <mpivars.h>
#include <boundaryconditions.h>
#include <immersedbody.h>
#include <ibcartins3d.h>

int ApplyBoundaryConditionsPressure(int *dim,double *p,void *s,
                                    void *m,int delta,int immersedflag) 
{
  IBCartINS3D     *solver   = (IBCartINS3D*)    s;
  DomainBoundary  *boundary = (DomainBoundary*) solver->BoundaryZones;
  ImmersedBody    *body     = (ImmersedBody*)   solver->body;
  MPIVariables    *mpi      = (MPIVariables*)   m;
  int             nb        = solver->nBoundaryZones;
  int             ierr      = 0;

  int offset[3];
  offset[0] = mpi->is;
  offset[1] = mpi->js;
  offset[2] = mpi->ks;

  /* Apply domain boundary conditions to p */
  int n;
  for (n = 0; n < nb; n++) {
    ierr = boundary[n].BCFunctionPressure(dim,p,&boundary[n],mpi,delta);
    if (ierr) return(ierr);
  }

  /* Apply immersed boundary conditions to p */
  if (body->is_present && immersedflag) {
    /* first, set values at all points inside the boundary to zero */
    ierr = Array3DElementMultiply(solver->imax_local,solver->jmax_local,solver->kmax_local,
                                  solver->ghosts,solver->ghosts,solver->iblank,p);
    if (ierr) return(ierr);

    /* then apply solid wall BCs to boundary points                */
    int     nb, dimensions[7];
    void    *ib;
    double  *x,*y,*z,*blank;
    nb        = solver->nboundary;
    dimensions[0]    = solver->imax_local;     dimensions[4] = solver->imax_global;
    dimensions[1]    = solver->jmax_local;     dimensions[5] = solver->jmax_global;
    dimensions[2]    = solver->kmax_local;     dimensions[6] = solver->kmax_global;
    dimensions[3]    = solver->ghosts;
    ib        = solver->immersed_boundary;
    x         = solver->xg;
    y         = solver->yg;
    z         = solver->zg;
    blank     = solver->iblank;
    ierr      = IBApplyBC(nb,&dimensions[0],&offset[0],ib,x,y,z,p,blank,_EXTRP_);
    if (ierr) return(ierr);

  }

  return(0);
}

int ApplyBoundaryConditionsVelocity(int *dim,double *u,double *v,double *w,
                                    void *s,void *m,int delta,int immersedflag)
{
  IBCartINS3D     *solver   = (IBCartINS3D*)    s;
  DomainBoundary  *boundary = (DomainBoundary*) solver->BoundaryZones;
  ImmersedBody    *body     = (ImmersedBody*)   solver->body;
  MPIVariables    *mpi      = (MPIVariables*)   m;
  int             nb        = solver->nBoundaryZones;
  int             ierr      = 0;

  int offset[3];
  offset[0] = mpi->is;
  offset[1] = mpi->js;
  offset[2] = mpi->ks;

  /* Apply domain boundary conditions to u,v,w */
  int n;
  for (n = 0; n < nb; n++) {
    ierr = boundary[n].BCFunctionVelocity(dim,u,v,w,&boundary[n],mpi,delta);
    if (ierr) return(ierr);
  }

  /* Apply immersed boundary conditions to u,v,w */
  if (body->is_present && immersedflag) {
    /* first, set values at all points inside the boundary to zero */
    ierr = Array3DElementMultiply(solver->imax_local+1,solver->jmax_local,solver->kmax_local,
                                  solver->ghosts,solver->ghosts,solver->iblanku,u);
    if (ierr) return(ierr);
    ierr = Array3DElementMultiply(solver->imax_local,solver->jmax_local+1,solver->kmax_local,
                                  solver->ghosts,solver->ghosts,solver->iblankv,v);
    if (ierr) return(ierr);
    ierr = Array3DElementMultiply(solver->imax_local,solver->jmax_local,solver->kmax_local+1,
                                  solver->ghosts,solver->ghosts,solver->iblankw,w);
    if (ierr) return(ierr);

    /* then apply solid wall BCs to boundary points                */
    int     nb, dimensions[7];
    void    *ib;
    double  *x,*y,*z,*blank;

    /* u */
    nb        = solver->nboundaryu;
    dimensions[0]    = solver->imax_local + 1; dimensions[4] = solver->imax_global + 1;
    dimensions[1]    = solver->jmax_local;     dimensions[5] = solver->jmax_global;
    dimensions[2]    = solver->kmax_local;     dimensions[6] = solver->kmax_global;
    dimensions[3]    = solver->ghosts;
    ib        = solver->immersed_boundaryu;
    x         = solver->xgu;
    y         = solver->yg;
    z         = solver->zg;
    blank     = solver->iblanku;
    ierr      = IBApplyBC(nb,&dimensions[0],&offset[0],ib,x,y,z,u,blank,_REFLECT_);
    if (ierr) return(ierr);

    /* v */
    nb        = solver->nboundaryv;
    dimensions[0]    = solver->imax_local;     dimensions[4] = solver->imax_global;
    dimensions[1]    = solver->jmax_local + 1; dimensions[5] = solver->jmax_global + 1;
    dimensions[2]    = solver->kmax_local;     dimensions[6] = solver->kmax_global;
    dimensions[3]    = solver->ghosts;
    ib        = solver->immersed_boundaryv;
    x         = solver->xg;
    y         = solver->ygv;
    z         = solver->zg;
    blank     = solver->iblankv;
    ierr      = IBApplyBC(nb,&dimensions[0],&offset[0],ib,x,y,z,v,blank,_REFLECT_);
    if (ierr) return(ierr);

    /* w */
    nb        = solver->nboundaryw;
    dimensions[0]    = solver->imax_local;     dimensions[4] = solver->imax_global;
    dimensions[1]    = solver->jmax_local;     dimensions[5] = solver->jmax_global;
    dimensions[2]    = solver->kmax_local + 1; dimensions[6] = solver->kmax_global + 1;
    dimensions[3]    = solver->ghosts;
    ib        = solver->immersed_boundaryw;
    x         = solver->xg;
    y         = solver->yg;
    z         = solver->zgw;
    blank     = solver->iblankw;
    ierr      = IBApplyBC(nb,&dimensions[0],&offset[0],ib,x,y,z,w,blank,_REFLECT_);
    if (ierr) return(ierr);

  }

  return(0);
}
