#include <stdlib.h>
#include <mpivars.h>
#include <arrayfunctions.h>
#include <linearsolver.h>
#include <ibcartins3d.h>

/* Function declarations */
int ApplyBoundaryConditionsVelocity  (int*,double*,double*,double*,void*,void*,int,int);

int Predictor(void *s,void *m,double *du,double *dv,double *dw)
{
  IBCartINS3D   *solver = (IBCartINS3D*) s;
  MPIVariables  *mpi    = (MPIVariables*) m;
  LinearSolver  *LSu    = (LinearSolver*) solver->predictorLSu;
  LinearSolver  *LSv    = (LinearSolver*) solver->predictorLSv;
  LinearSolver  *LSw    = (LinearSolver*) solver->predictorLSw;
  int           ierr    = 0, dimensions[7];

  int imax   = solver->imax_local;
  int jmax   = solver->jmax_local;
  int kmax   = solver->kmax_local;
  int ghosts = solver->ghosts;

  /* allocate right-hand side arrays */
  double *rhs_u = solver->rhs_u;
  double *rhs_v = solver->rhs_v;
  double *rhs_w = solver->rhs_w;

  /* initialize du,dv,dw to zero */
  int size_u = (imax+1+2*ghosts) * (jmax  +2*ghosts) * (kmax  +2*ghosts);
  int size_v = (imax  +2*ghosts) * (jmax+1+2*ghosts) * (kmax  +2*ghosts);
  int size_w = (imax  +2*ghosts) * (jmax  +2*ghosts) * (kmax+1+2*ghosts);
  ierr = SetArrayValue(du,size_u,0); if(ierr) return(ierr);
  ierr = SetArrayValue(dv,size_v,0); if(ierr) return(ierr);
  ierr = SetArrayValue(dw,size_w,0); if(ierr) return(ierr);

  /* initialize right-hand side to zero */
  ierr = SetArrayValue(rhs_u,(imax+1)*jmax*kmax,0.0); if(ierr) return(ierr);
  ierr = SetArrayValue(rhs_v,imax*(jmax+1)*kmax,0.0); if(ierr) return(ierr);
  ierr = SetArrayValue(rhs_w,imax*jmax*(kmax+1),0.0); if(ierr) return(ierr);

  /* Apply boundary conditions */
  dimensions[0] = imax;  dimensions[4] = dimensions[0] + 1;
  dimensions[1] = jmax;  dimensions[5] = dimensions[1] + 1;
  dimensions[2] = kmax;  dimensions[6] = dimensions[2] + 1;
  dimensions[3] = ghosts;
  ierr = ApplyBoundaryConditionsVelocity(&dimensions[0],solver->u,solver->v,solver->w,
                                         solver,mpi,0,1);
  if (ierr) return(ierr);

  /* exchange data over MPI boundaries */
  ierr = MPIExchangeBoundaries(imax+1,jmax  ,kmax  ,ghosts,1,0,0,mpi,solver->u); 
  if (ierr) return(ierr);
  ierr = MPIExchangeBoundaries(imax  ,jmax+1,kmax  ,ghosts,0,1,0,mpi,solver->v); 
  if (ierr) return(ierr);
  ierr = MPIExchangeBoundaries(imax  ,jmax  ,kmax+1,ghosts,0,0,1,mpi,solver->w); 
  if (ierr) return(ierr);

  /* calculate the convection and diffusion terms */
  ierr = solver->Convection(solver,mpi,solver->u,solver->v,solver->w,
                            rhs_u,rhs_v,rhs_w); if(ierr) return(ierr);
  ierr = solver->Diffusion (solver,mpi,solver->u,solver->v,solver->w,
                            rhs_u,rhs_v,rhs_w); if(ierr) return(ierr);

  /* Multiply right hand side by iblank arrays */
  ierr = Array3DElementMultiply(imax+1,jmax,kmax,ghosts,0,solver->iblanku,rhs_u); if(ierr) return(ierr);
  ierr = Array3DElementMultiply(imax,jmax+1,kmax,ghosts,0,solver->iblankv,rhs_v); if(ierr) return(ierr);
  ierr = Array3DElementMultiply(imax,jmax,kmax+1,ghosts,0,solver->iblankw,rhs_w); if(ierr) return(ierr);

  if (solver->mu == 0) {
    /* inviscid case */
    ierr = CopyArrayGhost1ToGhost2(imax+1,jmax  ,kmax  ,0,ghosts,rhs_u,du); if(ierr) return(ierr);
    ierr = CopyArrayGhost1ToGhost2(imax  ,jmax+1,kmax  ,0,ghosts,rhs_v,dv); if(ierr) return(ierr);
    ierr = CopyArrayGhost1ToGhost2(imax  ,jmax  ,kmax+1,0,ghosts,rhs_w,dw); if(ierr) return(ierr);
  } else {
    int ushift[3] = {1,0,0};
    int vshift[3] = {0,1,0};
    int wshift[3] = {0,0,1};
    /* viscous case -> solve system */
    dimensions[0] = imax + 1;
    dimensions[1] = jmax;
    dimensions[2] = kmax;
    dimensions[3] = solver->ghosts;
    ierr = LSu->Solver(LSu,mpi,du,rhs_u,&dimensions[0],&ushift[0],NULL);
    if (ierr) return(ierr);
    dimensions[0] = imax;
    dimensions[1] = jmax + 1;
    dimensions[2] = kmax;
    dimensions[3] = solver->ghosts;
    ierr = LSv->Solver(LSv,mpi,dv,rhs_v,&dimensions[0],&vshift[0],NULL);
    if (ierr) return(ierr);
    dimensions[0] = imax;
    dimensions[1] = jmax;
    dimensions[2] = kmax + 1;
    dimensions[3] = solver->ghosts;
    ierr = LSw->Solver(LSw,mpi,dw,rhs_w,&dimensions[0],&wshift[0],NULL);
    if (ierr) return(ierr);
  }

  /* Multiply du,dv,dw by iblank arrays */
  ierr = Array3DElementMultiply(imax+1,jmax,kmax,ghosts,ghosts,solver->iblanku,du); if(ierr) return(ierr);
  ierr = Array3DElementMultiply(imax,jmax+1,kmax,ghosts,ghosts,solver->iblankv,dv); if(ierr) return(ierr);
  ierr = Array3DElementMultiply(imax,jmax,kmax+1,ghosts,ghosts,solver->iblankw,dw); if(ierr) return(ierr);

  return(0);
}
