#include <stdlib.h>
#include <arrayfunctions.h>
#include <mpivars.h>
#include <interpolation.h>
#include <ibcartins3d.h>

int ConvectionConservative(void *s,void *m,double *u,double *v,double *w,
                           double *rhs_u,double *rhs_v,double *rhs_w)
{
  IBCartINS3D             *solver = (IBCartINS3D*) s;
  MPIVariables            *mpi    = (MPIVariables*) m;
  InterpolationParameters *interp = (InterpolationParameters*) solver->ReconstructParams;

  int imax   = solver->imax_local; int imaxu = imax+1;
  int jmax   = solver->jmax_local; int jmaxv = jmax+1;
  int kmax   = solver->kmax_local; int kmaxw = kmax+1;
  int ghosts = solver->ghosts;
  int is     = mpi->is; int ip = mpi->ip; int iproc = mpi->iproc;
  int js     = mpi->js; int jp = mpi->jp; int jproc = mpi->jproc;
  int ks     = mpi->ks; int kp = mpi->kp; int kproc = mpi->kproc;

  int i,j,k,ierr = 0;
  double *uI = solver->uI;
  double *vI = solver->vI;
  double *wI = solver->wI;
  double *qC = solver->qC;
  double *qI = solver->qI;
  double *up = solver->up;

  /* u-velocity */

  for (j = 0; j < jmax; j++) {
    for (k = 0; k < kmax; k++) {
      for (i = -ghosts; i < imaxu+ghosts; i++) {
        int p = Index1D(i,j,k,imaxu,jmax,kmax,ghosts);
        qC[i+ghosts] = u[p];
      }
      for (i = 0; i < imaxu+1; i++) {
        int pL = Index1D(i-1,j,k,imaxu,jmax,kmax,ghosts);
        int pR = Index1D(i  ,j,k,imaxu,jmax,kmax,ghosts);
        if      ((u[pL] > 0) && (u[pR] > 0)) up[i] =  1.0;
        else if ((u[pL] < 0) && (u[pR] < 0)) up[i] = -1.0;
        else                                 up[i] =  0.0;
      }
      interp->upw = up;
      ierr = solver->ReconstructNormal(qI,qC,imaxu,ghosts,mpi->ip,mpi->iproc,interp);
      if (ierr) return(ierr);
      for (i = 0; i < imaxu+1; i++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        uI[p] = qI[i];
      }
    }
  }
  for (i = 0; i < imaxu; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p  = Index1D(i  ,j,k,imaxu ,jmax  ,kmax  ,0);
        int qR = Index1D(i+1,j,k,imax+2,jmax+2,kmax+2,0);
        int qL = Index1D(i  ,j,k,imax+2,jmax+2,kmax+2,0);
        double dxinv;
        if ((i == 0) && (ip == 0)) 
          dxinv = 1.0 / (solver->xgu[is+i+1] - solver->xgu[is+i]);
        else if ((i == imaxu-1) && (ip == iproc-1)) 
          dxinv = 1.0 / (solver->xgu[is+i] - solver->xgu[is+i-1]);
        else 
          dxinv = 2.0 / (solver->xgu[is+i+1] - solver->xgu[is+i-1]);
        rhs_u[p] -= (uI[qR]*uI[qR] - uI[qL]*uI[qL]) * dxinv;
      }
    }
  }

  for (i = 0; i < imaxu; i++) {
    for (k = 0; k < kmax; k++) {
      for (j = -ghosts; j < jmax+ghosts; j++) {
        int p = Index1D(i,j,k,imaxu,jmax,kmax,ghosts);
        qC[j+ghosts] = u[p];
      }
      for (j = 0; j < jmax+1; j++) {
        int pL = Index1D(i-1,j,k,imax,jmaxv,kmax,ghosts);
        int pR = Index1D(i  ,j,k,imax,jmaxv,kmax,ghosts);
        if      ((v[pL] > 0) && (v[pR] > 0)) up[j] =  1.0;
        else if ((v[pR] < 0) && (v[pR] < 0)) up[j] = -1.0;
        else                                 up[j] =  0.0;
      }
      interp->upw = up;
      ierr = solver->ReconstructNormal(qI,qC,jmax,ghosts,mpi->jp,mpi->jproc,interp);
      if (ierr) return(ierr);
      for (j = 0; j < jmax+1; j++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        uI[p] = qI[j];
      }
    }
  }
  for (j = 0; j < jmaxv; j++) {
    for (k = 0; k < kmax; k++) {
      for (i = -ghosts; i < imax+ghosts; i++) {
        int p = Index1D(i,j,k,imax,jmaxv,kmax,ghosts);
        qC[i+ghosts] = v[p];
      }
      interp->upw = NULL;
      ierr = solver->ReconstructTransverse(qI,qC,imax,ghosts,mpi->ip,mpi->iproc,interp);
      if (ierr) return(ierr);
      for (i = 0; i < imaxu; i++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        vI[p] = qI[i];
      }
    }
  }
  for (i = 0; i < imaxu; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p  = Index1D(i,j  ,k,imaxu ,jmax  ,kmax  ,0);
        int qR = Index1D(i,j+1,k,imax+2,jmax+2,kmax+2,0);
        int qL = Index1D(i,j  ,k,imax+2,jmax+2,kmax+2,0);
        double dyinv;
        if ((j == 0) && (jp == 0))
          dyinv = 1.0 / (solver->yg[js+j+1] - solver->yg[js+j]);
        else if ((j == jmax-1) && (jp == jproc-1))
          dyinv = 1.0 / (solver->yg[js+j] - solver->yg[js+j-1]);
        else
          dyinv = 2.0 / (solver->yg[js+j+1] - solver->yg[js+j-1]);
        rhs_u[p] -= (uI[qR]*vI[qR] - uI[qL]*vI[qL]) * dyinv;
      }
    }
  }

  for (i = 0; i < imaxu; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = -ghosts; k < kmax+ghosts; k++) {
        int p = Index1D(i,j,k,imaxu,jmax,kmax,ghosts);
        qC[k+ghosts] = u[p];
      }
      for (k = 0; k < kmax+1; k++) {
        int pL = Index1D(i-1,j,k,imax,jmax,kmaxw,ghosts);
        int pR = Index1D(i  ,j,k,imax,jmax,kmaxw,ghosts);
        if      ((w[pL] > 0) && (w[pR] > 0)) up[k] =  1.0;
        else if ((w[pL] < 0) && (w[pR] < 0)) up[k] = -1.0;
        else                                 up[k] =  0.0;
      }
      interp->upw = up;
      ierr = solver->ReconstructNormal(qI,qC,kmax,ghosts,mpi->kp,mpi->kproc,interp);
      if (ierr) return(ierr);
      for (k = 0; k < kmax+1; k++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        uI[p] = qI[k];
      }
    }
  }
  for (j = 0; j < jmax; j++) {
    for (k = 0; k < kmaxw; k++) {
      for (i = -ghosts; i < imax+ghosts; i++) {
        int p = Index1D(i,j,k,imax,jmax,kmaxw,ghosts);
        qC[i+ghosts] = w[p];
      }
      interp->upw = NULL;
      ierr = solver->ReconstructTransverse(qI,qC,imax,ghosts,mpi->ip,mpi->iproc,interp);
      if (ierr) return(ierr);
      for (i = 0; i < imaxu; i++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        wI[p] = qI[i];
      }
    }
  }
  for (i = 0; i < imaxu; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p  = Index1D(i,j,k  ,imaxu ,jmax  ,kmax  ,0);
        int qR = Index1D(i,j,k+1,imax+2,jmax+2,kmax+2,0);
        int qL = Index1D(i,j,k  ,imax+2,jmax+2,kmax+2,0);
        double dzinv;
        if ((k == 0) && (kp == 0))
          dzinv = 1.0 / (solver->zg[ks+k+1] - solver->zg[ks+k]);
        else if ((k == kmax-1) && (kp == kproc-1))
          dzinv = 1.0 / (solver->zg[ks+k] - solver->zg[ks+k-1]);
        else
          dzinv = 2.0 / (solver->zg[ks+k+1] - solver->zg[ks+k-1]);
        rhs_u[p] -= (uI[qR]*wI[qR] - uI[qL]*wI[qL]) * dzinv;
      }
    }
  }

  /* v-velocity */

  for (i = 0; i < imax+1; i++) {
    for (k = 0; k < kmax; k++) {
      for (j = -ghosts; j < jmax+ghosts; j++) {
        int p = Index1D(i,j,k,imaxu,jmax,kmax,ghosts);
        qC[j+ghosts] = u[p];
      }
      interp->upw = NULL;
      ierr = solver->ReconstructTransverse(qI,qC,jmax,ghosts,mpi->jp,mpi->jproc,interp);
      if (ierr) return(ierr);
      for (j = 0; j < jmaxv; j++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        uI[p] = qI[j];
      }
    }
  }
  for (j = 0; j < jmaxv; j++) {
    for (k = 0; k < kmax; k++) {
      for (i = -ghosts; i < imax+ghosts; i++) {
        int p = Index1D(i,j,k,imax,jmaxv,kmax,ghosts);
        qC[i+ghosts] = v[p];
      }
      for (i = 0; i < imax+1; i++) {
        int pL = Index1D(i,j-1,k,imaxu,jmax,kmax,ghosts);
        int pR = Index1D(i,j  ,k,imaxu,jmax,kmax,ghosts);
        if      ((u[pL] > 0) && (u[pR] > 0)) up[i] =  1.0;
        else if ((u[pL] < 0) && (u[pR] < 0)) up[i] = -1.0;
        else                                 up[i] =  0.0;
      }
      interp->upw = up;
      ierr = solver->ReconstructNormal(qI,qC,imax,ghosts,mpi->ip,mpi->iproc,interp);
      if (ierr) return(ierr);
      for (i = 0; i < imax+1; i++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        vI[p] = qI[i];
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmaxv; j++) {
      for (k = 0; k < kmax; k++) {
        int p  = Index1D(i  ,j,k,imax  ,jmaxv ,kmax  ,0);
        int qR = Index1D(i+1,j,k,imax+2,jmax+2,kmax+2,0);
        int qL = Index1D(i  ,j,k,imax+2,jmax+2,kmax+2,0);
        double dxinv;
        if ((i == 0) && (ip == 0))
          dxinv = 1.0 / (solver->xg[is+i+1] - solver->xg[is+i]);
        else if ((i == imax-1) && (ip == iproc-1))
          dxinv = 1.0 / (solver->xg[is+i] - solver->xg[is+i-1]);
        else
          dxinv = 2.0 / (solver->xg[is+i+1] - solver->xg[is+i-1]);
        rhs_v[p] -= (uI[qR]*vI[qR] - uI[qL]*vI[qL]) * dxinv;
      }
    }
  }

  for (i = 0; i < imax; i++) {
    for (k = 0; k < kmax; k++) {
      for (j = -ghosts; j < jmaxv+ghosts; j++) {
        int p = Index1D(i,j,k,imax,jmaxv,kmax,ghosts);
        qC[j+ghosts] = v[p];
      }
      for (j = 0; j < jmaxv+1; j++) {
        int pL = Index1D(i,j-1,k,imax,jmaxv,kmax,ghosts);
        int pR = Index1D(i,j  ,k,imax,jmaxv,kmax,ghosts);
        if      ((v[pL] > 0) && (v[pR] > 0)) up[j] =  1.0;
        else if ((v[pL] < 0) && (v[pR] < 0)) up[j] = -1.0;
        else                                 up[j] =  0.0;
      }
      interp->upw = up;
      ierr = solver->ReconstructNormal(qI,qC,jmaxv,ghosts,mpi->jp,mpi->jproc,interp);
      if (ierr) return(ierr);
      for (j = 0; j < jmaxv+1; j++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        vI[p] = qI[j];
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmaxv; j++) {
      for (k = 0; k < kmax; k++) {
        int p  = Index1D(i,j  ,k,imax  ,jmaxv ,kmax  ,0);
        int qR = Index1D(i,j+1,k,imax+2,jmax+2,kmax+2,0);
        int qL = Index1D(i,j  ,k,imax+2,jmax+2,kmax+2,0);
        double dyinv;
        if ((j == 0) && (jp == 0))
          dyinv = 1.0 / (solver->ygv[js+j+1] - solver->ygv[js+j]);
        else if ((j == jmaxv-1) && (jp == jproc-1))
          dyinv = 1.0 / (solver->ygv[js+j] - solver->ygv[js+j-1]);
        else
          dyinv = 2.0 / (solver->ygv[js+j+1] - solver->ygv[js+j-1]);
        rhs_v[p] -= (vI[qR]*vI[qR] - vI[qL]*vI[qL]) * dyinv;
      }
    }
  }

  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmaxv; j++) {
      for (k = -ghosts; k < kmax+ghosts; k++) {
        int p = Index1D(i,j,k,imax,jmaxv,kmax,ghosts);
        qC[k+ghosts] = v[p];
      }
      for (k = 0; k < kmax+1; k++) {
        int pL = Index1D(i,j-1,k,imax,jmax,kmaxw,ghosts);
        int pR = Index1D(i,j  ,k,imax,jmax,kmaxw,ghosts);
        if      ((w[pL] > 0) && (w[pR] > 0)) up[k] =  1.0;
        else if ((w[pL] < 0) && (w[pR] < 0)) up[k] = -1.0;
        else                                 up[k] =  0.0;
      }
      interp->upw = up;
      ierr = solver->ReconstructNormal(qI,qC,kmax,ghosts,mpi->kp,mpi->kproc,interp);
      if (ierr) return(ierr);
      for (k = 0; k < kmax+1; k++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        vI[p] = qI[k];
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (k = 0; k < kmaxw; k++) {
      for (j = -ghosts; j < jmax+ghosts; j++) {
        int p = Index1D(i,j,k,imax,jmax,kmaxw,ghosts);
        qC[j+ghosts] = w[p];
      }
      interp->upw = NULL;
      ierr = solver->ReconstructTransverse(qI,qC,jmax,ghosts,mpi->jp,mpi->jproc,interp);
      if (ierr) return(ierr);
      for (j = 0; j < jmaxv; j++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        wI[p] = qI[j];
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmaxv; j++) {
      for (k = 0; k < kmax; k++) {
        int p  = Index1D(i,j,k  ,imax  ,jmaxv ,kmax  ,0);
        int qR = Index1D(i,j,k+1,imax+2,jmax+2,kmax+2,0);
        int qL = Index1D(i,j,k  ,imax+2,jmax+2,kmax+2,0);
        double dzinv;
        if ((k == 0) && (kp == 0))
          dzinv = 1.0 / (solver->zg[ks+k+1] - solver->zg[ks+k]);
        else if ((k == kmax-1) && (kp == kproc-1))
          dzinv = 1.0 / (solver->zg[ks+k] - solver->zg[ks+k-1]);
        else
          dzinv = 2.0 / (solver->zg[ks+k+1] - solver->zg[ks+k-1]);
        rhs_v[p] -= (vI[qR]*wI[qR] - vI[qL]*wI[qL]) * dzinv;
      }
    }
  }

  /* w-velocity */

  for (i = 0; i < imaxu; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = -ghosts; k < kmax+ghosts; k++) {
        int p = Index1D(i,j,k,imaxu,jmax,kmax,ghosts);
        qC[k+ghosts] = u[p];
      }
      interp->upw = NULL;
      ierr = solver->ReconstructTransverse(qI,qC,kmax,ghosts,mpi->kp,mpi->kproc,interp);
      if (ierr) return(ierr);
      for (k = 0; k < kmaxw; k++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        uI[p] = qI[k];
      }
    }
  }
  for (j = 0; j < jmax; j++) {
    for (k = 0; k < kmaxw; k++) {
      for (i = -ghosts; i < imax+ghosts; i++) {
        int p = Index1D(i,j,k,imax,jmax,kmaxw,ghosts);
        qC[i+ghosts] = w[p];
      }
      for (i = 0; i < imax+1; i++) {
        int pL = Index1D(i,j,k-1,imaxu,jmax,kmax,ghosts);
        int pR = Index1D(i,j,k  ,imaxu,jmax,kmax,ghosts);
        if      ((u[pL] > 0) && (u[pR] > 0)) up[i] =  1.0;
        else if ((u[pL] < 0) && (u[pR] < 0)) up[i] = -1.0;
        else                                 up[i] =  0.0;
      }
      interp->upw = up;
      ierr = solver->ReconstructNormal(qI,qC,imax,ghosts,mpi->ip,mpi->iproc,interp);
      if (ierr) return(ierr);
      for (i = 0; i < imax+1; i++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        wI[p] = qI[i];
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmaxw; k++) {
        int p  = Index1D(i  ,j,k,imax  ,jmax  ,kmaxw ,0);
        int qR = Index1D(i+1,j,k,imax+2,jmax+2,kmax+2,0);
        int qL = Index1D(i  ,j,k,imax+2,jmax+2,kmax+2,0);
        double dxinv;
        if ((i == 0) && (ip == 0))
          dxinv = 1.0 / (solver->xg[is+i+1] - solver->xg[is+i]);
        else if ((i == imax-1) && (ip == iproc-1))
          dxinv = 1.0 / (solver->xg[is+i] - solver->xg[is+i-1]);
        else
          dxinv = 2.0 / (solver->xg[is+i+1] - solver->xg[is+i-1]);
        rhs_w[p] -= (uI[qR]*wI[qR] - uI[qL]*wI[qL]) * dxinv;
      }
    }
  }

  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmaxv; j++) {
      for (k = -ghosts; k < kmax+ghosts; k++) {
        int p = Index1D(i,j,k,imax,jmaxv,kmax,ghosts);
        qC[k+ghosts] = v[p];
      }
      interp->upw = NULL;
      ierr = solver->ReconstructTransverse(qI,qC,kmax,ghosts,mpi->kp,mpi->kproc,interp);
      if (ierr) return(ierr);
      for (k = 0; k < kmaxw; k++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        vI[p] = qI[k];
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (k = 0; k < kmaxw; k++) {
      for (j = -ghosts; j < jmax+ghosts; j++) {
        int p = Index1D(i,j,k,imax,jmax,kmaxw,ghosts);
        qC[j+ghosts] = w[p];
      }
      for (j = 0; j < jmax+1; j++) {
        int pL = Index1D(i,j,k-1,imax,jmaxv,kmax,ghosts);
        int pR = Index1D(i,j,k  ,imax,jmaxv,kmax,ghosts);
        if      ((v[pL] > 0) && (v[pR] > 0)) up[j] =  1.0;
        else if ((v[pL] < 0) && (v[pR] < 0)) up[j] = -1.0;
        else                                 up[j] =  0.0;
      }
      interp->upw = up;
      ierr = solver->ReconstructNormal(qI,qC,jmax,ghosts,mpi->jp,mpi->jproc,interp);
      if (ierr) return(ierr);
      for (j = 0; j < jmax+1; j++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        wI[p] = qI[j];
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmaxw; k++) {
        int p  = Index1D(i,j  ,k,imax  ,jmax  ,kmaxw ,0);
        int qR = Index1D(i,j+1,k,imax+2,jmax+2,kmax+2,0);
        int qL = Index1D(i,j  ,k,imax+2,jmax+2,kmax+2,0);
        double dyinv;
        if ((j == 0) && (jp == 0))
          dyinv = 1.0 / (solver->yg[js+j+1] - solver->yg[js+j]);
        else if ((j == jmax-1) && (jp == jproc-1))
          dyinv = 1.0 / (solver->yg[js+j] - solver->yg[js+j-1]);
        else
          dyinv = 2.0 / (solver->yg[js+j+1] - solver->yg[js+j-1]);
        rhs_w[p] -= (vI[qR]*wI[qR] - vI[qL]*wI[qL]) * dyinv;
      }
    }
  }

  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = -ghosts; k < kmaxw+ghosts; k++) {
        int p = Index1D(i,j,k,imax,jmax,kmaxw,ghosts);
        qC[k+ghosts] = w[p];
      }
      for (k = 0; k < kmaxw+1; k++) {
        int pL = Index1D(i,j,k-1,imax,jmax,kmaxw,ghosts);
        int pR = Index1D(i,j,k  ,imax,jmax,kmaxw,ghosts);
        if      ((w[pL] > 0) && (w[pR] > 0)) up[k] =  1.0;
        else if ((w[pL] < 0) && (w[pR] < 0)) up[k] = -1.0;
        else                                 up[k] =  0.0;
      }
      interp->upw = up;
      ierr = solver->ReconstructNormal(qI,qC,kmaxw,ghosts,mpi->kp,mpi->kproc,interp);
      if (ierr) return(ierr);
      for (k = 0; k < kmaxw+1; k++) {
        int p = Index1D(i,j,k,imax+2,jmax+2,kmax+2,0);
        wI[p] = qI[k];
      }
    }
  }
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmaxw; k++) {
        int p  = Index1D(i,j,k  ,imax  ,jmax  ,kmaxw ,0);
        int qR = Index1D(i,j,k+1,imax+2,jmax+2,kmax+2,0);
        int qL = Index1D(i,j,k  ,imax+2,jmax+2,kmax+2,0);
        double dzinv;
        if ((k == 0) && (kp == 0))
          dzinv = 1.0 / (solver->zgw[ks+k+1] - solver->zgw[ks+k]);
        else if ((k == kmaxw-1) && (kp == kproc-1))
          dzinv = 1.0 / (solver->zgw[ks+k] - solver->zgw[ks+k-1]);
        else
          dzinv = 2.0 / (solver->zgw[ks+k+1] - solver->zgw[ks+k-1]);
        rhs_w[p] -= (wI[qR]*wI[qR] - wI[qL]*wI[qL]) * dzinv;
      }
    }
  }

  return(0);
}
