#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <arrayfunctions.h>
#include <mpivars.h>
#include <boundaryconditions.h>
#include <immersedbody.h>
#include <ibcartins3d.h>
#include <timeintegration.h>

/* Function declarations */
static int TimeInitialize (void*,void*,void*);
static int PreTimeStep    (void*,void*);
static int TimeStep       (void*,void*);
static int PostTimeStep   (void*,void*);
static int PrintTimeStep  (void*,void*);
static int TimeCleanUp    (void*,void*);

int InterpolateStagToColl   (double*,double*,double*,double*,double*,double*,int,int,int,int,int);
double     ComputeCFL     (void*,void*,double);

int Solve(void *s,void *m)
{
  IBCartINS3D   *solver = (IBCartINS3D*) s;
  MPIVariables  *mpi    = (MPIVariables*) m;
  int           ierr    = 0;

  /* Write an initial solution file */
  ierr = OutputSolution(solver,mpi); if (ierr) return(ierr);

  /* Define and initialize the time-integration object */
  TimeIntegration TS;
  ierr = TimeInitialize(solver,&TS,mpi); if (ierr)  return(ierr);

  if (!mpi->rank) printf("Solving in time (%d iterations)\n",TS.n_iter);
  for (TS.iter = 0; TS.iter < TS.n_iter; TS.iter++) {
    ierr = PreTimeStep  (&TS,mpi); if (ierr) return(ierr);
    ierr = TimeStep     (&TS,mpi); if (ierr) return(ierr);
    ierr = PostTimeStep (&TS,mpi); if (ierr) return(ierr);
    ierr = PrintTimeStep(&TS,mpi); if (ierr) return(ierr);

    /* Write intermediate solution to file */
    if ((TS.iter+1)%solver->file_op_iter == 0) {
      ierr = OutputSolution(solver,mpi);
      if (ierr) return(ierr);
    }
  }

  if (!mpi->rank) printf("Completed time integration (Final time: %f).\n",TS.waqt);
  ierr = TimeCleanUp(&TS,mpi);  if(ierr) return(ierr);
  return(0);
}

int TimeInitialize(void *s,void *ts,void *m)
{
  IBCartINS3D     *solver = (IBCartINS3D*) s;
  TimeIntegration *TS     = (TimeIntegration*) ts;
  MPIVariables    *mpi    = (MPIVariables*) m;
  ImmersedBody    *body   = (ImmersedBody*) solver->body;
  int             ierr    = 0;
  if (!solver) return(1);

  TS->solver = solver;
  TS->n_iter = solver->n_iter;
  TS->waqt   = 0.0;
  TS->dt     = solver->dt;
  TS->max_cfl= 0.0;
  TS->norm   = 0.0;

  int imax    = solver->imax_local;
  int jmax    = solver->jmax_local;
  int kmax    = solver->kmax_local;
  int ghosts  = solver->ghosts;
  int size    = (imax+2*ghosts)*(jmax+2*ghosts )*(kmax+2*ghosts);

  TS->u = (double*) calloc (size,sizeof(double));
  TS->v = (double*) calloc (size,sizeof(double));
  TS->w = (double*) calloc (size,sizeof(double));
  ierr = SetArrayValue(TS->u,size,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(TS->v,size,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(TS->w,size,0.0); if (ierr) return(ierr);

  /* open files for writing */
  if (!mpi->rank) {
    if (solver->write_residual) TS->ResidualFile = (void*) fopen(solver->res_filename,"w");
    if (body->is_present) TS->IBForceFile = (void*) fopen(solver->forces_filename,"w");
  }

  return(0);
}

int PreTimeStep(void *ts,void *m)
{
  TimeIntegration *TS     = (TimeIntegration*) ts;
  IBCartINS3D     *solver = (IBCartINS3D*)     TS->solver;
  MPIVariables    *mpi    = (MPIVariables*)    m;
  int             ierr    = 0;

  int imax    = solver->imax_local;
  int jmax    = solver->jmax_local;
  int kmax    = solver->kmax_local;
  int ghosts  = solver->ghosts;

  /* copy current solution as collocated variables (without ghosts) */
  ierr = InterpolateStagToColl(solver->u,solver->v,solver->w,
                               TS->u,TS->v,TS->w,imax,jmax,kmax,ghosts,0);
  if (ierr) return(ierr);

  /* compute max CFL over the domain */
  double local_max_cfl = ComputeCFL(solver,mpi,TS->dt);
  ierr = MPIMax_double(&TS->max_cfl,&local_max_cfl,1);

  return(0);
}

int TimeStep(void *ts,void *m)
{
  TimeIntegration *TS     = (TimeIntegration*) ts;
  MPIVariables    *mpi    = (MPIVariables*) m;
  IBCartINS3D     *solver = (IBCartINS3D*) TS->solver;
  int             ierr    = 0;

  ierr = solver->TimeIntegrate(TS,solver,mpi);

  return(ierr);
}

int PostTimeStep(void *ts,void *m)
{
  TimeIntegration *TS     = (TimeIntegration*) ts;
  MPIVariables    *mpi    = (MPIVariables*) m;
  IBCartINS3D     *solver = (IBCartINS3D*) TS->solver;
  ImmersedBody    *body   = (ImmersedBody*) solver->body;
  int             ierr    = 0;

  int imax   = solver->imax_local;
  int jmax   = solver->jmax_local;
  int kmax   = solver->kmax_local;
  int ghosts = solver->ghosts;

  /* update current time */
  TS->waqt += TS->dt;

//  int offset[3];
//  offset[0] = mpi->is;
//  offset[1] = mpi->js;
//  offset[2] = mpi->ks;

  if ((TS->iter+1)%solver->screen_op_iter == 0) {
    /* Calculate norm for this time step */
    int     dim[10];
    double  norm[3];

    ierr = InterpolateStagToColl(solver->u ,solver->v ,solver->w ,
                                 solver->uc,solver->vc,solver->wc,
                                 imax,jmax,kmax,solver->ghosts,0);
    if (ierr) return(ierr);
    ierr = ArrayAXPY(solver->uc,-1.0,TS->u,imax*jmax*kmax); if(ierr) return(ierr);
    ierr = ArrayAXPY(solver->vc,-1.0,TS->v,imax*jmax*kmax); if(ierr) return(ierr);
    ierr = ArrayAXPY(solver->wc,-1.0,TS->w,imax*jmax*kmax); if(ierr) return(ierr);

    ierr = Array3DElementMultiply(imax,jmax,kmax,ghosts,0,solver->iblank,TS->u);
    if (ierr) return(ierr);
    ierr = Array3DElementMultiply(imax,jmax,kmax,ghosts,0,solver->iblank,TS->v);
    if (ierr) return(ierr);
    ierr = Array3DElementMultiply(imax,jmax,kmax,ghosts,0,solver->iblank,TS->w);
    if (ierr) return(ierr);

    dim[0] = 0                 ;  dim[2] = 0                 ;  dim[4] = 0                ;
    dim[1] = solver->imax_local;  dim[3] = solver->jmax_local;  dim[5] = solver->kmax_local;
    dim[6] = solver->imax_local;  dim[7] = solver->jmax_local;  dim[8] = solver->kmax_local;
    dim[9] = 0;

    norm[0] = Array3DSumSquare(TS->u,&dim[0]);
    norm[1] = Array3DSumSquare(TS->v,&dim[0]);
    norm[2] = Array3DSumSquare(TS->w,&dim[0]);

    double global_norm[3] = {0,0,0};
    MPISum_double(&global_norm[0],&norm[0],3);
    int imax_global = solver->imax_global;
    int jmax_global = solver->jmax_global;
    int kmax_global = solver->kmax_global;
    int nPoints = imax_global*jmax_global*kmax_global;
    global_norm[0] = sqrt(global_norm[0]/((double)nPoints));
    global_norm[1] = sqrt(global_norm[1]/((double)nPoints));
    global_norm[2] = sqrt(global_norm[2]/((double)nPoints));
    TS->norm = global_norm[0] + global_norm[1] + global_norm[2];

    /* write to file */
    if ((!mpi->rank) && solver->write_residual) 
      fprintf((FILE*)TS->ResidualFile,"%10d\t%E\t%E\n",TS->iter+1,TS->waqt,TS->norm);
  
    /* calculate body forces on immersed body, if present */
    if (body->is_present) {
      double force[9];
//      ierr = IBCalculateBodyForces(body,mpi,&dim[0],&offset[0],
//                                   solver->xg,solver->yg,solver->zg,
//                                   solver->xgu,solver->ygv,solver->zgw,
//                                   solver->u,solver->v,solver->w,solver->phi,
//                                   solver->mu,solver->mode2d,&force[0]);
      if (ierr) return(ierr);
      /* write to file */
      if (!mpi->rank)
        fprintf((FILE*)TS->IBForceFile,"%10d\t%E\t%E\t%E\t%E\t%E\t%E\t%E\t%E\t%E\t%E\n",
                TS->iter+1,TS->waqt,force[0],force[1],force[2],force[3],force[4],
                force[5],force[6],force[7],force[8]);
    }
  }
  return(0);
}

int PrintTimeStep(void *ts,void *m)
{
  TimeIntegration *TS       = (TimeIntegration*) ts;
  IBCartINS3D     *solver   = (IBCartINS3D*) TS->solver;
  MPIVariables    *mpi      = (MPIVariables*) m;

  if ((!mpi->rank) && ((TS->iter+1)%solver->screen_op_iter == 0)) {
    printf("Iteration: %6d  " ,TS->iter+1);
    printf("Time: %E  "       ,TS->waqt);
    printf("Max CFL: %E  "    ,TS->max_cfl);
    printf("Norm: %E  "       ,TS->norm);
    printf("Divergence: %E  " ,solver->divergence_norm);
    printf("\n");
  }

  return(0);
}

int TimeCleanUp(void *ts,void *m)
{
  TimeIntegration *TS   = (TimeIntegration*) ts;
  MPIVariables    *mpi  = (MPIVariables*) m;
  IBCartINS3D     *s    = (IBCartINS3D*) TS->solver;
  ImmersedBody    *body = (ImmersedBody*) s->body;

  /* close files opened for writing */
  if (!mpi->rank) {
    if (s->write_residual) fclose((FILE*)TS->ResidualFile);
    if (body->is_present)  fclose((FILE*)TS->IBForceFile);
  }

  /* deallocate arrays */
  free(TS->u);
  free(TS->v);
  free(TS->w);
  return(0);
}
