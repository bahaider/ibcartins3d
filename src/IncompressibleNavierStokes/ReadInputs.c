#include <stdio.h>
#include <string.h>
#include <mpivars.h>
#include <ibcartins3d.h>

int ReadInputs(void *s, void *m)
{
  IBCartINS3D   *solver = (IBCartINS3D*)  s;
  MPIVariables  *mpi    = (MPIVariables*) m;
  int           ierr    = 0;

  /* Only root process reads in input files */
  if (!mpi->rank) {
    FILE *in;

    /* default values */
    strcpy(solver->inp_filename     , "solver.inp"      );
    strcpy(solver->bcinp_filename   , "bc.inp"          );
    strcpy(solver->ic_filename      , "init.dat"        );
    strcpy(solver->icrs_filename    , "init_restart.dat");
    strcpy(solver->res_filename     , "residual.log"    );
    strcpy(solver->stl_filename     , "body.stl"        );
    strcpy(solver->op_prefix        , "./"              );
    strcpy(solver->fsbc_prefix      , "./"              );
    strcpy(solver->forces_filename  , "bodyforces.dat"  );
    strcpy(solver->surface_filename , "surface_data.ply");
    /* reading input filenames */
    printf("Reading input filenames.\n");
    in = fopen(_MAIN_INPUT_FILE_,"r");
    if (!in) {
      fprintf(stderr,"Warning: File %s not found. Using default filenames.\n",_MAIN_INPUT_FILE_);
    } else {
      char  name[_MAX_STRING_SIZE_],value[_MAX_STRING_SIZE_];
      int   count = 0;
      while (!feof(in)) {
        ierr = fscanf(in,"%s %s",name,value);
        count++;
        if (!strcmp(name,"inp"))                strcpy(solver->inp_filename    ,value);
        else if (!strcmp(name,"bc_inp"))	      strcpy(solver->bcinp_filename  ,value);
        else if (!strcmp(name,"init"))		      strcpy(solver->ic_filename     ,value);
        else if (!strcmp(name,"init_restart"))	strcpy(solver->icrs_filename   ,value);
        else if (!strcmp(name,"res"))		        strcpy(solver->res_filename    ,value);
        else if (!strcmp(name,"body"))		      strcpy(solver->stl_filename    ,value);
        else if (!strcmp(name,"output_prefix")) strcpy(solver->op_prefix       ,value);
        else if (!strcmp(name,"fsbc_prefix"))	  strcpy(solver->fsbc_prefix     ,value);
        else if (!strcmp(name,"body_forces"))	  strcpy(solver->forces_filename ,value);
        else if (!strcmp(name,"body_surface"))	strcpy(solver->surface_filename,value);
      }
      fclose(in);
    }
    printf("\tinp_filename (Solver inputs):                             %s\n",solver->inp_filename    );
    printf("\tbcinp_filename (Boundary conditions):                     %s\n",solver->bcinp_filename  );
    printf("\tic_filename (Initial Solution):                           %s\n",solver->ic_filename     );
    printf("\ticrs_filename (Restart Solution):                         %s\n",solver->icrs_filename   );
    printf("\tres_filename (Output residuals):                          %s\n",solver->res_filename    );
    printf("\tstl_filename (STL for immersed body):                     %s\n",solver->stl_filename    );
    printf("\top_prefix (Location to place output file(s) in):          %s\n",solver->op_prefix       );
    printf("\tfsbc_prefix (Location to read freestream BC files from):  %s\n",solver->fsbc_prefix     );
    printf("\tforces_filename (Output body forces):                     %s\n",solver->forces_filename );
    printf("\tsurface_filename (Output surface data for immersed body): %s\n",solver->surface_filename);

    /* default values */
    solver->imax_global       = 10;
    solver->jmax_global       = 10;
    solver->kmax_global       = 10;
    mpi->iproc                = 1;
    mpi->jproc                = 1;
    mpi->kproc                = 1;
    solver->ghosts            = 3;
    solver->n_iter            = 0;
    solver->gs_iter           = 0;
    solver->spatial_scheme    = 1;
    solver->time_scheme       = 1;
    solver->dt                = 0.0;
    solver->mu                = 0.0;
    solver->screen_op_iter    = 1;
    solver->file_op_iter      = solver->n_iter;
    solver->print_ls          = 1;
    solver->ls_monitor        = 0;
    solver->write_residual    = 0;
    solver->restart_iter      = 0;
    strcpy(solver->write_restart  ,"no"             );
    strcpy(solver->flux_form      ,"nonconservative");
    strcpy(solver->mode2d         ,"no"             );
    strcpy(solver->op_file_format ,"tecplot"        );
    strcpy(solver->op_overwrite   ,"yes"            );
    strcpy(solver->op_details     ,"basic"          );
    /* reading solver inputs */
    printf("Reading solver inputs from %s.\n",solver->inp_filename);
    in = fopen(solver->inp_filename,"r");
    if (!in) {
      fprintf(stderr,"Error: File %s not found.\n",solver->inp_filename);
      return(1);
    } else {
	    char word[_MAX_STRING_SIZE_];
      ierr = fscanf(in,"%s",word);
    	if (!strcmp(word, "begin")){
		    while (strcmp(word, "end")){
			    ierr = fscanf(in,"%s",word);
    			if      (!strcmp(word, "imax"))			        ierr = fscanf(in,"%d",&solver->imax_global);
		    	else if (!strcmp(word, "jmax"))			        ierr = fscanf(in,"%d",&solver->jmax_global);
    			else if (!strcmp(word, "kmax"))			        ierr = fscanf(in,"%d",&solver->kmax_global);
#ifndef serial
          else if (!strcmp(word, "iproc"))		        ierr = fscanf(in,"%d",&mpi->iproc);
		    	else if (!strcmp(word, "jproc"))		        ierr = fscanf(in,"%d",&mpi->jproc);
		    	else if (!strcmp(word, "kproc"))		        ierr = fscanf(in,"%d",&mpi->kproc);
#endif
    			else if (!strcmp(word, "ghost"))		        ierr = fscanf(in,"%d",&solver->ghosts);
		    	else if (!strcmp(word, "n_iter"))		        ierr = fscanf(in,"%d",&solver->n_iter);
    			else if (!strcmp(word, "subiter"))	        ierr = fscanf(in,"%d",&solver->gs_iter);
		    	else if (!strcmp(word, "conv_space_order"))	ierr = fscanf(in,"%d",&solver->spatial_scheme);
    			else if (!strcmp(word, "conv_time_order"))	ierr = fscanf(in,"%d",&solver->time_scheme);
		    	else if (!strcmp(word, "flux_form"))        ierr = fscanf(in,"%s",solver->flux_form);
    			else if (!strcmp(word, "dt"))			          ierr = fscanf(in,"%lf",&solver->dt);
		    	else if (!strcmp(word, "mu"))			          ierr = fscanf(in,"%lf",&solver->mu);
    			else if (!strcmp(word, "screen_op_iter"))   ierr = fscanf(in,"%d",&solver->screen_op_iter);
    			else if (!strcmp(word, "file_op_iter"))		  ierr = fscanf(in,"%d",&solver->file_op_iter);
    			else if (!strcmp(word, "print_ls"))   		  ierr = fscanf(in,"%d",&solver->print_ls);
    			else if (!strcmp(word, "ls_monitor"))  		  ierr = fscanf(in,"%d",&solver->ls_monitor);
    			else if (!strcmp(word, "write_residual"))   ierr = fscanf(in,"%d",&solver->write_residual);
		    	else if (!strcmp(word, "restart_run"))		  ierr = fscanf(in,"%d",&solver->restart_iter);
    			else if (!strcmp(word, "write_restart"))    ierr = fscanf(in,"%s",solver->write_restart);
		    	else if (!strcmp(word, "2d_mode"))          ierr = fscanf(in,"%s",solver->mode2d);
    			else if (!strcmp(word, "op_file_format"))   ierr = fscanf(in,"%s",solver->op_file_format);
    			else if (!strcmp(word, "op_overwrite"))     ierr = fscanf(in,"%s",solver->op_overwrite);
    			else if (!strcmp(word, "op_details"))       ierr = fscanf(in,"%s",solver->op_details);
          else if ( strcmp(word, "end")) {
            char useless[_MAX_STRING_SIZE_];
            ierr = fscanf(in,"%s",useless);
            printf("Warning: keyword %s in %s with value %s not recognized or extraneous. Ignoring.\n",
                        word,solver->inp_filename,useless);
          }
        }
	    } else {
    		fprintf(stderr,"Error: Illegal format in %s.\n",solver->inp_filename);
        return(1);
	    }
	    printf("\tDomain size                   : %d X %d X %d\n",
              solver->imax_global,solver->jmax_global,solver->kmax_global);
#ifndef serial
	    printf("\tProcesses                     : %d X %d X %d\n",
              mpi->iproc,mpi->jproc,mpi->kproc);
#endif
	    printf("\tNo. of ghosts pts             : %d\n",solver->ghosts);
	    printf("\tNo. of iter.                  : %d\n",solver->n_iter);
      printf("\tNo. of linear solver iter.    : %d\n",solver->gs_iter);
      printf("\tSpatial reconstruction scheme : %d\n",solver->spatial_scheme);
      printf("\tTime integration scheme       : %d\n",solver->time_scheme);
      printf("\tConvective flux form          : %s\n",solver->flux_form);
    	printf("\tTime Step                     : %E\n",solver->dt);
      printf("\tCoefficient of viscosity      : %E\n",solver->mu);
      printf("\tScreen output iterations      : %d\n",solver->screen_op_iter);
      printf("\tPrint linear solver details   : %d\n",solver->print_ls);
      printf("\tPrint linear solver iterations: %d\n",solver->ls_monitor);
      printf("\tFile output iterations        : %d\n",solver->file_op_iter);
      printf("\tRestart iteration             : %d\n",solver->restart_iter);
      printf("\tWrite restart file            : %s\n",solver->write_restart);
      printf("\t2D Mode                       : %s\n",solver->mode2d);
      printf("\tSolution file format          : %s\n",solver->op_file_format);
      printf("\tOverwrite solution file       : %s\n",solver->op_overwrite);
      printf("\tSolution output details       : %s\n",solver->op_details);
    }
    fclose(in);
  }

  ierr = 0;
  if (ierr) return(ierr);

  return(0);
}

