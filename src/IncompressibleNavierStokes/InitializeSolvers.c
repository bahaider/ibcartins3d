#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpivars.h>
#include <interpolation.h>
#include <timeintegration.h>
#include <linearsolver.h>
#include <mathroutines.h>
#include <ibcartins3d.h>

/* Output functions */
int WriteTecplot  (int*,double*,double*,double*,double*,double*,double*,double*,double*,char*);

/* Convection and diffusion functions */
int ConvectionConservative    (void*,void*,double*,double*,double*,double*,double*,double*);
int ConvectionNonConservative (void*,void*,double*,double*,double*,double*,double*,double*);
int Diffusion                 (void*,void*,double*,double*,double*,double*,double*,double*);

/* Boundary conditions for pressure */
int ApplyBoundaryConditionsPressure(int*,double*,void*,void*,int,int);

int InitializeSolvers(void *s, void *m)
{
  IBCartINS3D   *solver = (IBCartINS3D*)  s;
  MPIVariables  *mpi    = (MPIVariables*) m;
  int           ierr    = 0;

  if (!mpi->rank) printf("Initializing solvers.\n");

  /* Time integration function */
  if      (solver->time_scheme == 1) solver->TimeIntegrate = ForwardEuler;
  else if (solver->time_scheme == 3) solver->TimeIntegrate = LowStorageRK3;
  else {
    fprintf(stderr,"Error in InitializeSolvers(): %d is not a valid time scheme\n",
            solver->time_scheme);
    return(1);
  }

  /* Convection and diffusion functions */
  if      (!strcmp(solver->flux_form,"conservative"   ))  solver->Convection = ConvectionConservative;
  else if (!strcmp(solver->flux_form,"nonconservative"))  solver->Convection = ConvectionNonConservative;
  else {
    fprintf(stderr,"Error in InitializeSolvers(): %s is not a valid flux form\n",
            solver->flux_form);
    return(1);
  }
  solver->Diffusion = Diffusion;

  /* Reconstruction functions */
  solver->ReconstructParams = (InterpolationParameters*) calloc (1,sizeof(InterpolationParameters));
  ierr = InterpInitialize(solver->ReconstructParams,solver->spatial_scheme);  if (ierr) return(ierr);
  if (solver->spatial_scheme == 1) {
    solver->ReconstructNormal      = FirstOrderUpwind;
    solver->ReconstructTransverse  = SecondOrderCentral;
    solver->interp_order           = 2;
  } else if (solver->spatial_scheme == 2) {
    solver->ReconstructNormal      = SecondOrderCentral;
    solver->ReconstructTransverse  = SecondOrderCentral;
    solver->interp_order           = 2;
  } else if (solver->spatial_scheme == 4) {
    solver->ReconstructNormal      = FourthOrderCentral;
    solver->ReconstructTransverse  = FourthOrderCentral;
    solver->interp_order           = 4;
  } else if (solver->spatial_scheme == 5) {
    solver->ReconstructNormal      = FifthOrderWENO;
    solver->ReconstructTransverse  = FourthOrderCentral;
    solver->interp_order           = 4;
  } else {
    fprintf(stderr,"Error in InitializeSolver(): %d is not a valid spatial scheme\n",
            solver->spatial_scheme);
    return(1);
  }

  /* Solution output function */
  if (!strcmp(solver->op_file_format,"tecplot")) {
    solver->WriteOutput = WriteTecplot;
    if (!strcmp(solver->op_overwrite,"no")) strcpy(solver->op_filename,"op_00000.dat");
    else                                    strcpy(solver->op_filename,"op.dat");
  } else if (!strcmp(solver->op_file_format,"none")) {
    solver->WriteOutput = NULL;
  } else {
    fprintf(stderr,"Error: %s is not a valid file format.\n",solver->op_file_format);
    return(1);
  }

  /* Initialize linear solvers for predictor and corrector stages */
  char name[_MAX_STRING_SIZE_] = "sip";
  int  dimensions[4];
  LinearSolver *LS;

  solver->predictorLSu = (LinearSolver*) calloc (1,sizeof(LinearSolver));
  solver->predictorLSv = (LinearSolver*) calloc (1,sizeof(LinearSolver));
  solver->predictorLSw = (LinearSolver*) calloc (1,sizeof(LinearSolver));
  solver->correctorLS  = (LinearSolver*) calloc (1,sizeof(LinearSolver));

  dimensions[0] = solver->imax_local + 1;
  dimensions[1] = solver->jmax_local;
  dimensions[2] = solver->kmax_local;
  dimensions[3] = solver->ghosts;
  ierr = LSInitialize(solver->predictorLSu,name,solver->gs_iter,solver,&dimensions[0],
                      1e-20,1e-10,1,solver->ls_monitor,NULL);
  if (ierr) return(ierr);
  LS = (LinearSolver*) solver->predictorLSu;
  ierr = SIPComputeCoefficients(LS->scheme,mpi,&dimensions[0],
                                solver->xgu,solver->yg,solver->zg,
                                1.0,-solver->mu*solver->dt);
  if (ierr) return(ierr);

  dimensions[0] = solver->imax_local;
  dimensions[1] = solver->jmax_local + 1;
  dimensions[2] = solver->kmax_local;
  dimensions[3] = solver->ghosts;
  ierr = LSInitialize(solver->predictorLSv,name,solver->gs_iter,solver,&dimensions[0],
                      1e-20,1e-10,1,solver->ls_monitor,NULL);
  if (ierr) return(ierr);
  LS = (LinearSolver*) solver->predictorLSv;
  ierr = SIPComputeCoefficients(LS->scheme,mpi,&dimensions[0],
                                solver->xg,solver->ygv,solver->zg,
                                1.0,-solver->mu*solver->dt);
  if (ierr) return(ierr);

  dimensions[0] = solver->imax_local;
  dimensions[1] = solver->jmax_local;
  dimensions[2] = solver->kmax_local + 1;
  dimensions[3] = solver->ghosts;
  ierr = LSInitialize(solver->predictorLSw,name,solver->gs_iter,solver,&dimensions[0],
                      1e-20,1e-10,1,solver->ls_monitor,NULL);
  if (ierr) return(ierr);
  LS = (LinearSolver*) solver->predictorLSw;
  ierr = SIPComputeCoefficients(LS->scheme,mpi,&dimensions[0],
                                solver->xg,solver->yg,solver->zgw,
                                1.0,-solver->mu*solver->dt);
  if (ierr) return(ierr);

  dimensions[0] = solver->imax_local;
  dimensions[1] = solver->jmax_local;
  dimensions[2] = solver->kmax_local;
  dimensions[3] = solver->ghosts;
  ierr = LSInitialize(solver->correctorLS,name,solver->gs_iter,solver,&dimensions[0],
                      1e-20,1e-10,0,solver->ls_monitor,&ApplyBoundaryConditionsPressure);
  if (ierr) return(ierr);
  LS = (LinearSolver*) solver->correctorLS;
  ierr = SIPComputeCoefficients(LS->scheme,mpi,&dimensions[0],
                                solver->xg,solver->yg,solver->zg,0.0,2.0);
  if (ierr) return(ierr);

  return(0);
}
