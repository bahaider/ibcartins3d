#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpivars.h>
#include <mathroutines.h>
#include <ibcartins3d.h>

int Initialize(void *s, void *m)
{
  IBCartINS3D   *solver = (IBCartINS3D*)  s;
  MPIVariables  *mpi    = (MPIVariables*) m;
  int           ierr    = 0;

#ifndef serial
  /* Domain partitioning */
  if (!mpi->rank) printf("Partitioning domain.\n");
  int buffer_size = 14;
  int *buffer;
  buffer = (int*) calloc(buffer_size,sizeof(int));
  if (!mpi->rank) {
    if (mpi->nproc != (mpi->iproc*mpi->jproc*mpi->kproc)) {
      printf("Error: number of processes is not consistent with iproc*jproc*kproc.\n");
      return(1);
    }
    buffer[0] = mpi->iproc;
    buffer[1] = mpi->jproc;
    buffer[2] = mpi->kproc;
    buffer[3] = solver->imax_global;
    buffer[4] = solver->jmax_global;
    buffer[5] = solver->kmax_global;
    buffer[6] = solver->ghosts;
    buffer[7] = solver->n_iter;
    buffer[8] = solver->gs_iter;
    buffer[9] = solver->spatial_scheme;
    buffer[10]= solver->time_scheme;
    buffer[11]= solver->screen_op_iter;
    buffer[12]= solver->file_op_iter;
    buffer[13]= solver->restart_iter;
  }
  MPIBroadcast_integer(buffer,buffer_size,0);
  mpi->iproc              = buffer[0];
  mpi->jproc              = buffer[1];
  mpi->kproc              = buffer[2];
  solver->imax_global     = buffer[3];
  solver->jmax_global     = buffer[4];
  solver->kmax_global     = buffer[5];
  solver->ghosts          = buffer[6];
  solver->n_iter          = buffer[7];
  solver->gs_iter         = buffer[8];
  solver->spatial_scheme  = buffer[9];
  solver->time_scheme     = buffer[10];
  solver->screen_op_iter  = buffer[11];
  solver->file_op_iter    = buffer[12];
  solver->restart_iter    = buffer[13];
  free(buffer);
  MPIBroadcast_double(&solver->dt,1,0);
  MPIBroadcast_double(&solver->mu,1,0);
  MPIBroadcast_character(solver->flux_form,_MAX_STRING_SIZE_,0);
  MPIBroadcast_character(solver->write_restart,_MAX_STRING_SIZE_,0);
  MPIBroadcast_character(solver->mode2d,_MAX_STRING_SIZE_,0);
  MPIBroadcast_character(solver->op_file_format,_MAX_STRING_SIZE_,0);
  MPIBroadcast_character(solver->op_overwrite,_MAX_STRING_SIZE_,0);
  MPIBroadcast_character(solver->op_details,_MAX_STRING_SIZE_,0);

  /* calculate 3D rank of each process (ip,jp,kp) from rank in MPI_COMM_WORLD */
  ierr = rank3D(mpi->rank,mpi->iproc,mpi->jproc,mpi->kproc,&mpi->ip,&mpi->jp,&mpi->kp);
  if (ierr) return(ierr);

  /* calculate local domain dimensions */
  ierr = partition1D(solver->imax_global,mpi->iproc,mpi->ip,&solver->imax_local);
  if (ierr) return(ierr);
  ierr = partition1D(solver->jmax_global,mpi->jproc,mpi->jp,&solver->jmax_local);
  if (ierr) return(ierr);
  ierr = partition1D(solver->kmax_global,mpi->kproc,mpi->kp,&solver->kmax_local);
  if (ierr) return(ierr);

  /* calculate local domain limits in terms of global domain */
  ierr = LocalDomainLimits(mpi->rank,mpi,
                           solver->imax_global,solver->jmax_global,solver->kmax_global,
                           &mpi->is,&mpi->ie,&mpi->js,&mpi->je,&mpi->ks,&mpi->ke);
#else
  mpi->ip = mpi->jp = mpi->kp = 0;
  solver->imax_local = solver->imax_global;
  solver->jmax_local = solver->jmax_global;
  solver->kmax_local = solver->kmax_global;
  
  mpi->ip = 0; mpi->iproc = 1; mpi->is = 0; mpi->ie = solver->imax_local;
  mpi->jp = 0; mpi->jproc = 1; mpi->js = 0; mpi->je = solver->jmax_local;
  mpi->kp = 0; mpi->kproc = 1; mpi->ks = 0; mpi->ke = solver->kmax_local;
#endif

  /* Allocations */
  if (!mpi->rank) printf("Allocating data arrays.\n");
  int ni,nj,nk;
  /* global grid coordinates */
  ni = solver->imax_global;
  nj = solver->jmax_global;
  nk = solver->kmax_global;
  solver->xg = (double*) calloc(ni,  sizeof(double));
  solver->xgu= (double*) calloc(ni+1,sizeof(double));
  solver->yg = (double*) calloc(nj,  sizeof(double));
  solver->ygv= (double*) calloc(nj+1,sizeof(double));
  solver->zg = (double*) calloc(nk,  sizeof(double));
  solver->zgw= (double*) calloc(nk+1,sizeof(double));
  ierr = SetArrayValue(solver->xg ,ni  ,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->xgu,ni+1,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->yg ,nj  ,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->ygv,nj+1,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->zg ,nk , 0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->zgw,nk+1,0.0); if (ierr) return(ierr);
  /* local grid coordinates */
  ni = solver->imax_local;
  nj = solver->jmax_local;
  nk = solver->kmax_local;
  solver->x = (double*) calloc(ni,  sizeof(double));
  solver->xu= (double*) calloc(ni+1,sizeof(double));
  solver->y = (double*) calloc(nj,  sizeof(double));
  solver->yv= (double*) calloc(nj+1,sizeof(double));
  solver->z = (double*) calloc(nk,  sizeof(double));
  solver->zw= (double*) calloc(nk+1,sizeof(double));
  ierr = SetArrayValue(solver->x ,ni  ,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->xu,ni+1,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->y ,nj  ,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->yv,nj+1,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->z ,nk , 0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->zw,nk+1,0.0); if (ierr) return(ierr);
  /* local solution */
  ni = solver->imax_local + 2*solver->ghosts;
  nj = solver->jmax_local + 2*solver->ghosts;
  nk = solver->kmax_local + 2*solver->ghosts;
  solver->u    = (double*) calloc((ni+1)*nj*nk,sizeof(double));
  solver->v    = (double*) calloc(ni*(nj+1)*nk,sizeof(double));
  solver->w    = (double*) calloc(ni*nj*(nk+1),sizeof(double));
  solver->phi  = (double*) calloc(ni*nj*nk,    sizeof(double));
  solver->phic = (double*) calloc(ni*nj*nk,    sizeof(double));
  solver->uc   = (double*) calloc(ni*nj*nk,    sizeof(double));
  solver->vc   = (double*) calloc(ni*nj*nk,    sizeof(double));
  solver->wc   = (double*) calloc(ni*nj*nk,    sizeof(double));
  ierr = SetArrayValue(solver->u   ,(ni+1)*nj*nk,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->v   ,ni*(nj+1)*nk,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->w   ,ni*nj*(nk+1),0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->phi ,ni*nj*nk    ,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->uc  ,ni*nj*nk    ,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->vc  ,ni*nj*nk    ,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->wc  ,ni*nj*nk    ,0.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->phic,ni*nj*nk    ,0.0); if (ierr) return(ierr);
  /* iblank arrays for the main and staggered meshes */
  solver->iblanku = (double*) calloc((ni+1)*nj*nk,sizeof(double));
  solver->iblankv = (double*) calloc(ni*(nj+1)*nk,sizeof(double));
  solver->iblankw = (double*) calloc(ni*nj*(nk+1),sizeof(double));
  solver->iblank  = (double*) calloc(ni*nj*nk,    sizeof(double));
  solver->iblankc = (double*) calloc(ni*nj*nk,    sizeof(double));
  ierr = SetArrayValue(solver->iblanku,(ni+1)*nj*nk,1.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->iblankv,ni*(nj+1)*nk,1.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->iblankw,ni*nj*(nk+1),1.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->iblank ,ni*nj*nk    ,1.0); if (ierr) return(ierr);
  ierr = SetArrayValue(solver->iblankc,ni*nj*nk    ,1.0); if (ierr) return(ierr);
  /* RHS arrays */
  ni = solver->imax_local;
  nj = solver->jmax_local;
  nk = solver->kmax_local;
  solver->rhs_u = (double*) calloc((ni+1)*nj*nk,sizeof(double));
  solver->rhs_v = (double*) calloc(ni*(nj+1)*nk,sizeof(double));
  solver->rhs_w = (double*) calloc(ni*nj*(nk+1),sizeof(double));
  solver->divergence = (double*) calloc(ni*nj*nk,sizeof(double));
  solver->uI = (double*) calloc((ni+2)*(nj+2)*(nk+2),sizeof(double));
  solver->vI = (double*) calloc((ni+2)*(nj+2)*(nk+2),sizeof(double));
  solver->wI = (double*) calloc((ni+2)*(nj+2)*(nk+2),sizeof(double));
  /* delta arrays */
  ni = solver->imax_local + 2*solver->ghosts;
  nj = solver->jmax_local + 2*solver->ghosts;
  nk = solver->kmax_local + 2*solver->ghosts;
  solver->du      = (double*) calloc((ni+1)*nj*nk,sizeof(double));
  solver->dv      = (double*) calloc(ni*(nj+1)*nk,sizeof(double));
  solver->dw      = (double*) calloc(ni*nj*(nk+1),sizeof(double));
  solver->prev_du = (double*) calloc((ni+1)*nj*nk,sizeof(double));
  solver->prev_dv = (double*) calloc(ni*(nj+1)*nk,sizeof(double));
  solver->prev_dw = (double*) calloc(ni*nj*(nk+1),sizeof(double));
  /* 1D arrays for interpolation */
  ni = solver->imax_local;
  nj = solver->jmax_local;
  nk = solver->kmax_local;
  int maxdim = (int) max3 ((double)ni+1,(double)nj+1,(double)nk+1);
  solver->qC = (double*) calloc (maxdim+2*solver->ghosts,sizeof(double));
  solver->qI = (double*) calloc (maxdim+1               ,sizeof(double));
  solver->up = (double*) calloc (maxdim+1               ,sizeof(double));
  return(0);
}
