#include <stdio.h>
#include <stdlib.h>
#include <arrayfunctions.h>
#include <mpivars.h>
#include <ibcartins3d.h>

/* Function declarations */
int InterpolateCollToStag  (double*,double*,double*,double*,double*,double*,int,int,int,int);
int InterpolateCollToStag1D(double*,double*,int);

int InitialSolution(void *s, void *m)
{
  IBCartINS3D   *solver = (IBCartINS3D*) s;
  MPIVariables  *mpi    = (MPIVariables*) m;
  int           ierr    = 0;
  int           i,j,k;

  int imax_global = solver->imax_global;
  int jmax_global = solver->jmax_global;
  int kmax_global = solver->kmax_global;
  int imax_local  = solver->imax_local;
  int jmax_local  = solver->jmax_local;
  int kmax_local  = solver->kmax_local;

  /* collocated velocity component arrays */
  double *uc, *vc, *wc;
  int    dim = imax_local * jmax_local * kmax_local;
  uc = (double*) calloc (dim,sizeof(double));
  vc = (double*) calloc (dim,sizeof(double));
  wc = (double*) calloc (dim,sizeof(double));

  /* Only root process reads in initial solution file */
  double *ug, *vg, *wg;
  if (!mpi->rank) {
    int i,j,k;
    /* allocate global solution arrays (collocated) */
    int     dim = imax_global * jmax_global * kmax_global;
    ug = (double*) calloc(dim,sizeof(double));
    vg = (double*) calloc(dim,sizeof(double));
    wg = (double*) calloc(dim,sizeof(double));

    /* Reading grid and initial solution */
    printf("Reading grid and initial conditions from %s.\n",solver->ic_filename);
    FILE *in;
    in = fopen(solver->ic_filename,"r");
    if (!in) {
      fprintf(stderr,"Error: initial solution file %s not found.\n",solver->ic_filename);
      return(1);
    }
    for (i = 0; i < imax_global; i++) ierr = fscanf(in,"%lf",&solver->xg[i]);
    for (j = 0; j < jmax_global; j++) ierr = fscanf(in,"%lf",&solver->yg[j]);
    for (k = 0; k < kmax_global; k++) ierr = fscanf(in,"%lf",&solver->zg[k]);
    for (i = 0; i < imax_global; i++) {
      for (j = 0; j < jmax_global; j++) {
        for (k = 0; k < kmax_global; k++) {
          int p = Index1D(i,j,k,imax_global,jmax_global,kmax_global,0);
          ierr = fscanf(in,"%lf",&ug[p]);
        }
      }
    }
    for (i = 0; i < imax_global; i++) {
      for (j = 0; j < jmax_global; j++) {
        for (k = 0; k < kmax_global; k++) {
          int p = Index1D(i,j,k,imax_global,jmax_global,kmax_global,0);
          ierr = fscanf(in,"%lf",&vg[p]);
        }
      }
    }
    for (i = 0; i < imax_global; i++) {
      for (j = 0; j < jmax_global; j++) {
        for (k = 0; k < kmax_global; k++) {
          int p = Index1D(i,j,k,imax_global,jmax_global,kmax_global,0);
          ierr = fscanf(in,"%lf",&wg[p]);
        }
      }
    }
    fclose(in);

  } else {
    ug = NULL;
    vg = NULL;
    wg = NULL;
  }

  int dim_global[3], dim_local[3];
  dim_global[0] = imax_global;
  dim_global[1] = jmax_global;
  dim_global[2] = kmax_global;
  dim_local[0]  = imax_local;
  dim_local[1]  = jmax_local;
  dim_local[2]  = kmax_local;
  ierr = MPIPartitionArray3D(mpi,ug,uc,&dim_global[0],&dim_local[0]);
  if (ierr) return(ierr);
  ierr = MPIPartitionArray3D(mpi,vg,vc,&dim_global[0],&dim_local[0]);
  if (ierr) return(ierr);
  ierr = MPIPartitionArray3D(mpi,wg,wc,&dim_global[0],&dim_local[0]);
  if (ierr) return(ierr);

  if (!mpi->rank) {
    free(ug);
    free(vg);
    free(wg);
  }

  /* Since xg,yg,zg are one-dimensional arrays,         */
  /* for a Cartesian grid, let all processes have them  */
  MPIBroadcast_double(solver->xg,imax_global,0);
  MPIBroadcast_double(solver->yg,jmax_global,0);
  MPIBroadcast_double(solver->zg,kmax_global,0);
  /* Extract this process' part of the coordinates */
  int is,ie,js,je,ks,ke;
  is = mpi->is; ie = mpi->ie;
  js = mpi->js; je = mpi->je;
  ks = mpi->ks; ke = mpi->ke;
  for (i = is; i < ie; i++) solver->x[i-is] = solver->xg[i];
  for (j = js; j < je; j++) solver->y[j-js] = solver->yg[j];
  for (k = ks; k < ke; k++) solver->z[k-ks] = solver->zg[k];
  /* Calculate staggered x,y,z coordinates for u,v,w */
  ierr = InterpolateCollToStag1D(solver->x, solver->xu, imax_local ); if (ierr) return(ierr);
  ierr = InterpolateCollToStag1D(solver->y, solver->yv, jmax_local ); if (ierr) return(ierr);
  ierr = InterpolateCollToStag1D(solver->z, solver->zw, kmax_local ); if (ierr) return(ierr);
  ierr = InterpolateCollToStag1D(solver->xg,solver->xgu,imax_global); if (ierr) return(ierr);
  ierr = InterpolateCollToStag1D(solver->yg,solver->ygv,jmax_global); if (ierr) return(ierr);
  ierr = InterpolateCollToStag1D(solver->zg,solver->zgw,kmax_global); if (ierr) return(ierr);

  /* Convert collocated initial solution to staggered grid */
  double *u = solver->u;
  double *v = solver->v;
  double *w = solver->w;
  ierr = InterpolateCollToStag(uc,vc,wc,u,v,w,imax_local,jmax_local,kmax_local,solver->ghosts);
  if (ierr) return(ierr);

  free(uc);
  free(vc);
  free(wc);
  return(0);
}
