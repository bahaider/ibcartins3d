#include <stdio.h>
#include <interpolation.h>

/* 
  Fourth order central interpolation (uniform grid)
*/

#undef  _MINIMUM_GHOSTS_
#define _MINIMUM_GHOSTS_ 2

int FourthOrderCentral(double *fI,double *fC,int N,int ghosts,int rank,int nproc,void *p)
{
  if ((!fI) || (!fC)) {
    fprintf(stderr, "Error in FourthOrderCentral(): input arrays not allocated.\n");
    return(1);
  }
  if (ghosts < _MINIMUM_GHOSTS_) {
    fprintf(stderr, "Error in FourthOrderCentral(): insufficient number of ghosts.\n");
    return(1);
  }

  int i;
  double one_by_twelve = 1.0/12.0;

  for (i = 0; i < N+1; i++) 
    fI[i] =   7*one_by_twelve * (fC[ghosts+i-1] + fC[ghosts+i]  )
            - one_by_twelve   * (fC[ghosts+i-2] + fC[ghosts+i+1]);
  
  return(0);
}
