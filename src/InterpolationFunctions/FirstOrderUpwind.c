#include <stdio.h>
#include <interpolation.h>

/* 
  First order upwind interpolation (uniform grid)
*/

#undef  _MINIMUM_GHOSTS_
#define _MINIMUM_GHOSTS_ 1

int FirstOrderUpwind(double *fI,double *fC,int N,int ghosts,int rank,int nproc,void *p)
{
  InterpolationParameters *params = (InterpolationParameters*) p;

  if ((!fI) || (!fC)) {
    fprintf(stderr, "Error in FirstOrderUpwind(): input arrays not allocated.\n");
    return(1);
  }
  if (ghosts < _MINIMUM_GHOSTS_) {
    fprintf(stderr, "Error in FirstOrderUpwind(): insufficient number of ghosts.\n");
    return(1);
  }
  if (!params->upw) {
    fprintf(stderr, "Error in FirstOrderUpwind(): Array upw cannot be NULL for an upwind scheme.\n");
    return(1);
  }

  int i;
  for (i = 0; i < N+1; i++) fI[i] = (params->upw[i] > 0 ? fC[ghosts+i-1] : fC[ghosts+i]);
  
  return(0);
}
