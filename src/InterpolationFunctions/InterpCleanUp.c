#include <stdlib.h>
#include <interpolation.h>

int InterpCleanUp(void *s)
{
  InterpolationParameters *interp = (InterpolationParameters*) s;
  if (interp->scheme) free(interp->scheme);
  return(0);
}
