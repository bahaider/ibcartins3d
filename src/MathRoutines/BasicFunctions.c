#include <math.h>

double min(double a, double b)
{
  if (a < b)  return(a);
  else        return(b);
}

double max(double a, double b)
{
  if (a > b)  return(a);
  else        return(b);
}

double min3(double a, double b, double c)
{
  return (min(min(a,b),min(a,c)));
}

double max3(double a, double b, double c)
{
  return (max(max(a,b),max(a,c)));
}

double absolute(double a)
{
  if (a < 0) return(-a);
  else       return( a);
}

double raiseto(double x,double a)
{
  return (exp(a*log(x)));
}

double sign(double a)
{
  return (a < 0 ? -1.0 : 1.0);
}

void FindInterval(double a, double b, double *x, int N, int *imin, int *imax)
{
  int i;
  *imax = -1;
  *imin =  N;

  for (i = 0; i < N; i++) {
    if (x[i] <= b) *imax = i+1;
  }
  for (i = N-1; i > -1; i--) {
    if (x[i] >= a) *imin = i;
  }

  return;
}

double distance(double x1,double y1,double z1,double x2,double y2,double z2)
{
  double distance = 0;
  distance += (x1-x2)*(x1-x2);
  distance += (y1-y2)*(y1-y2);
  distance += (z1-z2)*(z1-z2);
  distance = sqrt(distance);
  return(distance);
}

double TriangleArea(double x1,double y1,double z1,
                    double x2,double y2,double z2,
                    double x3, double y3, double z3)
{
  double a, b, c, s;
  a = distance(x1, y1, z1, x2, y2, z2);
  b = distance(x2, y2, z2, x3, y3, z3);
  c = distance(x3, y3, z3, x1, y1, z1);
  s = 0.5 * (a + b + c);
  double area = sqrt(s*(s-a)*(s-b)*(s-c));
  return area;
}
