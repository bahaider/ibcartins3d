double TrilinearInterpolation(double *x,double *y,double *z,double *q,double a,double b,double c)
{
  int i;
  double tlx1, tlx2, tly1, tly2, tlz1, tlz2;
	tlx1 = x[0];
	tlx2 = x[1];
	tly1 = y[0];
	tly2 = y[1];
	tlz1 = z[0];
	tlz2 = z[1];

	double vol_inv = 1 / ((tlx2-tlx1)*(tly2-tly1)*(tlz2-tlz1));
	double tldx1 = a - tlx1;
	double tldx2 = tlx2 - a;
	double tldy1 = b - tly1;
	double tldy2 = tly2 - b;
	double tldz1 = c - tlz1;
	double tldz2 = tlz2 - c;

  double tlc[8];
	tlc[0] = tldz2 * tldy2 * tldx2 * vol_inv;
	tlc[1] = tldz2 * tldy2 * tldx1 * vol_inv;
	tlc[2] = tldz2 * tldy1 * tldx2 * vol_inv;
	tlc[3] = tldz2 * tldy1 * tldx1 * vol_inv;
	tlc[4] = tldz1 * tldy2 * tldx2 * vol_inv;
	tlc[5] = tldz1 * tldy2 * tldx1 * vol_inv;
	tlc[6] = tldz1 * tldy1 * tldx2 * vol_inv;
	tlc[7] = tldz1 * tldy1 * tldx1 * vol_inv;

  double qinterp = 0;
  for (i=0; i<8; i++) qinterp += tlc[i]*q[i];
	return(qinterp);	
}
