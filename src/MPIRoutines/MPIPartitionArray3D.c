#include <stdio.h>
#include <stdlib.h>
#include <arrayfunctions.h>
#ifndef serial
#include <mpi.h>
#endif
#include <mpivars.h>

int MPIPartitionArray3D(void *m,double *xg,double *x,int *dim_global,int *dim_local)
{
  MPIVariables *mpi = (MPIVariables*) m;
  int          ierr = 0;

  int imax_global = dim_global[0];
  int jmax_global = dim_global[1];
  int kmax_global = dim_global[2];
  int imax_local  = dim_local[0];
  int jmax_local  = dim_local[1];
  int kmax_local  = dim_local[2];

  /* xg should be non-null only on root */
  if (mpi->rank && xg) {
    fprintf(stderr,"Error in MPIPartitionArray3D: global array exists on non-root processors.\n");
    ierr = 1;
  }
  if ((!mpi->rank) && (!xg)) {
    fprintf(stderr,"Error in MPIPartitionArray3D: global array is not allocated on root processor.\n");
    ierr = 1;
  }

  if (!mpi->rank) {
    int proc;
    for (proc = 0; proc < mpi->nproc; proc++) {
      /* Find out the domain limits for each process */
      int is,ie,js,je,ks,ke;
      ierr = LocalDomainLimits(proc,mpi,imax_global,jmax_global,kmax_global,
                               &is,&ie,&js,&je,&ks,&ke);
      if (ierr) return(ierr);
      int     dim = (ie-is) * (je-js) * (ke-ks);
      double *buffer = (double*) calloc (dim,sizeof(double));
      int     i,j,k;
      for (i = is; i < ie; i++) {
        for (j = js; j < je; j++) {
          for (k = ks; k < ke; k++) {
            int p1     = Index1D(i,j,k,imax_global,jmax_global,kmax_global,0);
            int p2     = Index1D(i-is,j-js,k-ks,ie-is,je-js,ke-ks,0);
            buffer[p2] = xg[p1];
          }
        }
      }
      if (proc) {
#ifndef serial
        MPI_Send(buffer,dim,MPI_DOUBLE,proc,1538,MPI_COMM_WORLD);
#else
        fprintf(stderr,"Error in MPIPartitionArray3D(): This is a serial run; number of processes ");
        fprintf(stderr,"should be 1. Instead it is %d.\n",mpi->nproc);
        return(1);
#endif
      } else {
        for (i = 0; i < imax_local; i++) {
          for (j = 0; j < jmax_local; j++) {
            for (k = 0; k < kmax_local; k++) {
              int p = Index1D(i,j,k,imax_local,jmax_local,kmax_local,0);
              x[p]  = buffer[p];
            }
          }
        }
      }
      free(buffer);
    }

  } else {
#ifdef serial
    fprintf(stderr,"Error in MPIPartitionArray3D(): This is a serial run; process rank ");
    fprintf(stderr,"should be 0. Instead it is %d.\n",mpi->rank);
    return(1);
#else
    /* Meanwhile, on other processes */
    int         i,j,k;
    MPI_Status  status;
    int         dim = imax_local*jmax_local*kmax_local;
    double      *buffer = (double*) calloc (dim,sizeof(double));
    MPI_Recv(buffer,dim,MPI_DOUBLE,0,1538,MPI_COMM_WORLD,&status);
    for (i = 0; i < imax_local; i++) {
      for (j = 0; j < jmax_local; j++) {
        for (k = 0; k < kmax_local; k++) {
          int p = Index1D(i,j,k,imax_local,jmax_local,kmax_local,0);
          x[p] = buffer[p];
        }
      }
    }
    free(buffer);
#endif
  }
  return(ierr);
}
