#include <stdio.h>
#include <mpivars.h>

int rank1D(int *rank,int iproc,int jproc,int kproc,int ip,int jp,int kp)
{
  *rank = ip + iproc*jp + iproc*jproc*kp;
  return(0);
}

int rank3D(int rank,int iproc,int jproc,int kproc,int *ip,int *jp,int *kp)
{
  *kp = rank/(iproc*jproc);
  *jp = (rank%(iproc*jproc))/iproc;
  *ip = (rank%(iproc*jproc))%iproc;
  if (rank != ((*kp)*iproc*jproc+(*jp)*iproc+(*ip))) {
    printf("Error: Bug in the code - calculating 3D rank of each process.\n");
    return(1);
  }
  return(0);
}

int partition1D(int nglobal,int nproc,int rank,int* nlocal)
{
  if (nglobal%nproc == 0) *nlocal = nglobal/nproc;
  else {
    if (rank == nproc-1)  *nlocal = nglobal/nproc+nglobal%nproc;
    else                  *nlocal = nglobal/nproc;
  }
  return(0);
}

int LocalDomainLimits(int p,void *m,int imax, int jmax, int kmax, 
                      int* is, int *ie, int *js, int *je, int *ks, int *ke) {
  MPIVariables *mpi = (MPIVariables*) m;
  int           ip, jp, kp, ierr = 0;
  ierr = rank3D(p,mpi->iproc,mpi->jproc,mpi->kproc,&ip,&jp,&kp);
  if (ierr) return(ierr);

  int imax_local, isize;
  int jmax_local, jsize;
  int kmax_local, ksize;
  ierr = partition1D(imax,mpi->iproc,0, &imax_local);
  if (ierr) return(ierr);
  ierr = partition1D(imax,mpi->iproc,ip,&isize);
  if (ierr) return(ierr);
  ierr = partition1D(jmax,mpi->jproc,0, &jmax_local);
  if (ierr) return(ierr);
  ierr = partition1D(jmax,mpi->jproc,jp,&jsize);
  if (ierr) return(ierr);
  ierr = partition1D(kmax,mpi->kproc,0, &kmax_local);
  if (ierr) return(ierr);
  ierr = partition1D(kmax,mpi->kproc,kp,&ksize);
  if (ierr) return(ierr);
  if (is)  *is = ip*imax_local;
  if (ie)  *ie = ip*imax_local + isize;
  if (js)  *js = jp*jmax_local;
  if (je)  *je = jp*jmax_local + jsize;
  if (ks)  *ks = kp*kmax_local;
  if (ke)  *ke = kp*kmax_local + ksize;
  return(0);
}
