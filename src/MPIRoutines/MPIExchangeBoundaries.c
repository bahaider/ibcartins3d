#include <stdio.h>
#include <stdlib.h>
#include <arrayfunctions.h>
#ifndef serial
#include <mpi.h>
#endif
#include <mpivars.h>

int MPIExchangeBoundaries(int imax,int jmax,int kmax,int ghosts,
                          int xshift,int yshift,int zshift,
                          void *m,double *var)
{
#ifndef serial
  MPIVariables  *mpi = (MPIVariables*) m;
  MPI_Request   *requests;
  MPI_Status    *statuses;
  int           ierr = 0;
  
  int ip = mpi->ip;
  int jp = mpi->jp;
  int kp = mpi->kp;

  int iproc = mpi->iproc;
  int jproc = mpi->jproc;
  int kproc = mpi->kproc;
  int n_neighbors = 0;

  /* each process has 6 neighbors (except at physical boundaries) */
  /* calculate the rank of these neighbors (-1 -> none)           */
  int neighbor_rank[6]; /* Order: i-,i+,j-,j+,k-,k+ */
  if (ip == 0)          neighbor_rank[0] = -1;
  else                  ierr = rank1D(&neighbor_rank[0],mpi->iproc,mpi->jproc,mpi->kproc,ip-1,jp,kp);
  if (ierr)             return(ierr);
  if (ip == (iproc-1))  neighbor_rank[1] = -1;
  else                  ierr = rank1D(&neighbor_rank[1],mpi->iproc,mpi->jproc,mpi->kproc,ip+1,jp,kp);
  if (ierr)             return(ierr);
  if (jp == 0)          neighbor_rank[2] = -1;
  else                  ierr = rank1D(&neighbor_rank[2],mpi->iproc,mpi->jproc,mpi->kproc,ip,jp-1,kp);
  if (ierr)             return(ierr);
  if (jp == (jproc-1))  neighbor_rank[3] = -1;
  else                  ierr = rank1D(&neighbor_rank[3],mpi->iproc,mpi->jproc,mpi->kproc,ip,jp+1,kp);
  if (ierr)             return(ierr);
  if (kp == 0)          neighbor_rank[4] = -1;
  else                  ierr = rank1D(&neighbor_rank[4],mpi->iproc,mpi->jproc,mpi->kproc,ip,jp,kp-1);
  if (ierr)             return(ierr);
  if (kp == (kproc-1))  neighbor_rank[5] = -1;
  else                  ierr = rank1D(&neighbor_rank[5],mpi->iproc,mpi->jproc,mpi->kproc,ip,jp,kp+1);
  if (ierr)             return(ierr);

  /* calculate dimensions of each of the send-receive regions */
  int dim[6];
  dim[0] = dim[1] = ghosts * jmax   * kmax;
  dim[2] = dim[3] = imax   * ghosts * kmax;
  dim[4] = dim[5] = imax   * jmax   * ghosts;
  
  /* Allocate send and receive buffers, depending on existence of neighbor */
  /* and copy the data to be sent to the send buffers                      */
  /* Also count the number of neighbors                                    */
  double *sendbuf_imin,*sendbuf_imax,*sendbuf_jmin,*sendbuf_jmax,*sendbuf_kmin,*sendbuf_kmax;
  double *recvbuf_imin,*recvbuf_imax,*recvbuf_jmin,*recvbuf_jmax,*recvbuf_kmin,*recvbuf_kmax;
  sendbuf_imin = (double*) calloc (dim[0],sizeof(double));
  recvbuf_imin = (double*) calloc (dim[0],sizeof(double));
  sendbuf_imax = (double*) calloc (dim[1],sizeof(double));
  recvbuf_imax = (double*) calloc (dim[1],sizeof(double));
  sendbuf_jmin = (double*) calloc (dim[2],sizeof(double));
  recvbuf_jmin = (double*) calloc (dim[2],sizeof(double));
  sendbuf_jmax = (double*) calloc (dim[3],sizeof(double));
  recvbuf_jmax = (double*) calloc (dim[3],sizeof(double));
  sendbuf_kmin = (double*) calloc (dim[4],sizeof(double));
  recvbuf_kmin = (double*) calloc (dim[4],sizeof(double));
  sendbuf_kmax = (double*) calloc (dim[5],sizeof(double));
  recvbuf_kmax = (double*) calloc (dim[5],sizeof(double));

  n_neighbors = 0;
  if (neighbor_rank[0] != -1) {
    n_neighbors++;
    int i,j,k;
    for (i = 0; i < ghosts; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < kmax; k++) {
          int p = Index1D(i,j,k,ghosts,jmax,kmax,0);
          int q = Index1D(i+xshift,j,k,imax,jmax,kmax,ghosts);
          sendbuf_imin[p] = var[q];
        }
      }
    }
  }
  if (neighbor_rank[1] != -1) {
    n_neighbors++;
    int i,j,k;
    for (i = 0; i < ghosts; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < kmax; k++) {
          int p = Index1D(i,j,k,ghosts,jmax,kmax,0);
          int q = Index1D(i+imax-ghosts-xshift,j,k,imax,jmax,kmax,ghosts);
          sendbuf_imax[p] = var[q];
        }
      }
    }
  }
  if (neighbor_rank[2] != -1) {
    n_neighbors++;
    int i,j,k;
    for (i = 0; i < imax; i++) {
      for (j = 0; j < ghosts; j++) {
        for (k = 0; k < kmax; k++) {
          int p = Index1D(i,j,k,imax,ghosts,kmax,0);
          int q = Index1D(i,j+yshift,k,imax,jmax,kmax,ghosts);
          sendbuf_jmin[p] = var[q];
        }
      }
    }
  }
  if (neighbor_rank[3] != -1) {
    n_neighbors++;
    int i,j,k;
    for (i = 0; i < imax; i++) {
      for (j = 0; j < ghosts; j++) {
        for (k = 0; k < kmax; k++) {
          int p = Index1D(i,j,k,imax,ghosts,kmax,0);
          int q = Index1D(i,j+jmax-ghosts-yshift,k,imax,jmax,kmax,ghosts);
          sendbuf_jmax[p] = var[q];
        }
      }
    }
  }
  if (neighbor_rank[4] != -1) {
    n_neighbors++;
    int i,j,k;
    for (i = 0; i < imax; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < ghosts; k++) {
          int p = Index1D(i,j,k,imax,jmax,ghosts,0);
          int q = Index1D(i,j,k+zshift,imax,jmax,kmax,ghosts);
          sendbuf_kmin[p] = var[q];
        }
      }
    }
  }
  if (neighbor_rank[5] != -1) {
    n_neighbors++;
    int i,j,k;
    for (i = 0; i < imax; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < ghosts; k++) {
          int p = Index1D(i,j,k,imax,jmax,ghosts,0);
          int q = Index1D(i,j,k+kmax-ghosts-zshift,imax,jmax,kmax,ghosts);
          sendbuf_kmax[p] = var[q];
        }
      }
    }
  }

  requests = (MPI_Request*) calloc(2*n_neighbors,sizeof(MPI_Request));
  statuses = (MPI_Status* ) calloc(2*n_neighbors,sizeof(MPI_Status ));

  /* exchange the data */
  int tick = 0;
  if (neighbor_rank[0] != -1) {
    MPI_Irecv(recvbuf_imin,dim[0],MPI_DOUBLE,neighbor_rank[0],1630,
              MPI_COMM_WORLD,&requests[tick]);
    MPI_Isend(sendbuf_imin,dim[0],MPI_DOUBLE,neighbor_rank[0],1630,
              MPI_COMM_WORLD,&requests[tick+n_neighbors]);
    tick++;
  }
  if (neighbor_rank[1] != -1) {
    MPI_Irecv(recvbuf_imax,dim[1],MPI_DOUBLE,neighbor_rank[1],1630,
              MPI_COMM_WORLD,&requests[tick]);
    MPI_Isend(sendbuf_imax,dim[1],MPI_DOUBLE,neighbor_rank[1],1630,
              MPI_COMM_WORLD,&requests[tick+n_neighbors]);
    tick++;
  }
  if (neighbor_rank[2] != -1) {
    MPI_Irecv(recvbuf_jmin,dim[2],MPI_DOUBLE,neighbor_rank[2],1630,
              MPI_COMM_WORLD,&requests[tick]);
    MPI_Isend(sendbuf_jmin,dim[2],MPI_DOUBLE,neighbor_rank[2],1630,
              MPI_COMM_WORLD,&requests[tick+n_neighbors]);
    tick++;
  }
  if (neighbor_rank[3] != -1) {
    MPI_Irecv(recvbuf_jmax,dim[3],MPI_DOUBLE,neighbor_rank[3],1630,
              MPI_COMM_WORLD,&requests[tick]);
    MPI_Isend(sendbuf_jmax,dim[3],MPI_DOUBLE,neighbor_rank[3],1630,
              MPI_COMM_WORLD,&requests[tick+n_neighbors]);
    tick++;
  }
  if (neighbor_rank[4] != -1) {
    MPI_Irecv(recvbuf_kmin,dim[4],MPI_DOUBLE,neighbor_rank[4],1630,
              MPI_COMM_WORLD,&requests[tick]);
    MPI_Isend(sendbuf_kmin,dim[4],MPI_DOUBLE,neighbor_rank[4],1630,
              MPI_COMM_WORLD,&requests[tick+n_neighbors]);
    tick++;
  }
  if (neighbor_rank[5] != -1) {
    MPI_Irecv(recvbuf_kmax,dim[5],MPI_DOUBLE,neighbor_rank[5],1630,
              MPI_COMM_WORLD,&requests[tick]);
    MPI_Isend(sendbuf_kmax,dim[5],MPI_DOUBLE,neighbor_rank[5],1630,
              MPI_COMM_WORLD,&requests[tick+n_neighbors]);
    tick++;
  }

  /* Wait till data transfer is done */
  MPI_Waitall(2*n_neighbors,requests,statuses);
  if (requests) free(requests);
  if (statuses) free(statuses);

  /* copy received data to ghost points */
  if (neighbor_rank[0] != -1) {
    int i,j,k;
    for (i = 0; i < ghosts; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < kmax; k++) {
          int p = Index1D(i,j,k,ghosts,jmax,kmax,0);
          int q = Index1D(i-ghosts,j,k,imax,jmax,kmax,ghosts);
          var[q] = recvbuf_imin[p];
        }
      }
    }
  }
  if (neighbor_rank[1] != -1) {
    int i,j,k;
    for (i = 0; i < ghosts; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < kmax; k++) {
          int p = Index1D(i,j,k,ghosts,jmax,kmax,0);
          int q = Index1D(i+imax,j,k,imax,jmax,kmax,ghosts);
          var[q] = recvbuf_imax[p];
        }
      }
    }
  }
  if (neighbor_rank[2] != -1) {
    int i,j,k;
    for (i = 0; i < imax; i++) {
      for (j = 0; j < ghosts; j++) {
        for (k = 0; k < kmax; k++) {
          int p = Index1D(i,j,k,imax,ghosts,kmax,0);
          int q = Index1D(i,j-ghosts,k,imax,jmax,kmax,ghosts);
          var[q] = recvbuf_jmin[p];
        }
      }
    }
  }
  if (neighbor_rank[3] != -1) {
    int i,j,k;
    for (i = 0; i < imax; i++) {
      for (j = 0; j < ghosts; j++) {
        for (k = 0; k < kmax; k++) {
          int p = Index1D(i,j,k,imax,ghosts,kmax,0);
          int q = Index1D(i,j+jmax,k,imax,jmax,kmax,ghosts);
          var[q] = recvbuf_jmax[p];
        }
      }
    }
  }
  if (neighbor_rank[4] != -1) {
    int i,j,k;
    for (i = 0; i < imax; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < ghosts; k++) {
          int p = Index1D(i,j,k,imax,jmax,ghosts,0);
          int q = Index1D(i,j,k-ghosts,imax,jmax,kmax,ghosts);
          var[q] = recvbuf_kmin[p];
        }
      }
    }
  }
  if (neighbor_rank[5] != -1) {
    int i,j,k;
    for (i = 0; i < imax; i++) {
      for (j = 0; j < jmax; j++) {
        for (k = 0; k < ghosts; k++) {
          int p = Index1D(i,j,k,imax,jmax,ghosts,0);
          int q = Index1D(i,j,k+kmax,imax,jmax,kmax,ghosts);
          var[q] = recvbuf_kmax[p];
        }
      }
    }
  }

  free(sendbuf_imin);
  free(recvbuf_imin);
  free(sendbuf_imax);
  free(recvbuf_imax);
  free(sendbuf_jmin);
  free(recvbuf_jmin);
  free(sendbuf_jmax);
  free(recvbuf_jmax); 
  free(sendbuf_kmin);
  free(recvbuf_kmin);
  free(sendbuf_kmax);
  free(recvbuf_kmax);
#endif
  return(0);
}

