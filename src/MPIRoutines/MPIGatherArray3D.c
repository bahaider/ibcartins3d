#include <stdio.h>
#include <stdlib.h>
#include <arrayfunctions.h>
#ifndef serial
#include <mpi.h>
#endif
#include <mpivars.h>

int MPIGatherArray3D(void *m,double* xg,double *x,int *dim_global,int *dim_local,int overlap)
{
  MPIVariables *mpi = (MPIVariables*) m;
  int          ierr = 0;

  int imax_global = dim_global[0];
  int jmax_global = dim_global[1];
  int kmax_global = dim_global[2];
  int imax_local  = dim_local[0];
  int jmax_local  = dim_local[1];
  int kmax_local  = dim_local[2];

  /* Create buffer to send to root proc */
  int dim =  (imax_local+2*overlap)
            *(jmax_local+2*overlap)
            *(kmax_local+2*overlap);
  double *buffer = (double*) calloc (dim,sizeof(double));
  int p;  for (p = 0; p < dim; p++) buffer[p] = x[p];

  if (!mpi->rank) {
    /* On root processor */
    if (!xg) {
      fprintf(stderr,"Error in MPIGatherArray3D: global array not allocated on root process.\n");
      ierr = 1;
    }

    /* Fill the global arrays with data from each process */
    int proc;
    for (proc = 0; proc < mpi->nproc; proc++){
      int i, j, k;

      /* Find out the domain limits for each process */
      int is,ie,js,je,ks,ke;
      ierr = LocalDomainLimits(proc,mpi,imax_global,jmax_global,kmax_global,
                               &is,&ie,&js,&je,&ks,&ke);
      if (ierr) return(ierr);

      if (!proc) {
        /* for a "star" configuration of overlap regions, copy the overlap regions */
        /* separately, one by one, to avoid the corners, which have junk values    */
        /* Main region */
        for (i = is; i < ie; i++) {
          for (j = js; j < je; j++) {
            for (k = ks; k < ke; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = buffer[q]; 
            }
          }
        }
        /* imin overlap region */
        for (i = is-overlap; i < is; i++) {
          for (j = js; j < je; j++) {
            for (k = ks; k < ke; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = buffer[q]; 
            }
          }
        }
        /* imax overlap region */
        for (i = ie; i < ie+overlap; i++) {
          for (j = js; j < je; j++) {
            for (k = ks; k < ke; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = buffer[q]; 
            }
          }
        }
        /* jmin overlap region */
        for (i = is; i < ie; i++) {
          for (j = js-overlap; j < js; j++) {
            for (k = ks; k < ke; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = buffer[q]; 
            }
          }
        }
        /* jmax overlap region */
        for (i = is; i < ie; i++) {
          for (j = je; j < je+overlap; j++) {
            for (k = ks; k < ke; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = buffer[q]; 
            }
          }
        }
        /* kmin overlap region */
        for (i = is; i < ie; i++) {
          for (j = js; j < je; j++) {
            for (k = ks-overlap; k < ks; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = buffer[q]; 
            }
          }
        }
        /* kmax overlap region */
        for (i = is; i < ie; i++) {
          for (j = js; j < je; j++) {
            for (k = ke; k < ke+overlap; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = buffer[q]; 
            }
          }
        }
      } else {
#ifndef serial
        int dim = (ie-is+2*overlap)*(je-js+2*overlap)*(ke-ks+2*overlap);
        /* receive data from other processes */
        double    *recvbuf = (double*) calloc (dim, sizeof(double));
        MPI_Status status;
        MPI_Recv(recvbuf,dim,MPI_DOUBLE,proc,1812,MPI_COMM_WORLD,&status);
        /* for a "star" configuration of overlap regions, copy the overlap regions */
        /* separately, one by one, to avoid the corners, which have junk values    */
        /* Main region */
        for (i = is; i < ie; i++) {
          for (j = js; j < je; j++) {
            for (k = ks; k < ke; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = recvbuf[q]; 
            }
          }
        }
        /* imin overlap region */
        for (i = is-overlap; i < is; i++) {
          for (j = js; j < je; j++) {
            for (k = ks; k < ke; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = recvbuf[q]; 
            }
          }
        }
        /* imax overlap region */
        for (i = ie; i < ie+overlap; i++) {
          for (j = js; j < je; j++) {
            for (k = ks; k < ke; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = recvbuf[q]; 
            }
          }
        }
        /* jmin overlap region */
        for (i = is; i < ie; i++) {
          for (j = js-overlap; j < js; j++) {
            for (k = ks; k < ke; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = recvbuf[q]; 
            }
          }
        }
        /* jmax overlap region */
        for (i = is; i < ie; i++) {
          for (j = je; j < je+overlap; j++) {
            for (k = ks; k < ke; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = recvbuf[q]; 
            }
          }
        }
        /* kmin overlap region */
        for (i = is; i < ie; i++) {
          for (j = js; j < je; j++) {
            for (k = ks-overlap; k < ks; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = recvbuf[q]; 
            }
          }
        }
        /* kmax overlap region */
        for (i = is; i < ie; i++) {
          for (j = js; j < je; j++) {
            for (k = ke; k < ke+overlap; k++) {
              int p = Index1D(i   ,j   ,k   ,imax_global,jmax_global,kmax_global,overlap);
              int q = Index1D(i-is,j-js,k-ks,ie-is      ,je-js      ,ke-ks      ,overlap);
              xg[p] = recvbuf[q]; 
            }
          }
        }
        free(recvbuf);
#else
        fprintf(stderr,"Error in MPIGatherArray3D(): This is a serial run, number of processes ");
        fprintf(stderr,"should be 1. Instead, it is %d.\n",mpi->nproc);
        return(1);
#endif
      }

    }

  } else {
#ifndef serial
    /* Meanwhile on other processors */

    if (xg) {
      fprintf(stderr,"Error in MPIGatherArray3D: global array allocated on non-root process.\n");
      ierr = 1;
    }

    MPI_Send(buffer,dim,MPI_DOUBLE,0,1812,MPI_COMM_WORLD);
#else
    fprintf(stderr,"Error in MPIGatherArray3D(): This is a serial run, rank should be zero.");
    fprintf(stderr,"Instead, it is %d.\n",mpi->rank);
    return(1);
#endif
  }

  free(buffer);
  return(ierr);
}
