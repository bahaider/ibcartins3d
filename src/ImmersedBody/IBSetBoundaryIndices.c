#include <arrayfunctions.h>
#include <immersedbody.h>

int IBSetBoundaryIndices(int imax,int jmax,int kmax,int ghosts,double *blank,void *b)
{
  BoundaryNode *boundary = (BoundaryNode*) b;

  int i, j, k;
  int count = 0;
	for (i = 0; i < imax; i++) {
		for (j = 0; j < jmax; j++) {
			for (k = 0; k < kmax; k++) {
				int p = Index1D(i,j,k,imax,jmax,kmax,ghosts);
        /* if this point is inside the body (0), find out if any */
        /* of the neighboring points are outside (1)              */
				if (!blank[p]){
          int g;
          int flag = 0;
					for (g = 1; g <= ghosts; g++){
            int q;

            q = Index1D(i+g,j,k,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

            q = Index1D(i-g,j,k,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

            q = Index1D(i,j+g,k,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

            q = Index1D(i,j-g,k,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

            q = Index1D(i,j,k+g,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;

            q = Index1D(i,j,k-g,imax,jmax,kmax,ghosts);
						if (blank[q])	flag = 1;
					}
          if (flag) {
            /* Note: boundary indices are set without ghosts offset */
            boundary[count].i = i;
            boundary[count].j = j;
            boundary[count].k = k;
            count++;
          }
				}
			}
		}
	}
  return(0);
}
