#include <stdio.h>
#include <stdlib.h>
#include <arrayfunctions.h>
#include <immersedbody.h>
#include <mathroutines.h>

/*
  dim should be an integer array of dimension 7
  first three elements should be global dimensions
  next three elements should be local dimensions
  and the final element should be no. of ghosts
*/

int IBIdentifyBody(void *ib,int *dim,int *limits,
                   double *x,double *y,double *z,double *blank)
{
  ImmersedBody *IB = (ImmersedBody*) ib;
	int           i, j, k, n, dg;
  double        eps = IBTolerance;

  /* Calculating bounds of the immersed body */
	double xmax, xmin, ymax, ymin, zmax, zmin;
	xmax = IB->surface[0].x1;	
	xmin = IB->surface[0].x1;	
	ymax = IB->surface[0].y1;	
	ymin = IB->surface[0].y1;	
	zmax = IB->surface[0].z1;	
	zmin = IB->surface[0].z1;
	for (n = 0; n < IB->nfacets; n++) {
		if (IB->surface[n].x1 > xmax)	xmax = IB->surface[n].x1;
		if (IB->surface[n].y1 > ymax)	ymax = IB->surface[n].y1;
		if (IB->surface[n].z1 > zmax)	zmax = IB->surface[n].z1;
		if (IB->surface[n].x2 > xmax)	xmax = IB->surface[n].x2;
		if (IB->surface[n].y2 > ymax)	ymax = IB->surface[n].y2;
		if (IB->surface[n].z2 > zmax)	zmax = IB->surface[n].z2;
		if (IB->surface[n].x3 > xmax)	xmax = IB->surface[n].x3;
		if (IB->surface[n].y3 > ymax)	ymax = IB->surface[n].y3;
		if (IB->surface[n].z3 > zmax)	zmax = IB->surface[n].z3;

		if (IB->surface[n].x1 < xmin)	xmin = IB->surface[n].x1;
		if (IB->surface[n].y1 < ymin)	ymin = IB->surface[n].y1;
		if (IB->surface[n].z1 < zmin)	zmin = IB->surface[n].z1;
		if (IB->surface[n].x2 < xmin)	xmin = IB->surface[n].x2;
		if (IB->surface[n].y2 < ymin)	ymin = IB->surface[n].y2;
		if (IB->surface[n].z2 < zmin)	zmin = IB->surface[n].z2;
		if (IB->surface[n].x3 < xmin)	xmin = IB->surface[n].x3;
		if (IB->surface[n].y3 < ymin)	ymin = IB->surface[n].y3;
		if (IB->surface[n].z3 < zmin)	zmin = IB->surface[n].z3;
	}
  IB->xmax = xmax; IB->xmin = xmin;
  IB->ymax = ymax; IB->ymin = ymin;
  IB->zmax = zmax; IB->zmin = zmin;
  
  double fac = 1.5;
	double Lx, Ly, Lz;
	Lx = (xmax - xmin);
	Ly = (ymax - ymin);
	Lz = (zmax - zmin);
	double xc, yc, zc;
	xc = 0.5 * (xmin + xmax);
	yc = 0.5 * (ymin + ymax);
	zc = 0.5 * (zmin + zmax);
	xmax = xc + fac * Lx/2;
	xmin = xc - fac * Lx/2;
	ymax = yc + fac * Ly/2;
	ymin = yc - fac * Ly/2;
	zmax = zc + fac * Lz/2;
	zmin = zc - fac * Lz/2;

	int imin, imax, jmin, jmax, kmin, kmax;
	imin = dim[0]-1;	imax = 0;
	jmin = dim[1]-1;	jmax = 0;
	kmin = dim[2]-1;	kmax = 0;
	for (i = 0; i < dim[0]; i++) {
		for (j = 0; j < dim[1]; j++) {
			for (k = 0; k < dim[2]; k++) {
				if (   ((x[i]-xmin)*(x[i]-xmax) < 0) 
            && ((y[j]-ymin)*(y[j]-ymax) < 0) 
            && ((z[k]-zmin)*(z[k]-zmax) < 0)) {
					imin = min(i, imin);
					imax = max(i, imax);
					jmin = min(j, jmin);
					jmax = max(j, jmax);
					kmin = min(k, kmin);
					kmax = max(k, kmax);
				}
			}
		}
	}

	double *cof[5];
	double xd[500], dist[500];
	for (dg = 0; dg < 5; dg++) 
		cof[dg] = (double*) calloc(IB->nfacets, sizeof(double));

	for (n = 0; n < IB->nfacets; n++) {
		cof[0][n] = IB->surface[n].y1 - IB->surface[n].y3;
		cof[1][n] = IB->surface[n].y2 - IB->surface[n].y3;
    cof[2][n] = IB->surface[n].z1 - IB->surface[n].z3;
    cof[3][n] = IB->surface[n].z2 - IB->surface[n].z3;
		double den = cof[0][n]*cof[3][n] - cof[1][n]*cof[2][n];
		if (absolute(den) > eps)	cof[4][n] = 1.0 / den;
		else			          cof[4][n] = 0;		
	}

	for (dg = 0; dg < dim[3]*dim[4]*dim[5]; dg++)		blank[dg] = 1;

  int count = 0;
	for (j = jmin; j <= jmax; j++) {
		for (k = kmin; k <= kmax; k++) {
			int itr = 0;
			for (n = 0; n < IB->nfacets; n++) {
				if (cof[4][n] != 0) {
					double yy, zz;
					yy = IB->surface[n].y3 - y[j];
					zz = IB->surface[n].z3 - z[k];
					double l1, l2, l3;
					l1 = (cof[1][n]*zz - cof[3][n]*yy) * cof[4][n];
					l2 = (cof[2][n]*yy - cof[0][n]*zz) * cof[4][n];
					l3 = 1 - l1 - l2;
					if ((l1 > -eps) && (l2 > -eps) && (l3 > -eps)){
						xd[itr]   = l1*IB->surface[n].x1 + l2*IB->surface[n].x2 + l3*IB->surface[n].x3;
						dist[itr] = abs(x[imin]-xd[itr]);
						itr++;
					}
				}
			}
			if (itr > 500) {
				fprintf(stderr,"Error: In IBIdentyBody() - itr > 500. Recompilation of code needed.\n");
				fprintf(stderr,"Increase size of xd and dist arrays.\n");
        return(1);
			}
	
			if (itr > 0) {
				int ii, jj;
	
				for (ii = 0; ii < itr; ii++) {
					for (jj = ii+1; jj < itr; jj++) {
						if (dist[jj] < dist[ii]) {
							double temp;
							temp     = dist[ii];
							dist[ii] = dist[jj];
							dist[jj] = temp;
							temp     = xd[ii];
							xd[ii]   = xd[jj];
							xd[jj]   = temp;
						}
					}
				}

				for (ii = 1; ii < itr-1; ii++) {
					if (abs(xd[ii]-xd[ii-1]) < eps) {
						for (jj = ii+1; jj < itr; jj++) {
							xd[jj-1]    = xd[jj];
							dist[jj-1]  = dist[jj];
						}
						itr--;
						ii--;
					}
				}

				ii = 0;
				int inside = 0;
				for (i = imin; i <= imax; i++) {
					if ((x[i]-xd[ii])*(x[i]-xd[ii+1]) < eps) {
						inside = 1;
            /* this point is inside                      */
            /* check if (i,j,k) lies within this process */
            /* if so, set blank                          */
            if (   ((i-limits[0])*(i-limits[1]+1) <= 0) 
                && ((j-limits[2])*(j-limits[3]+1) <= 0) 
                && ((k-limits[4])*(k-limits[5]+1) <= 0)) {
              /* calculate local indices */
              int il = i-limits[0];
              int jl = j-limits[2];
              int kl = k-limits[4];
              int p = Index1D(il,jl,kl,dim[3],dim[4],dim[5],dim[6]);
              blank[p] = 0;
              count++;
            }
					} else {
						if (inside) {
							if (ii+2 < itr-1)	ii += 2;
							inside = 0;
						}
					}
				}

			}
		}
	}

  return(count);
}
