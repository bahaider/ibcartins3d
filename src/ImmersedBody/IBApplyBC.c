#include <stdio.h>
#include <arrayfunctions.h>
#include <immersedbody.h>
#include <mathroutines.h>

int IBApplyBC(int n_boundary,int *dim,int *offset,void *b,double *xg,double *yg,double *zg,double *q,double *blank, int flag)
{
  BoundaryNode *boundary = (BoundaryNode*) b;
  double        eps      = IBTolerance;
  int           maxiter  = IBIterLimit;

  int imax        = dim[0];
  int jmax        = dim[1];
  int kmax        = dim[2];
  int g           = dim[3];
  int imax_global = dim[4];
  int jmax_global = dim[5];
  int kmax_global = dim[6];

  int is   = offset[0];
  int js   = offset[1];
  int ks   = offset[2];

	int dg;
	for (dg = 0; dg < n_boundary; dg++) {
		int    i, j, k, p;
		double xb, yb, zb;
		double nx, ny, nz;
		double xx, yy, zz;
		double dx, dy, dz;
    double ds, dist;
		double xtip, ytip, ztip;

		i = boundary[dg].i;
		j = boundary[dg].j;
		k = boundary[dg].k;
    p = Index1D(i,j,k,imax,jmax,kmax,g);
		xb = xg[is+i];
		yb = yg[js+j];
		zb = zg[ks+k];
		nx = boundary[dg].nx;
		ny = boundary[dg].ny;
		nz = boundary[dg].nz;
		xx = boundary[dg].xv;
		yy = boundary[dg].yv;
		zz = boundary[dg].zv;

		dist = nx*(xx-xb) + ny*(yy-yb) + nz*(zz-zb);
		dx = 0.5 * (xg[is+i+1] - xg[is+i-1]);
		dy = 0.5 * (yg[js+j+1] - yg[js+j-1]);
		dz = 0.5 * (zg[ks+k+1] - zg[ks+k-1]);
		ds = min3(dx, dy, dz);

		xtip = xb + dist*nx;
		ytip = yb + dist*ny;
		ztip = zb + dist*nz;

		int is_it_in = 0;
		int iter = 0;
		int itip, jtip, ktip;
		while(!is_it_in && (iter < maxiter)) {
			iter++;
			itip = i;
			jtip = j;
			ktip = k;
		  
			if (xtip > xb)  {
        double xx = xg[is+itip];
        while ((xx < xtip) && (itip < imax+g-1)) {
          itip++;
          if (is+itip < imax_global) xx = xg[is+itip];
          else xx = xg[imax_global-1] + (is+itip-imax_global+1)*(xg[imax_global-1]-xg[imax_global-2]);
        }
      }	else {
        double xx = (is+itip-1 < 0 ? xg[0] + (is+itip-1)*(xg[1]-xg[0]) : xg[is+itip-1]);
        while ((xx > xtip) && (itip > -g)) {
          itip--;
          if (is+itip-1 >= 0) xx = xg[is+itip-1];
          else xx = xg[0] + (is+itip-1)*(xg[1]-xg[0]);
        }
      }
			if (ytip > yb) {
        double yy = yg[js+jtip];
        while ((yy < ytip) && (jtip < jmax+g-1)) {
          jtip++;
          if (js+jtip < jmax_global) yy = yg[js+jtip];
          else yy = yg[jmax_global-1] + (js+jtip-jmax_global+1)*(yg[jmax_global-1]-yg[jmax_global-2]);
        }
      } else {
        double yy = (js+jtip-1 < 0 ? yg[0] + (js+jtip-1)*(yg[1]-yg[0]) : yg[js+jtip-1]);
        while ((yy > ytip) && (jtip > -g)) {
          jtip--;
          if (js+jtip-1 >= 0) yy = yg[js+jtip-1];
          else yy = yg[0] + (js+jtip-1)*(yg[1]-yg[0]);
        }
      }
			if (ztip > zb) {
        double zz = zg[ks+ktip];
        while ((zz < ztip) && (ktip < kmax+g-1))	{
          ktip++;
          if (ks+ktip < kmax_global) zz = zg[ks+ktip];
          else zz = zg[kmax_global-1] + (ks+ktip-kmax_global+1)*(zg[kmax_global-1]-zg[kmax_global-2]);
        }
      } else {
        double zz = (ks+ktip-1 < 0 ? zg[0] + (ks+ktip-1)*(zg[1]-zg[0]) : zg[ks+ktip-1]);
        while ((zz > ztip) && (ktip > -g))	{
          ktip--;
          if (ks+ktip-1 >= 0) zz = zg[ks+ktip-1];
          else zz = zg[0] + (ks+ktip-1)*(zg[1]-zg[0]);
        }
      }

      int ptip[8];
      ptip[0] = Index1D(itip  ,jtip  ,ktip  ,imax,jmax,kmax,g);
      ptip[1] = Index1D(itip-1,jtip  ,ktip  ,imax,jmax,kmax,g);
      ptip[2] = Index1D(itip  ,jtip-1,ktip  ,imax,jmax,kmax,g);
      ptip[3] = Index1D(itip  ,jtip  ,ktip-1,imax,jmax,kmax,g);
      ptip[4] = Index1D(itip-1,jtip-1,ktip  ,imax,jmax,kmax,g);
      ptip[5] = Index1D(itip  ,jtip-1,ktip-1,imax,jmax,kmax,g);
      ptip[6] = Index1D(itip-1,jtip  ,ktip-1,imax,jmax,kmax,g);
      ptip[7] = Index1D(itip-1,jtip-1,ktip-1,imax,jmax,kmax,g);

			int nflow = 0;
			nflow += blank[ptip[0]];
			nflow += blank[ptip[1]];
			nflow += blank[ptip[2]];
			nflow += blank[ptip[3]];
			nflow += blank[ptip[4]];
			nflow += blank[ptip[5]];
			nflow += blank[ptip[6]];
			nflow += blank[ptip[7]];
			if (nflow == 8) {
				is_it_in = 1;
			} else if (nflow < 8) {
				is_it_in = 0;
				xtip += nx*absolute(ds);
				ytip += ny*absolute(ds);
				ztip += nz*absolute(ds);
			} else {
				fprintf(stderr,"Error in IBApplyBC (Bug in code) - counting interior points surrounding probe tip.\n");
        fprintf(stderr,"Value of nflow is %d but can only be positive and <= 8.\n",nflow);
        return(1);
			}
		}

    if (!is_it_in) {
      fprintf(stderr,"Error in IBApplyBC - interior point not found!\n");
      return(1);
    }    
	
		double tlx[2],tly[2],tlz[2];
    if (itip+is-1 < 0)                tlx[0] = xg[0] + (is+itip-1)*(xg[1]-xg[0]);
    else if (itip+is > imax_global)   tlx[0] = xg[imax_global-1] + (is+itip-imax_global)*(xg[imax_global-1]-xg[imax_global-2]);
    else                              tlx[0] = xg[is+itip-1];
    if (itip+is < 0)                  tlx[1] = xg[0] + (is+itip)*(xg[1]-xg[0]);
    else if (itip+is > imax_global-1) tlx[1] = xg[imax_global-1] + (is+itip-imax_global+1)*(xg[imax_global-1]-xg[imax_global-2]);
    else                              tlx[1] = xg[is+itip];
    if (jtip+js-1 < 0)                tly[0] = yg[0] + (js+jtip-1)*(yg[1]-yg[0]);
    else if (jtip+js > jmax_global)   tly[0] = yg[jmax_global-1] + (js+jtip-jmax_global)*(yg[jmax_global-1]-yg[jmax_global-2]);
    else                              tly[0] = yg[js+jtip-1];
    if (jtip+js < 0)                  tly[1] = yg[0] + (js+jtip)*(yg[1]-yg[0]);
    else if (jtip+js > jmax_global-1) tly[1] = yg[jmax_global-1] + (js+jtip-jmax_global+1)*(yg[jmax_global-1]-yg[jmax_global-2]);
    else                              tly[1] = yg[js+jtip];
    if (ktip+ks-1 < 0)                tlz[0] = zg[0] + (ks+ktip-1)*(zg[1]-zg[0]);
    else if (ktip+ks > kmax_global)   tlz[0] = zg[kmax_global-1] + (ks+ktip-kmax_global)*(zg[kmax_global-1]-zg[kmax_global-2]);
    else                              tlz[0] = zg[ks+ktip-1];
    if (ktip+ks < 0)                  tlz[1] = zg[0] + (ks+ktip)*(zg[1]-zg[0]);
    else if (ktip+ks > kmax_global-1) tlz[1] = zg[kmax_global-1] + (ks+ktip-kmax_global+1)*(zg[kmax_global-1]-zg[kmax_global-2]);
    else                              tlz[1] = zg[ks+ktip];


		tly[0] = ((!js && !jtip) ? 2*yg[js+jtip]-yg[js+jtip+1] : yg[js+jtip-1]);
		tly[1] = yg[js+jtip];
		tlz[0] = ((!ks && !ktip) ? 2*zg[ks+ktip]-yg[ks+ktip+1] : zg[ks+ktip-1]);
		tlz[1] = zg[ks+ktip];

    int ptip[8];
    ptip[0] = Index1D(itip-1,jtip-1,ktip-1,imax,jmax,kmax,g);
    ptip[1] = Index1D(itip  ,jtip-1,ktip-1,imax,jmax,kmax,g);
    ptip[2] = Index1D(itip-1,jtip  ,ktip-1,imax,jmax,kmax,g);
    ptip[3] = Index1D(itip  ,jtip  ,ktip-1,imax,jmax,kmax,g);
    ptip[4] = Index1D(itip-1,jtip-1,ktip  ,imax,jmax,kmax,g);
    ptip[5] = Index1D(itip  ,jtip-1,ktip  ,imax,jmax,kmax,g);
    ptip[6] = Index1D(itip-1,jtip  ,ktip  ,imax,jmax,kmax,g);
    ptip[7] = Index1D(itip  ,jtip  ,ktip  ,imax,jmax,kmax,g);

    double tlq[8];
		tlq[0] = q[ptip[0]]; 
		tlq[1] = q[ptip[1]]; 
		tlq[2] = q[ptip[2]]; 
		tlq[3] = q[ptip[3]]; 
		tlq[4] = q[ptip[4]]; 
		tlq[5] = q[ptip[5]]; 
		tlq[6] = q[ptip[6]]; 
		tlq[7] = q[ptip[7]]; 
		
    double qtip = TrilinearInterpolation(&tlx[0],&tly[0],&tlz[0],&tlq[0],xtip,ytip,ztip);

		double tipdist = absolute(nx*(xx-xtip) + ny*(yy-ytip) + nz*(zz-ztip));
		if (tipdist > eps) {
			if      (flag == _REFLECT_)  q[p] = -qtip * absolute(dist) / tipdist;
      else if (flag == _EXTRP_)    q[p] = qtip;
      else                         return(1);
		} else {
			fprintf(stderr,"Warning: in IBApplyBC() - how can probe tip be on surface? Tipdist = %E\n",tipdist);
			q[p] = 0;
		}
	}

  return(0);
}
