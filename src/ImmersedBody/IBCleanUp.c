#include <stdio.h>
#include <stdlib.h>
#include <immersedbody.h>

int IBCleanUp(void* b)
{
  ImmersedBody *ib = (ImmersedBody*) b;
  if (ib->is_present)  free(ib->surface);
  return(0);
}
