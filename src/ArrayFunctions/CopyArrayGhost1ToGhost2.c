#include <arrayfunctions.h>

int CopyArrayGhost1ToGhost2(int imax,int jmax,int kmax,int g1,int g2,double *arrg1, double *arrg2)
{
  int i, j, k;
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p = Index1D(i,j,k,imax,jmax,kmax,g1);
        int q = Index1D(i,j,k,imax,jmax,kmax,g2);
        arrg2[q] = arrg1[p];
      }
    }
  }
  return(0);
}
