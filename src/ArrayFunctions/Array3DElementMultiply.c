#include <arrayfunctions.h>

int Array3DElementMultiply(int imax,int jmax,int kmax,int g1,int g2,double *a,double *x) 
{
  if (!a || !x) return(1);
  int i,j,k;
  for (i = 0; i < imax; i++) {
    for (j = 0; j < jmax; j++) {
      for (k = 0; k < kmax; k++) {
        int p1 = Index1D(i,j,k,imax,jmax,kmax,g1);
        int p2 = Index1D(i,j,k,imax,jmax,kmax,g2);
        x[p2] *= a[p1];
      }
    }
  }
  return(0);
}
