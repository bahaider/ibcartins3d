#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string.h>

int main(){
	int NI, NJ, NK;
	std::ifstream in;
	std::cout << "Reading file \"solver.inp\"...\n";
	in.open("solver.inp");
	if (!in){
		std::cout << "Error: Input file \"solver.inp\" not found. Default values will be used.\n";
	}else{
		char word[20];
		in >> word;
		if (!strcmp(word, "begin")){
			while (strcmp(word, "end")){
				in >> word;
				if      (!strcmp(word, "imax"))			in >> NI;
				else if (!strcmp(word, "jmax"))			in >> NJ;
				else if (!strcmp(word, "kmax"))			in >> NK;
			}			
		}else{
			std::cout << "Error: Illegal format in solver.inp. Default values will be used.\n";
		}
	}
	in.close();
	std::cout << "Grid:\t\t\t" << NI << " X " << NJ << " X " << NK << "\n";

	int i, j, k;
	double dx, dy, dz;
	double *X, *Y,*Z;
	X = (double*) calloc(NI, sizeof(double));	
	Y = (double*) calloc(NJ, sizeof(double));
	Z = (double*) calloc(NK, sizeof(double));

	X[NI/8] = -2.0;
	X[7*NI/8] = 6.0;
	dx = 8.0/ ((double)(6*NI/8)); 
	for (i = NI/8; i < 7*NI/8; i++)	X[i] = X[NI/8] + dx * (i-NI/8);
	for (i = 7*NI/8; i < NI; i++)  	X[i] = X[i-1] + 1.20 * (X[i-1] - X[i-2]);
	for (i = NI/8-1; i >= 0; i--)		X[i] = X[i+1] - 1.15 * (X[i+2] - X[i+1]);

	Y[NJ/8] = -2.0;
	Y[7*NJ/8] = 2.0;
	dy = 4.0/ ((double)(6*NJ/8)); 
	for (j = NJ/8; j < 7*NJ/8; j++)	Y[j] = Y[NJ/8] + dy * (j-NJ/8);
	for (j = 7*NJ/8; j < NJ; j++)		Y[j] = Y[j-1] + 1.4 * (Y[j-1] - Y[j-2]);
	for (j = NJ/8-1; j >= 0; j--)		Y[j] = Y[j+1] - 1.4 * (Y[j+2] - Y[j+1]);

	dz = 0.5 * (dx + dy);
  for (k = 0; k < NK; k++)        Z[k] = k * dz;

	FILE *out;
	out = fopen("init.dat","w");
  for (i = 0; i < NI; i++)  fprintf(out,"%lf ",X[i]);
  fprintf(out,"\n");
  for (j = 0; j < NJ; j++)  fprintf(out,"%lf ",Y[j]);
  fprintf(out,"\n");
  for (k = 0; k < NK; k++)  fprintf(out,"%lf ",Z[k]);
  fprintf(out,"\n");
	for (i = 0; i < NI; i++){
		for (j = 0; j < NJ; j++){
			for (k = 0; k < NK; k++){
				int p = NJ*NK*i + NK*j + k;
				fprintf(out,"%lf ",1.0);						
			}
		}
	}
  fprintf(out,"\n");
	for (i = 0; i < NI; i++){
		for (j = 0; j < NJ; j++){
			for (k = 0; k < NK; k++){
				int p = NJ*NK*i + NK*j + k;
				fprintf(out,"%lf ",0.0);						
			}
		}
	}
  fprintf(out,"\n");
	for (i = 0; i < NI; i++){
		for (j = 0; j < NJ; j++){
			for (k = 0; k < NK; k++){
				int p = NJ*NK*i + NK*j + k;
				fprintf(out,"%lf ",0.0);						
			}
		}
	}
  fprintf(out,"\n");
	fclose(out);

	free(X);
	free(Y);
	free(Z);
	return(0);
}
