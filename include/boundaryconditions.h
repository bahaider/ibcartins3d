#define _MAX_STRING_SIZE_ 500

#define _NOSLIP_      "noslip"
#define _PERIODIC_    "periodic"
#define _SYMMETRIC_   "symmetric"
#define _EXTRAPOLATE_ "extrapolate"
#define _FREESTREAM_  "freestream"

typedef struct domain_boundaries {
  char bctype [_MAX_STRING_SIZE_];                 /* Type of boundary condition                */
  char face   [_MAX_STRING_SIZE_];                 /* Face on which this bc applies             */
                                                   /* "imin","imax","jmin","jmax","kmin","kmax" */
  double  xmin,xmax,ymin,ymax,zmin,zmax;           /* x,y,z extend of this boundary condition   */

  int on_this_proc;   /* flag indicating if this BC is applicable on this process               */
  /* Index range on which to apply this BC on this processor                                    */
  int is, ie, js, je, ks, ke;
  int isu, ieu;               /* i-indices may differ for u-grid                                */
  int jsv, jev;               /* j-indices may differ for v-grid                                */
  int ksw, kew;               /* k-indices may differ for w-grid                                */

  int (*BCFunctionVelocity)(int*,double*,double*,double*,void*,void*,int); /* velocity BC           */
  int (*BCFunctionPressure)(int*,double*,void*,void*,int);                 /* pressure BC           */

  /* specified velocity at the boundary (like wall, freestream,etc) */
  double uWall, vWall, wWall;

  /* buffer array for periodic BCs and its dimensions */
  int     bufdim[4];
  double *buffer;
} DomainBoundary;

/* Functions */
int BCInitialize(void*,int*);
int BCCleanUp   (void*);

/* Periodic boundary conditions for velocity */
int BCVelocityPeriodic_imin     (int*,double*,double*,double*,void*,void*,int);
int BCVelocityPeriodic_imax     (int*,double*,double*,double*,void*,void*,int);
int BCVelocityPeriodic_jmin     (int*,double*,double*,double*,void*,void*,int);
int BCVelocityPeriodic_jmax     (int*,double*,double*,double*,void*,void*,int);
int BCVelocityPeriodic_kmin     (int*,double*,double*,double*,void*,void*,int);
int BCVelocityPeriodic_kmax     (int*,double*,double*,double*,void*,void*,int);

/* Viscous solid wall boundary conditions for velocity */
int BCVelocitySolid_imin        (int*,double*,double*,double*,void*,void*,int);
int BCVelocitySolid_imax        (int*,double*,double*,double*,void*,void*,int);
int BCVelocitySolid_jmin        (int*,double*,double*,double*,void*,void*,int);
int BCVelocitySolid_jmax        (int*,double*,double*,double*,void*,void*,int);
int BCVelocitySolid_kmin        (int*,double*,double*,double*,void*,void*,int);
int BCVelocitySolid_kmax        (int*,double*,double*,double*,void*,void*,int);

/* Zero gradient boundary conditions for velocity */
int BCVelocityExtrapolate_imin  (int*,double*,double*,double*,void*,void*,int);
int BCVelocityExtrapolate_imax  (int*,double*,double*,double*,void*,void*,int);
int BCVelocityExtrapolate_jmin  (int*,double*,double*,double*,void*,void*,int);
int BCVelocityExtrapolate_jmax  (int*,double*,double*,double*,void*,void*,int);
int BCVelocityExtrapolate_kmin  (int*,double*,double*,double*,void*,void*,int);
int BCVelocityExtrapolate_kmax  (int*,double*,double*,double*,void*,void*,int);

/* Freestream boundary conditions for velocity */
int BCVelocityFreestream_imin   (int*,double*,double*,double*,void*,void*,int);
int BCVelocityFreestream_imax   (int*,double*,double*,double*,void*,void*,int);
int BCVelocityFreestream_jmin   (int*,double*,double*,double*,void*,void*,int);
int BCVelocityFreestream_jmax   (int*,double*,double*,double*,void*,void*,int);
int BCVelocityFreestream_kmin   (int*,double*,double*,double*,void*,void*,int);
int BCVelocityFreestream_kmax   (int*,double*,double*,double*,void*,void*,int);

/* Periodic boundary conditions for pressure */
int BCPressurePeriodic_imin     (int*,double*,void*,void*,int);
int BCPressurePeriodic_imax     (int*,double*,void*,void*,int);
int BCPressurePeriodic_jmin     (int*,double*,void*,void*,int);
int BCPressurePeriodic_jmax     (int*,double*,void*,void*,int);
int BCPressurePeriodic_kmin     (int*,double*,void*,void*,int);
int BCPressurePeriodic_kmax     (int*,double*,void*,void*,int);

/* Zero gradient boundary conditions for pressure */
int BCPressureExtrapolate_imin  (int*,double*,void*,void*,int);
int BCPressureExtrapolate_imax  (int*,double*,void*,void*,int);
int BCPressureExtrapolate_jmin  (int*,double*,void*,void*,int);
int BCPressureExtrapolate_jmax  (int*,double*,void*,void*,int);
int BCPressureExtrapolate_kmin  (int*,double*,void*,void*,int);
int BCPressureExtrapolate_kmax  (int*,double*,void*,void*,int);

/* Define no-slip pressure boundary conditions as zero gradient */
#define BCPressureSolid_imin BCPressureExtrapolate_imin
#define BCPressureSolid_imax BCPressureExtrapolate_imax
#define BCPressureSolid_jmin BCPressureExtrapolate_jmin
#define BCPressureSolid_jmax BCPressureExtrapolate_jmax
#define BCPressureSolid_kmin BCPressureExtrapolate_kmin
#define BCPressureSolid_kmax BCPressureExtrapolate_kmax

/* Define freestream pressure boundary conditions as zero gradient */
#define BCPressureFreestream_imin BCPressureExtrapolate_imin
#define BCPressureFreestream_imax BCPressureExtrapolate_imax
#define BCPressureFreestream_jmin BCPressureExtrapolate_jmin
#define BCPressureFreestream_jmax BCPressureExtrapolate_jmax
#define BCPressureFreestream_kmin BCPressureExtrapolate_kmin
#define BCPressureFreestream_kmax BCPressureExtrapolate_kmax
