/* define a mapping of a 3D array index to a 1D array index*/
#define Index1D(i,j,k,I,J,K,ghost) (((K)+2*(ghost))*((J)+2*(ghost))*((i)+(ghost)) + ((K)+2*(ghost))*((j)+(ghost)) + ((k)+(ghost)) + 0*(I)) 

/* Function declarations */
int     SetArrayValue           (double*,int,double);
int     ArrayCopy               (double*,double*,int);
int     CopyArrayGhost1ToGhost2 (int,int,int,int,int,double*,double*);
int     ExtrapolateArray        (double*,double*,int,int);
int     ArrayAXPY               (double*,double,double*,int);
double  Array3DSumSquare        (double*,int*);
int     Array3DElementMultiply  (int,int,int,int,int,double*,double*);
int     ArrayScale              (double*,int,double);
