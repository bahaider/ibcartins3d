#define _MAIN_INPUT_FILE_ "main.inp"
#define _MAX_STRING_SIZE_ 500

typedef struct solver_parameters {

  /* Data arrays */
  double  *x,*y,*z;         /* x,y,z coordinates                                          */
  double  *xg,*yg,*zg;      /* x,y,z coordinates (global)                                 */
  double  *xu,*yv,*zw;      /* Staggered x,y,z coordinates                                */
  double  *xgu,*ygv,*zgw;   /* Staggered x,y,z coordinates (global)                       */
  double  *u,*v,*w;         /* u,v,w velocity components                                  */
  double  *phi;             /* pseudo-pressure                                            */
  double  *uc,*vc,*wc;      /* collocated velocity component arrays                       */
  double  *phic;            /* duplicate array for phi, used in output                    */

  /* work arrays used by the solver during time-integration */
  double *rhs_u, *rhs_v, *rhs_w;    /* right-hand side arrays for predictor               */
  double *divergence;               /* divergence of the solution for corrector           */
  double *du, *dv, *dw, *prev_du, *prev_dv, *prev_dw; /* delta arrays for time-marching   */
  double *uI, *vI, *wI;             /* interface velocities for reconstruction            */
  double *qC, *qI, *up;             /* 1D arrays for interpolation                        */

  /* Solver parameters */
  int     imax_global;      /* Global number of points in i-direction                     */
  int     jmax_global;      /* Global number of points in j-direction                     */
  int     kmax_global;      /* Global number of points in k-direction                     */
  int     imax_local;       /* local number of points in i-direction                      */
  int     jmax_local;       /* local number of points in j-direction                      */
  int     kmax_local;       /* local number of points in k-direction                      */
  int     ghosts;           /* Number of ghost points                                     */ 
  int     n_iter;           /* Number of iterations in time                               */
  int     gs_iter;          /* Max number of linear solver iterations                     */
  int     spatial_scheme;   /* Choice of discretization scheme for convective fluxes      */
  int     time_scheme;      /* Choice of time integration scheme                          */
  char    flux_form[_MAX_STRING_SIZE_]; /* Form of the convective flux                    */
  double  dt;               /* Time step size                                             */
  double  mu;               /* Coefficient of viscosity                                   */
  int     interp_order;     /* Order of interpolation for staggered variables             */

  /* I/O Options */
  int     screen_op_iter;   /* Frequency of screen output                                 */
  int     file_op_iter;     /* Frequency of file output                                   */
  int     print_ls;         /* print linear solver details                                */
  int     ls_monitor;       /* print linear solver iterations                             */
  int     write_residual;   /* write residual history to file                             */
  int     restart_iter;     /* Iteration of restart if this is a restart run              */
  char    write_restart   [_MAX_STRING_SIZE_]; /* Write restart files?                    */
  char    mode2d          [_MAX_STRING_SIZE_]; /* Run in 2D mode (xy,yz,xz,no)            */
  char    op_file_format  [_MAX_STRING_SIZE_]; /* Format of solution output               */
  char    op_overwrite    [_MAX_STRING_SIZE_]; /* Overwrite solution files?               */
  char    op_details      [_MAX_STRING_SIZE_]; /* Level of details in solution file       */

  /* solution output function    */
  int (*WriteOutput)(int*,double*,double*,double*,double*,double*,double*,double*,double*,char*);  

  /* time integration function   */
  int (*TimeIntegrate)(void*,void*,void*);

  /* convection and diffusion functions */
  int (*Convection) (void*,void*,double*,double*,double*,double*,double*,double*);
  int (*Diffusion ) (void*,void*,double*,double*,double*,double*,double*,double*);

  /* linear solver objects for predictor and corrector steps */
  void *predictorLSu,*predictorLSv,*predictorLSw,*correctorLS;

  /* reconstruction functions and parameters */
  int (*ReconstructNormal)      (double*,double*,int,int,int,int,void*);
  int (*ReconstructTransverse)  (double*,double*,int,int,int,int,void*);
  void *ReconstructParams;

  /* Variables for immersed boundaries */
  void    *body;                                /* Immersed body, if present              */
  void    *immersed_boundary;                   /* Boundary of immersed body on main mesh */
  void    *immersed_boundaryu;                  /* Boundary of immersed body on main mesh */
  void    *immersed_boundaryv;                  /* Boundary of immersed body on main mesh */
  void    *immersed_boundaryw;                  /* Boundary of immersed body on main mesh */
  double  *iblank,*iblanku,*iblankv,*iblankw;   /* iblank arrays                          */
  double  *iblankc;                             /* duplicate iblank arrays                */
  /* number of points inside immersed body, and in the boundary of that body              */
  int     nsolid, nboundary ;          /* for the main mesh                               */
  int     nsolidu,nboundaryu;          /* for the staggered u-mesh                        */
  int     nsolidv,nboundaryv;          /* for the staggered v-mesh                        */
  int     nsolidw,nboundaryw;          /* for the staggered w-mesh                        */

  /* Variables for domain boundaries */
  int     nBoundaryZones;              /* Number of domain boundary conditions            */
  void    *BoundaryZones;              /* Boundary zones with bc information              */

  /* Input and output filenames */
  char  inp_filename      [_MAX_STRING_SIZE_];
  char  bcinp_filename    [_MAX_STRING_SIZE_];
  char  ic_filename       [_MAX_STRING_SIZE_];
  char  icrs_filename     [_MAX_STRING_SIZE_];
  char  res_filename      [_MAX_STRING_SIZE_];
  char  stl_filename      [_MAX_STRING_SIZE_];
  char  op_prefix         [_MAX_STRING_SIZE_];
  char  op_filename       [_MAX_STRING_SIZE_];
  char  fsbc_prefix       [_MAX_STRING_SIZE_];
  char  forces_filename   [_MAX_STRING_SIZE_];
  char  surface_filename  [_MAX_STRING_SIZE_];

  /* miscellaneous */
  double divergence_norm;   /* Norm of the divergence of the velocity field */

} IBCartINS3D;

/* Functions */
int ReadInputs            (void*,void*);
int Initialize            (void*,void*);
int InitialSolution       (void*,void*);
int InitializeImmersedBody(void*,void*);
int InitializeBoundaries  (void*,void*);
int InitializeSolvers     (void*,void*);
int Solve                 (void*,void*);
int OutputSolution        (void*,void*);
int CleanUpImmersedBody   (void*);
int CleanUp               (void*);

/* Main driver function */
#ifdef __cplusplus
extern "C"
#endif
int ibcartins3d(int,char**);
