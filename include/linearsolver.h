#define _MAX_STRING_SIZE_ 500

typedef struct _linearsolver_ {

  char    type[_MAX_STRING_SIZE_];            /* type of solver                             */
  int     maxiter;                            /* Maximum number of iterations               */
  void    *context;                           /* solver context                             */
  void    *scheme;                            /* linear solver method                       */
  double  atol,rtol;                          /* absolute and relative tolerance            */
  int     delta;                              /* flag to indicate if solution var is delta  */
  int     verbose;                            /* verbosity flag                             */

  /* function to solve the linear system */
  int (*Solver)      (void*,void*,double*,double*,int*,int*,double*);

  /* function to apply boundary conditions */
  int (*ApplyBC)     (int*,double*,void*,void*,int,int);

  /* number of iterations after solve and error norm */
  int     exit_iter;
  double  exit_norm;

} LinearSolver;

int LSInitialize(void*,char*,int,void*,int*,double,double,int,int,
                 int (*)(int*,double*,void*,void*,int,int));
int LSCleanUp   (void*);

/* Strongly Implicit Procedure */
typedef struct _stronglyimplicitprocedure_ {

  double alpha;
  /* SIP is applicable only to linear systems resulting */
  /* from the heat equation.                            */
  double *Ac, *Ae, *Aw, *An, *As, *At, *Ab;
  /* SIP coefficients */
  double *Lc, *Lw, *Ls, *Lb;
  double *Ue, *Un, *Ut;

} SIP;

int SIPInitialize         (void*,int,int,int);
int SIPCleanUp            (void*);
int SIPComputeCoefficients(void*,void*,int*,double*,double*,double*,double,double);
int SIPSolve              (void*,void*,double*,double*,int*,int*,double*);
