#define _MAX_STRING_SIZE_ 500

extern double IBTolerance;
extern int    IBIterLimit;
extern int    _REFLECT_;
extern int    _EXTRP_;

typedef struct _facet_{
  /* Vertices */
	double x1, x2, x3;
	double y1, y2, y3;
	double z1, z2, z3;
  /* surface normal */
	double nx, ny, nz;
} facet;

typedef struct _body_{
  int     nfacets;    /* number of surface facets   */
  int     is_present; /* is there an immersed body? */
  facet   *surface;   /* array of surface facets    */

  /* coordinates of bounding box */
  double xmin,xmax,ymin,ymax,zmin,zmax;
} ImmersedBody;

typedef struct _boundary_{
  int    i,j,k;          /* grid coordinates                    */
  double nx,ny,nz;       /* nearest normal to the surface       */
  double xv,yv,zv;       /* coordinates of any corner of the    */
                         /* facet containing the nearest normal */
} BoundaryNode;

/* Functions */
#ifdef __cplusplus
extern "C" 
#endif
int STLFileRead           (char*,facet**,int*);
int IBIdentifyBody        (void*,int*,int*,double*,double*,double*,double*);
int IBCountBoundaryPoints (int,int,int,int,double*);
int IBCalculateBodyForces (void*,void*,int*,int*,double*,double*,double*,double*,double*,
                           double*,double*,double*,double*,double*,double,char*,double*);
int IBNearestNormal       (int,int,void*,void*,double*,double*,double*,double);
int IBSetBoundaryIndices  (int,int,int,int,double*,void*);
int IBApplyBC             (int,int*,int*,void*,double*,double*,double*,double*,double*,int);
int IBCleanUp             (void*);
