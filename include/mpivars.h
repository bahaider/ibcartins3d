typedef struct mpi_variable_set {
  int	rank;   /* process rank               */
  int	nproc;  /* total number of processes  */

  int iproc;  /* number of processes along i-direction of the grid */
  int jproc;  /* number of processes along j-direction of the grid */
  int kproc;  /* number of processes along k-direction of the grid */
  /* Note: nproc = iproc * jproc * kproc  */

  int ip;     /* process rank along i-direction of the grid       */
  int jp;     /* process rank along j-direction of the grid       */
  int kp;     /* process rank along k-direction of the grid       */

  int is,ie;  /* start and end i-indices for this proc in global domain */
  int js,je;  /* start and end j-indices for this proc in global domain */
  int ks,ke;  /* start and end j-indices for this proc in global domain */

} MPIVariables;

/* Functions */
int rank1D                (int*,int,int,int,int,int,int);
int rank3D                (int,int,int,int,int*,int*,int*);
int partition1D           (int,int,int,int*);
int LocalDomainLimits     (int,void*,int,int,int,int*,int*,int*,int*,int*,int*);
int MPIBroadcast_double   (double*,int,int);
int MPIBroadcast_integer  (int*,int,int);
int MPIBroadcast_character(char*,int,int);
int MPIMax_integer        (int*,int*,int);
int MPIMax_double         (double*,double*,int);
int MPISum_integer        (int*,int*,int);
int MPISum_double         (double*,double*,int);
int MPIExchangeBoundaries (int,int,int,int,int,int,int,void*,double*);
int MPIPartitionArray3D   (void*,double*,double*,int*,int*);
int MPIGatherArray3D      (void*,double*,double*,int*,int*,int);
int MPIGetArrayData3D     (double*,double*,int*,int*,int*,int*,void*);
