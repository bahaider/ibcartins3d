/* Basic functions */
double min          (double,double);
double min3         (double,double,double);
double max          (double,double);
double max3         (double,double,double);
double absolute     (double);
double raiseto      (double,double);
double sign         (double);
void   FindInterval (double,double,double*,int,int*,int*);
double distance     (double,double,double,double,double,double);
double TriangleArea (double,double,double,double,double,double,double,double,double);

/* Array functions */
int SetArrayValue (double*,int,double);

/* Other functions */
double TrilinearInterpolation(double*,double*,double*,double*,double,double,double);
