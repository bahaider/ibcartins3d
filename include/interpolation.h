typedef struct _interpolations_parameters_ {
  double *upw;    /* array of variables whose sign decides upwinding direction */
  void *scheme;   /* scheme specific set of parameters                         */

} InterpolationParameters;

typedef struct parameters_weno {
  /* Options related to the type of WENO scheme */
  int mapped;		          /* Use mapped weights?                                    */
  int borges;		          /* Use Borges' implementation of weights?                 */
  int yc;		              /* Use Yamaleev-Carpenter implementation of weights?      */
  int no_limiting;        /* Remove limiting -> 5th order polynomial interpolation  */
  double		eps;		      /* epsilon parameter                                      */
  double		p;			      /* p parameter                                            */
} WENOParameters;

/* initializations and clean-up functions */
int InterpInitialize(void*,int);
int InterpCleanUp   (void*);

/*
  One-dimensional Interpolation Functions
  Functions to interpolate a function at interfaces
  from its cell-centered values.

  Argurments:-
    fI        double*   array of size (N+1) with the interpolated interface flux
                        (needs to be allocated outside this function)
                        |<--ghosts-->|0 1 2 ..(interior).. N-1|<--ghosts-->|
    fC        double*   array of size (N+2*ghosts) of the cell centered
                        function values
    N         int       total number of grid points (w/o ghosts)
    ghosts    int       number of ghosts points
    rank      int       rank of this process
    nproc     int       total number of processes
    p         void*     object of type InterpolationParameters containing the required
                        reconstruction-scheme specific info

    Notes: 
      + number of interfaces are one more than number of interior points
      + interface i lies between grid points i-1+ghosts and i+ghosts

            0   1   2   .....        ....          N-3 N-2 N-1
          | x | x | x | x | x | x | x | x | x | x | x | x | x |
          0   1   2   ...             ...    ...     N-2 N-1  N 

      + rank and nproc are useful only for compact interpolation schemes

    Returns 0 on normal execution, non-zero on error.
*/

int FirstOrderUpwind    (double*,double*,int,int,int,int,void*);
int SecondOrderCentral  (double*,double*,int,int,int,int,void*);
int FourthOrderCentral  (double*,double*,int,int,int,int,void*);
int FifthOrderWENO      (double*,double*,int,int,int,int,void*);
